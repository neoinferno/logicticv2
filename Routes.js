import React from "react";
import { Route, Switch } from "react-router-dom";

import Priceconfig from "./configs/Priceconfig";
import logisDoc from "./logisdoc/Datadoc";
import Settingsys from "./setting/Settingsys";
import CreateDoc from "./logistics/Createjob";
import login from "./authlogin/Login";
import NotFoundPage from './pages/NotFoundPage';
import Loadprint from './stockload/loadprint';
import report from './report/report';
import logisDocPre from './report/logisdocpre';
import Pilot from "./settingpilot/pilot";
import Return from "./returnlogis/return";
import Scanitem from "./returnlogis/scanitem";
import Signature from "./Signature/DataSigna";
// report
import Reportbyday from './report/report_byday';
import Reportbymonth from './report/report_bymonth';

class Routes extends React.Component {
  render() {
    return (
      
      <Switch>
        <Route path="/" exact component={login} />
        <Route path="/Login" exact component={login} />
        <Route path="/createLogis" component={CreateDoc} />
        <Route path="/logisDoc" component={logisDoc} />
        <Route path="/config" component={Priceconfig} />
        <Route path="/settingsys" component={Settingsys} />
        <Route path="/loadprint" component={Loadprint} />
       
        <Route path="/report" component={report} />
        <Route path="/report1" component={Reportbyday} />
        <Route path="/report2" component={Reportbymonth} />
        <Route path="/logisDocPre" component={logisDocPre} />
        <Route path="/Signature" component={Signature} />
        <Route path="/settingpilot" component={Pilot} />
        <Route path="/return" component={Return} />
        <Route path="/scanitem" component={Scanitem} />
        <Route path='/404' component={NotFoundPage} />
      </Switch>
    
    );
  }
}

export default Routes;
