# mdbreact-admin-template-free

> MDB - React Admin Dashboard Free

## Build Setup

``` bash
# install dependencies
npm

# serve with hot reload at localhost:8080
npm start

# build for production with minification
npm build

# build for production and view the bundle analyzer report
npm build --report
```