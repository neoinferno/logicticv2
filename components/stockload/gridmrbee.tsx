import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Pre_datagridhead2 extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();
  private selectedRowIndex = React.createRef<HTMLSpanElement>();
  constructor(props: any) {
      super(props);
      // this.myGridOnRowSelect = this.myGridOnRowSelect.bind(this);
  }
  public render() {
   
    const columns: any =[
    
      { text: 'เลขกล่อง ( packrun )', editable: false,  datafield: 'packrun', cellsalign: 'center',align: 'center', width: '10%', filterable: true ,hidden:'true'},
      { text: 'เลขพาเลท ( loaddoc )', editable: false,  datafield: 'loaddoc', cellsalign: 'center',align: 'center', width: '10%' ,hidden:'true'},
      { text: 'บริษัทขนส่ง',editable: false, datafield: 'pilot', align: 'center', cellsalign: 'center' , width: '10%', filterable: false,hidden:'true' },
      { text: 'วันที่โหลด', editable: false,  datafield: 'loaddate', cellsalign: 'center',align: 'center', width: '10%', filterable: false ,hidden:'true' },
      { text: 'BR.',editable: false, datafield: 'branch', align: 'center', cellsalign: 'center'},
      { text: 'สาขา',editable: false, datafield: 'branchname', align: 'center', cellsalign: 'center' , filterable: false },
     
      { text: 'tel',editable: false, datafield: 'tel', align: 'center', cellsalign: 'center',hidden:'true'},
      { text: 'zipcode',editable: false, datafield: 'zipcode', align: 'center', cellsalign: 'center',hidden:'true'},
      { text: 'ที่อยู่สาขา', editable: false,  datafield: 'address', cellsalign: 'center',align: 'center', filterable: false },
     
      {
        text: 'พิมพ์', editable: false, filterable: false, columntype: 'button', buttonclick: (row: number): void => {

            this.props.load_printmrbee(this.myGrid.current!.getrowdata(row));
            

        },
        cellsrenderer: (): string => {
            return 'พิมพ์';
        }, align: 'center', cellsalign: 'center', width: '10%'
    }
  ]
  const source: any = {
    datafields: [
        { name: 'packrun', type: 'string' },
        { name: 'pilot', type: 'string' },
        { name: 'loaddate', type: 'string' },
        { name: 'loaddoc', type: 'string' },
        { name: 'tel', type: 'string' },
        { name: 'zipcode', type: 'number' },
        { name: 'branch', type: 'number' },
        { name: 'address', type: 'string' },
      
        { name: 'branchname', type: 'string' },
    ],
    datatype: 'array',
    localdata:this.props.mrbeedata,
};
      return (
        <JqxGrid
              // onRowselect={this.myGridOnRowSelect}
              ref={this.myGrid}
              theme="metrodark" 
              showstatusbar={true}
              width={'75%'} source={new jqx.dataAdapter(source)} columns={columns}  filterable={true}
              pageable={false} autoheight={false} sortable={true} altrows={false} showfilterrow={true}
              editmode ={'click'} height={380}
              enabletooltips={true} editable={true} selectionmode={'singlecell'}
              showaggregates={true}
        />
      );
  }

  // private myGridOnRowSelect(event: any): void {
  //   let value = this.myGrid.current!.getrowdata(event.args.rowindex);
  //   this.props.setCode_body(value.loaddoc);
  // };


}
export default Pre_datagridhead2;