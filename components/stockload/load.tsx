import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Datagrid extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();

  constructor(props: any) {
      super(props);
     
     
  }
  
  public render() {
     const cellsrenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:5px;color:orange;"><strong>'+value+'</strong></div>'; 
    };
    const columns: any =[
        { text: 'เลขที่ Pallet',cellsrenderer,  editable: false, datafield: 'loaddoc',  cellsalign: 'center',align: 'center', width: '10%' },
        { text: 'เส้นทาง-รอบ',  editable: false, datafield: 'carnumber',  cellsalign: 'center',align: 'center', width: '10%' },
        { text: 'สถานที่',  editable: false, datafield: 'detail',  cellsalign: 'center',align: 'center', width: '20%' },
        { text: 'วันที่โหลด',editable: false,filterable:false, datafield: 'loaddate',  align: 'center', cellsalign: 'center',width: '10%' },
        { text: 'pallet',  editable: false, datafield: 'pallet',  cellsalign: 'center',align: 'center', width: '10%' },
        { text: 'บริษัทขนส่ง',editable: false, filtertype:'list', datafield: 'initials', align: 'center', cellsalign: 'center',width: '20%' },
        { text: 'จำนวนกล่อง',editable: false,filterable:false,  datafield: 'box_num', align: 'center', cellsalign: 'center',width: '10%' },
        { text: 'พิมพ์ใบ',editable: false,filterable:false,columntype:'button', buttonclick: (row: number): void => {

            this.props.loadfn_print(this.myGrid.current!.getrowdata(row));
            
        },
        cellsrenderer: (): string => {
            return 'พิมพ์';
        }, align: 'center', cellsalign: 'center',width: '10%' },
    ]
    const source: any = {
      datafields: [
          { name: 'loaddoc', type: 'string' },
          { name: 'pallet', type: 'string' },
          { name: 'detail', type: 'string' },
          { name: 'loaddate', type: 'string' },
          { name: 'carnumber', type: 'string' },
          { name: 'initials', type: 'string' },
          { name: 'box_num', type: 'number' }, 
      ],
      datatype: 'array',
      localdata:this.props.loaddata,
  };
      return (
        <JqxGrid
              ref={this.myGrid}
              theme="metrodark"
              filterable={true}
              showstatusbar={true}
              width={'99%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={false}
              editmode ={'click'} height={580}
              enabletooltips={true} editable={true} selectionmode={'singlecell'}
              showaggregates={true}
              showfilterrow={true}
        />
      );
  }





}
export default Datagrid;