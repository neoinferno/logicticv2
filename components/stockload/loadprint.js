import React, { Component } from "react";
import {
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  
  MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter,
  MDBCardFooter
} from "mdbreact";
import Select from "react-select";
import {
  ExcelExport,
  ExcelExportColumn
} from "@progress/kendo-react-excel-export";
import { TextInput } from "grommet";
import Grid from "@material-ui/core/Grid";
import {  MDBIcon } from "mdbreact";
import LinearProgress from "@material-ui/core/LinearProgress";
import Barcode from "react-barcode";
import Load from "./load.tsx";
import ApiService from "../actions/apidata";
import Datagridhead from "./datagridhead.tsx";
import Gridmrbee from "./gridmrbee.tsx";
import AuthService from "../authlogin/AuthService";
import moment from 'moment'; 

export default class Loadprint extends Component {
  _exporter;
  constructor(props) {
    super(props);

    this.state = {
      
      completed: 0,
      loaddata:[],
      dockerry:[],
      kerrydata:[],
      mrbeedata:[],
      excelbeedata:[],
      print_sup:'',
      sup:'',
      numpack:'',
      packrun:'',
      address:'',
      tel:'',
      zipcode:'',
      branchname:'',
      supname:'',
      branch:'',
      loaddate:'',
      loaddoc:'',
      modal: false,
      modal2: false,
      palete:''
    };
    this.Auth = new AuthService();
    this.ApiCall = new ApiService();
    this.export = this.export.bind(this);
    this.loadfn_print = this.loadfn_print.bind(this);
    this.load_printkerry = this.load_printkerry.bind(this);
    this.load_printmrbee = this.load_printmrbee.bind(this);
    this.printmrbee = this.printmrbee.bind(this);
    this.apidocdata = this.apidocdata.bind(this);
  }
  
  toggle = () => {
    // console.log(this.state.supname)
   
    this.setState({
    
      modal: !this.state.modal
      
    });
  
  }
  apidocdata(){
    // console.log(this.state.supname)
    var currentDate1 = moment().format("YYYY-MM-DD");
    let docvar = Array();
    docvar = {
      sup:this.state.supname,
      docdate: currentDate1
    };
    this.ApiCall.getKerryDoc(docvar) 
      
    .then(res => {
      if (res.status === true) {
        this.setState({ kerrydata: res.data,});
        console.log(this.state.supname);
        
      } else {
        console.log(res.message);
      }
    })
    .catch(error => {
      console.error(error.message);
    });
   
  }
  toggle2 = () => {
   
  
    this.setState( 
      
    {
      
      modal2: !this.state.modal2 
     
    });
   
  }
  export() {
    this._exporter.save();
  }

  loadfn_print(data){
    this.setState({print_sup:data.initials,loaddoc:data.loaddoc,palete:data.carnumber+'-'+data.pallet},()=>{
        var content = document.getElementById('printarea');
        var pri = document.getElementById('ifmcontentstoprint').contentWindow;
        pri.document.open();
        pri.document.write(content.innerHTML);
        pri.document.close();
        pri.focus();
        pri.print();
    });
   
  }
  load_printkerry(data){
    
    
    this.setState({sup:data.pilot,packrun:data.packrun,address:data.address,loaddate:data.loaddate,branchname:data.branchname,branch:data.branch,tel:data.tel,zipcode:data.zipcode},()=>{
      
    
        var content = document.getElementById('kerry');
        var pri = document.getElementById('ifmkerry').contentWindow;
        pri.document.open();
        pri.document.write(content.innerHTML);
        pri.document.close();
        pri.focus();
        pri.print();
    });

  
  }
  load_printmrbee(data){
    
    
    this.setState({sup:data.pilot,packrun:data.packrun,address:data.address,loaddate:data.loaddate,branchname:data.branchname,branch:data.branch,tel:data.tel,zipcode:data.zipcode},()=>{
      let logdoc={};
              logdoc={
                packrun: this.state.packrun,
                loaddate: this.state.loaddate,
                branch: this.state.branch,
                branchname: this.state.branchname,
                sup: this.state.sup
        }

            // this.ApiCall.receiveLog(logdoc)
      
            // .then(res => {
            //   if (res.status === true) {
              
            //     // console.log(this.state.mrbeedata[0].packrun);
            //   } else {
            //     console.log(res);
            //   }
            // })
            // .catch(error => {
            //   console.error(error.message);
            // });
          
    
        var content = document.getElementById('mrbee');
        var pri = document.getElementById('ifmmrbee').contentWindow;
        pri.document.open();
        pri.document.write(content.innerHTML);
        pri.document.close();
        pri.focus();
        pri.print();
        // this.apidocdata()
    });
  }
  printmrbee(){
  var docvar = this.state.numpack;
    this.ApiCall.printMrBee(docvar)
      
    .then(res => {
      if (res.status === true) {
        this.setState({ mrbeedata: res.data });
        // console.log(this.state.mrbeedata[0].packrun);
      } else {
        console.log(res.message);
      }
    })
    .catch(error => {
      console.error(error.message);
    });
    this.ApiCall.printMrBee(docvar)
    .then(res => {
      if (res.status === true) {
        this.setState(
          {
            mrbeedata: res.data
          },
          () => { }
        );
        this.setState(
          {
            sup: this.state.mrbeedata[0].pilot,
          packrun: this.state.mrbeedata[0].packrun,
        address: this.state.mrbeedata[0].address,
        loaddate: this.state.mrbeedata[0].loaddate,
        branchname: this.state.mrbeedata[0].branchname,
        branch: this.state.mrbeedata[0].branch,
        tel: this.state.mrbeedata[0].tel,
        zipcode: this.state.mrbeedata[0].zipcode
          },
          () => { 
            var content = document.getElementById('mrbee');
            var pri = document.getElementById('ifmmrbee').contentWindow;
            pri.document.open();
            pri.document.write(content.innerHTML);
            pri.document.close();
            pri.focus();
            pri.print();
              let logdoc={};
              logdoc={
                packrun: this.state.mrbeedata[0].packrun,
                loaddate: this.state.mrbeedata[0].loaddate,
                branch: this.state.mrbeedata[0].branch,
                branchname: this.state.mrbeedata[0].branchname,
                sup: this.state.mrbeedata[0].pilot
        }

            this.ApiCall.receiveLog(logdoc)
      
            .then(res => {
              if (res.status === true) {
              
                // console.log(this.state.mrbeedata[0].packrun);
              } else {
                console.log(res.message);
              }
            })
            .catch(error => {
              console.error(error.message);
            });
          }
        );
      } else {
        console.log(res.message);
      }
    })
    .catch(error => {
      console.error(error.message);
    });
    // var numpackrun = this.state.mrbeedata[0].packrun
    // this.setState(
    //   {
      
    //     packrun: this.state.mrbeedata[0].packrun,
    //     address: this.state.mrbeedata[0].address,
    //     loaddate: this.state.mrbeedata[0].loaddate,
    //     branchname: this.state.mrbeedata[0].branchname,
    //     branch: this.state.mrbeedata[0].branch,
    //     tel: this.state.mrbeedata[0].tel,
    //     zipcode: this.state.mrbeedata[0].zipcode
    //   },
    //   () => {
      
    
      
      
        this.setState({ numpack: "" });
        this.apidocdata()
    // });
    
  }
  componentDidMount() {
   
    if (!this.Auth.loggedIn()) {
      window.location.href = "/Login";
    }
  
    let docvar={};
    docvar={
    
      pilot:"MR.bee"
    }

    this.ApiCall.getDataPrintAddr(docvar)
      
    .then(res => {
      if (res.status === true) {
        this.setState({ mrbeedata: res.data });
        // console.log(this.state.mrbeedata[0].packrun);
      } else {
        console.log(res.message);
      }
    })
    .catch(error => {
      console.error(error.message);
    });
    this.ApiCall.Loadprint()
      .then(res => {
        if (res.status === true) {
          this.setState({ loaddata: res.data });
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
     

      // this.ApiCall.getMrbeeDoc(docvar)
      
      // .then(res => {
      //   if (res.status === true) {
      //     this.setState({ mrbeedata: res.data });
      //     console.log(this.state.mrbeedata);
      //   } else {
      //     console.log(res.message);
      //   }
      // })
      // .catch(error => {
      //   console.error(error.message);
      // });
     
  }
  render() {
    var currentDate = moment().format("DD-MM-YYYY");
    var now = moment().format();
    return (
        <div>
      <MDBRow>
        <MDBCol md="12">
          <MDBCard>
            <MDBCardBody style={{ fontFamily: "Prompt" }}>
              <Grid container spacing={24}>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 3 }}
                >
                  <h4 style={{ color: "green", marginTop: 20 }}>
                    <MDBIcon icon="dolly-flatbed" className="pink-text pr-3" />
                    &nbsp;รายการโหลดสินค้าแยกตาม Pallet (Print Pallet)
                  </h4>
                  {/* <h5 style={{ color: "#8089a9", textAlign: "right" }}>
                    <MDBIcon icon="user-edit" className="cyan-text pr-3" />
                    &nbsp;
                  </h5> */}
                </Grid>
                
                <Grid
                  item
                  lg={12}
                  xl={12}
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 10 }}
                >
                  <LinearProgress
                    color="secondary"
                    variant="determinate"
                    value={this.state.completed}
                  />
                  <hr />
                </Grid>
              
                       <MDBBtn
                        color="danger"
                        // disabled={this.state.btndelete}
                        onClick={() => {
                          this.setState(
                            {
                              supname: "Kerry  Express"
                            },
                            () => {  this.apidocdata()}
                          );
                         
                          this.toggle();
                        }}
                      >
                        <MDBIcon icon="print" className="mr-1" /> PrintKerry
                      </MDBBtn>
                      <MDBBtn
                        color="danger"
                        // disabled={this.state.btndelete}
                        onClick={() => {
                          this.setState(
                            {
                              supname: "MR.bee"
                            },
                            () => {  this.apidocdata()}
                          );
                         
                          this.toggle2();
                        }}
                      >
                        <MDBIcon icon="print" className="mr-1" /> MR.BEE
                      </MDBBtn>
                      <MDBBtn
                              color="secondary"
                              onClick={() => {
                                this.export();
                              }}
                            >
                              <MDBIcon
                                icon="calendar-alt"
                                className="mr-1"
                                size="sm"
                              />{" "}
                              Excel
                          </MDBBtn>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 10 }}
                >
                  <MDBCard
                    style={{ width: "100%", marginTop: "1rem" }}
                    className="text-center"
                  >
                    <MDBCardHeader
                      style={{
                        borderBottomWidth: "0px",
                        backgroundColor: "#272262"
                      }}
                    />
                    <MDBCardBody
                      style={{ padding: 12, backgroundColor: "#272262" }}
                    >
                      <Grid container spacing={24}>
                        <Grid
                          item
                          lg={12}
                          xl={12}
                          xs={12}
                          sm={12}
                          md={12}
                          style={{ padding: 0 }}
                        >
                       
                          <center>
                         
                            <Load loaddata={this.state.loaddata} loadfn_print={this.loadfn_print} />
                          </center>
                        </Grid>
                      </Grid>
                    </MDBCardBody>
                    <MDBCardFooter
                      style={{
                        borderTopWidth: "0px",
                        backgroundColor: "#272262",
                        color: "white"
                      }}
                    />
                  </MDBCard>
                </Grid>
              </Grid>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
      



      <MDBContainer>
     
      <MDBModal isOpen={this.state.modal} toggle={this.toggle} size="fluid"> 
                    <MDBModalHeader toggle={this.toggle}>พิมพ์สติ๊กเกอร์({currentDate})</MDBModalHeader>
        <MDBModalBody>
        <Grid container spacing={24}>
        <Grid
                      item
                      lg={2}
                      xl={2}
                      xs={2}
                      sm={2}
                      md={2}
                      style={{ padding: 3 }}
                    >
                      {/* <div className="select-up" style={{ textAlign: "left" }}>
                        <Select
                          value={this.state.transelect}
                          placeholder="-- รายชื่อบริษัทขนส่ง --"
                          size="xsmall"
                          options={this.state.transportdata}
                          onChange={event => {
                            this.setState(
                              { transelect: event, selected: event.defaultapp },
                              () => {
                                this.fildata();
                                // console.log(this.state.transelect.label)
                              }
                            );
                          }}
                        />
                      </div> */}

                    </Grid><br/>
                              <Grid
                                item
                                lg={12}
                                xl={12}
                                xs={12}
                                sm={12}
                                md={12}
                                style={{ padding: 0 }}
                              >
                                <center>
                        
                      
                      
                                  <Datagridhead
                                    // setCode_body={this.setCode_body}
                                    kerrydata={this.state.kerrydata}  load_printkerry={this.load_printkerry}
                                  />
                                </center>
                              </Grid>

                            </Grid>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="secondary" onClick={this.toggle}>ปิด</MDBBtn>
          {/* <MDBBtn color="primary">EXCEL</MDBBtn> */}
        </MDBModalFooter>
      </MDBModal>
    </MDBContainer>




    <MDBContainer>
     
     <MDBModal isOpen={this.state.modal2} toggle={this.toggle2} size="fluid"> 
                   <MDBModalHeader toggle={this.toggle2}>พิมพ์สติ๊กเกอร์MR.BEE({currentDate})</MDBModalHeader>
       <MDBModalBody style={{
                          borderBottomWidth: "0px",
                          backgroundColor: "#272262"
                        }}>
       <Grid container spacing={24}>
       <Grid
                     item
                     lg={2}
                     xl={2}
                     xs={2}
                     sm={2}
                     md={2}
                     style={{ padding: 3 }}
                   >
                    

                   </Grid><br/>
                             <Grid
                               item
                               lg={12}
                               xl={12}
                               xs={12}
                               sm={12}
                               md={12}
                               style={{ padding: 0 }}
                             >
                               <center>
                       
                     
                               {/* <Grid
                            item
                            lg={3}
                            xl={3}
                            xs={3}
                            sm={3}
                            md={3}
                            style={{ padding: 0, marginLeft: 0 }}
                          >
                            <TextInput
                              style={{
                                width: "100%",
                                fontSize: 15,
                                backgroundColor: "white",
                                color: "black"
                              }}
                              size="xsmall"
                              placeholder="พิมพสติ๊กเกอร์"
                              value={this.state.numpack}
                              ref={input => {
                                this.numpack = input;
                              }}
                              onChange={event =>
                                this.setState(
                                  { numpack: event.target.value },
                                  () => { }
                                )
                              }
                              onKeyPress={event => {
                                if (event.key === "Enter") {
                                  {
                                    this.printmrbee();
                                  }
                                }
                              }}
                            />
                          </Grid> */}
                                 <Gridmrbee
                                   // setCode_body={this.setCode_body}
                                   mrbeedata={this.state.mrbeedata}  load_printmrbee={this.load_printmrbee}
                                 />
                               </center>
                             </Grid>

                           </Grid>
                           <ExcelExport
          data={this.state.excelbeedata}
          // group={group}
          fileName={
            "ข้อมูลขนส่ง " +
            currentDate +
            ".xlsx"
          }
          ref={exporter => {
            this._exporter = exporter;
          }}
        >
          <ExcelExportColumn
            field="doc_code"
            title="เลขที่เอกสาร"
            locked={true}
            width={150}
          />
          <ExcelExportColumn field="" title="ขนส่ง" width={150} />
          <ExcelExportColumn field="" title="วันที่ขนส่ง" width={150} />
          <ExcelExportColumn field="" title="บริษัท" width={200} />
          <ExcelExportColumn field="" title="ทะเบียนรถ" width={200} />
          <ExcelExportColumn field="" title="เส้นทาง" width={150} />
          <ExcelExportColumn field="" title="บริษัท" width={200} />
          <ExcelExportColumn field="" title="ทะเบียนรถ" width={100} />
          <ExcelExportColumn field="" title="ชื่อคนขับ" width={100} />
          <ExcelExportColumn field="" title="เบอร์โทร" width={100} />
          <ExcelExportColumn field="" title="สถานะ" width={100} />
          <ExcelExportColumn field="" title="ค่าขนส่ง" width={100} />
          <ExcelExportColumn field="" title="จำนวนสาขา" width={100} />
          <ExcelExportColumn field="" title="จำนวนกล่อง" width={100}  />
        </ExcelExport>
       </MDBModalBody>
       <MDBModalFooter>
         <MDBBtn color="secondary" onClick={this.toggle2}>ปิด</MDBBtn>
         {/* <MDBBtn color="primary">EXCEL</MDBBtn> */}
       </MDBModalFooter>
     </MDBModal>
   </MDBContainer>



      <div style={{margin:'0px'}} style={{display:'none'}}>
            <iframe id={"ifmcontentstoprint"} style={{margin:'0px',height: '0px',width: '0px',position: 'absolute'}}></iframe> 
            <div id={"printarea"} style={{margin:0}}>
                      <div style={{fontSize:16,textAlign:'left',padding:0,margin:0,marginLeft:5}}>{this.state.print_sup}</div>
                      <div style={{textAlign:'center',padding:0,margin:0}}><Barcode value={this.state.loaddoc} fontSize={14} marginLeft={0} marginRight={10} format={"CODE128"} width={2} height={'60%'} /></div><br/>
                      {/* <div style={{fontSize:16,textAlign:'right',margin:0,marginRight:10}}>{this.state.palete}</div> */}
                    
            </div>
            
      </div>
      <div style={{margin:'0px'}} style={{display:'none'}}>
            <iframe id={"ifmkerry"} style={{margin:'0px',height: '0px',width: '0px',position: 'absolute'}}></iframe> 
            <div id={"kerry"} style={{margin:30}}>
                      {/* <div style={{fontSize:20,textAlign:'left',padding:0,margin:0,marginLeft:5}}>{this.state.sup}</div>
                      <div style={{fontSize:20,textAlign:'left',padding:0,margin:0,marginLeft:5}}>{this.state.address}</div>
                      <div style={{fontSize:20,textAlign:'left',padding:0,margin:0,marginLeft:5}}>{this.state.loaddate}</div> */}
                      
                      {/* <div style={{fontSize:16,textAlign:'right',margin:0,marginRight:10}}>{this.state.palete}</div> */}
                      <table border="1"  border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td > Sup:<div style={{fontSize:20,textAlign:'left',padding:0,margin:0,marginLeft:5}}><b>{this.state.sup}</b></div></td>
<td> Date:<div style={{fontSize:20,textAlign:'left',padding:0,margin:0,marginLeft:5}}><b>{this.state.loaddate}</b></div></td>
</tr>
<tr>
<td colspan="2"> สาขา:<div style={{fontSize:30,textAlign:'left',padding:0,margin:0,marginLeft:5}}><b>{this.state.branchname}({this.state.branch})</b></div></td>

</tr>
<tr>
<td  colspan="2">ผู้ส่ง:<div style={{fontSize:16,textAlign:'left',padding:0,margin:0,marginLeft:5}}><b>บริษัทเจ.ไอ.บี.คอมพิวเตอร์กรุ๊ป 
เลขที่ 21 ถนนพหลโยธิน แขวงสนามบิน เขตดอนเมือง กรุงเทพฯ 10210 โทร 088-0026572</b></div></td>

</tr>

<tr>
                    <td  colspan="2">ผู้รับ:<div style={{fontSize:16,textAlign:'left',padding:0,margin:0,marginLeft:5}}><b>{this.state.address} {this.state.zipcode} โทร:{this.state.tel}</b></div></td>

</tr>
<tr>
<td colspan="2"><div style={{textAlign:'center',padding:0,margin:0}}><Barcode value={"LGT"+this.state.packrun} fontSize={16} 
                      marginLeft={0} marginRight={10} format={"CODE128"} width={2} height={'60%'} /></div><br/></td>
</tr>
</tbody>
</table>
            </div>
            
      </div>



{/* ////////////////////////////mrbee///////////////////////////////// */}
      <div style={{margin:'0px'}} style={{display:'none'}}>
            <iframe id={"ifmmrbee"} style={{margin:'0px',height: '0px',width: '0px',position: 'absolute'}}></iframe> 
            <div id={"mrbee"} style={{margin:10}}>
                      {/* <div style={{fontSize:20,textAlign:'left',padding:0,margin:0,marginLeft:5}}>{this.state.sup}</div>
                      <div style={{fontSize:20,textAlign:'left',padding:0,margin:0,marginLeft:5}}>{this.state.address}</div>
                      <div style={{fontSize:20,textAlign:'left',padding:0,margin:0,marginLeft:5}}>{this.state.loaddate}</div> */}
                      
                      {/* <div style={{fontSize:16,textAlign:'right',margin:0,marginRight:10}}>{this.state.palete}</div> */}<br/><br/>
                      <table border="1"  border-collapse="collapse" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td> Sup:<div style={{fontSize:20,textAlign:'left',padding:0,margin:0,marginLeft:5}}><b>{this.state.sup}</b></div></td>
<td> Date:<div style={{fontSize:20,textAlign:'left',padding:0,margin:0,marginLeft:5}}><b>{this.state.loaddate}</b></div></td>
</tr>
<tr>
<td colspan="2"> สาขา:<div style={{fontSize:30,textAlign:'left',padding:0,margin:0,marginLeft:5}}><b>{this.state.branchname}({this.state.branch})</b></div></td>

</tr>
<tr>
<td  colspan="2">ผู้ส่ง:<div style={{fontSize:12,textAlign:'left',padding:0,margin:0,marginLeft:5}}><b>บริษัทเจ.ไอ.บี.คอมพิวเตอร์กรุ๊ป 
เลขที่ 21 ถนนพหลโยธิน แขวงสนามบิน เขตดอนเมือง กรุงเทพฯ 10210 โทร 088-0026572</b></div></td>

</tr>

<tr>
                    <td  colspan="2">ผู้รับ:<div style={{fontSize:12,textAlign:'left',padding:0,margin:0,marginLeft:5}}><b>{this.state.address} {this.state.zipcode} โทร:{this.state.tel}</b></div></td>

</tr>
{/* <tr>
<td colspan="2"><div style={{textAlign:'center',padding:0,margin:0}}><Barcode value={this.state.packrun} fontSize={16} 
                      marginLeft={0} marginRight={10} format={"CODE128"} width={2} height={'60%'} /></div><br/></td>
</tr> */}
</tbody>
</table>
            </div>
            
      </div>
      </div>
    );
   
  }
}
