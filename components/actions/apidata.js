export default class ApiService {
  constructor(domain) {
    this.domain = "http://172.18.0.135:8007";
    this.host_get = "http://172.18.24.113:5010/gateway/get/routeapiget";
    // this.host_get = "http://"+window.location.hostname+":5010/gateway/get/routeapiget";
    this.host_post = "http://172.18.24.113:5010/gateway/post/routeapipost";
    // this.host_post = "http://"+window.location.hostname+":5010/gateway/post/routeapipost";

    this.token = "edMPNtVgBgoSbBJVPWjeLPpxjgVwRsF";
    this.getbranch = this.getbranch.bind(this);
  }
  
  //API GET DATA
  getPrintregister(datadoc) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/getPrintregister/"+datadoc,
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          console.log(responseData)
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

 getlogregis() {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/getLog",
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  getlogregispre() {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/pre_getloadtoday",
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }


  
  gettransport() {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/ReciveTransport",
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

getlogcar() {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/getLog",
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  

  getroute(carsup) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/ReciveRouteAll/" + carsup,
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  getrouteMonjib(varSearch) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/ReciveMoneyJIB",
            Datapass:varSearch,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  printMrBee(docvar) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/printMrBee/" + docvar,
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  getKerryDoc(docvar) {
    let varSearch={};
        varSearch={
          docdate:"2019-08-28",
          sup:"MR.bee"
        }

    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logisticsApp/getDockerry",
            Datapass:docvar,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  } 
  getDataPrintAddr(docvar) {
    let varSearch={};
        varSearch={
          docdate:"2019-08-28",
          sup:"MR.bee"
        }

    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logisticsApp/getDataPrintAddr",
            Datapass:docvar,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  } 
  receiveLog(logdoc) {
    // let varSearch={};
    //     varSearch={
    //       docdate:"2020-02-11"
    //     }

    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/printMrBee/receiveLog",
            Datapass:logdoc,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  } 
  getMaxdoc_kerry() {
   
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logisticsApp/getMaxdoc_kerry",
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  getrouteJIB(carsup) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/ReciveRouteJIB/" + carsup,
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  GetDetailBox() {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/Getbox",
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  DDl_GetDetailBox() {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/ReciveBoxoption",
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  ReciveBoxDetail(sup_id) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/ReciveBoxDetail/" + sup_id,
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  RecivegetGridWeight(sup_id) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/RecivegetGridWeight/" + sup_id,
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  DDl_GetSelectType() {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/GetType",
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  TTB_GetBranch(route) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/GetBranchSet/"+route,
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  ReciveDocLoad(Docno) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/ReciveDocLoad/"+encodeURIComponent(Docno),
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  Deletepackrun(packrun) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/Deletepackrun/"+encodeURIComponent(packrun),
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }



  CancelDocjob(doc_code) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/CancelDocjob/"+encodeURIComponent(doc_code),
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  showDocByID(doc_code) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/showDocByID/"+encodeURIComponent(doc_code),
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  LoadBranchAll() {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/SelectBR",
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }


  Loadprint() {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/loadprint",
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  
  //API POST DATA
  getbranch(varSearch) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/recivebranch",
            Datapass:varSearch,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  UpdatePrice(dataupdate) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/UpdatePrice",
            Datapass:dataupdate,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  UpdateJibPrice(dataupdate) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/UpdateJibPrice",
            Datapass:dataupdate,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  
  LoadJobForBranch(dataupdate) {
    // dataupdate={
    //   "doccode":"LGT201911000217",
    //   "branch":"143"
    // }
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/DocForbranch",
            Datapass:dataupdate,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }


  SetBox(boxarray) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/setBox",
            Datapass:boxarray,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  UpdatePriceBoxsize(dataupdate) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/UpdatePriceBoxsize",
            Datapass:dataupdate,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  UpdatePriceWeight(dataupdate) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/UpdatePriceWeight",
            Datapass:dataupdate,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
UpdateRoute(dataupdate) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/updateRoute",
            Datapass:dataupdate,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  //Regis

  InsertDoc(docvar) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/Savelogisticsdoc",
            Datapass:docvar,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }


  InsertDocHead(docvar) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/Savelogisticsdochead",
            Datapass:docvar,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  UpdateDoc(docvar) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/SaveEndjob",
            Datapass:docvar,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  GetdataLogisDoc(docvar) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/getDocLogistics",
            Datapass:docvar,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  Receivelogisticsdochead(docvar) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/Receivelogisticsdochead",
            Datapass:docvar,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  Updatelogisticsprice(docvar) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/updatePriceregister",
            Datapass:docvar,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  

  // report

  GetdataLogisDoc_report1(docvar) {
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/report/report_byday",
            Datapass:docvar,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  // อัพเดทรายชื่อคนขับ

  Updatedriverlist(docvar) {
    //ตัวอย่างข้อมูลที่ส่ง type 0 INSERT  1 UPDATE 
    //  {
    //   "type":0,
    //   "listname":"อมเรศ เทพณรงค์",
    //   "nickname":"test",
    //   "phone":"0804421128",
     //  "supid":0
    // }
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/updatedriverlist",
            Datapass:docvar,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  
  Deletedriverlist(docvar) {
    //ตัวอย่างข้อมูลที่ส่ง type 2 DELETE 
    //  {
    //   "type":2,
    //   "listname":"อมเรศ เทพณรงค์",
    // }
    // console.log("sd")
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/deletedriverlist",
            Datapass:docvar,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  Showdriverlist() {
 
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/showdriverlist",
            Datapass:[],
            Methodpass:'GET'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_get, options)
        .then(response => response.json())
        .then(responseData => {
         
          return Promise.resolve(responseData);

        });
    } catch (err) {
      return err;
    }
  }
  Apittx() {
    try {
     
      var url ='https://demo-pacjob.thaigpstracker.co.th/apis/logistic/pacjob/doc/addListByMode'
      var options = {
        // url: 'https://demo-pacjob.thaigpstracker.co.th/apis/logistic/pacjob/doc/addListByMode',
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "authorization": 'Basic SklCOmY2azNSclJHNlY='
          
        },
        body: {
       job_mode: 2,
     job_mode_condition: 0,
     job_type_id: 1,
     data: 
      [ { customer_first_name: 'aaaa',
          customer_last_name: 'bbbb',
          customer_address: 'yyyyyyyyyyy',
          customer_zip_code: 50000,
          customer_tel: '0123456789',
          shipment_no: '19000000',
          due_date_time_delivery: '2018-10-23 18:00:00'
          } ] 
        },
        
      };
    
      return fetch(url,options)
      
        .then(response => response.json())
        .then(responseData => {
          // console.log(responseData)
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }


  PackGetBoxInsert(docvar) {
    //ตัวอย่างข้อมูลที่ส่ง type 2 DELETE 
    // {
    //   "doc_code":"LGT201908000021",
    //   "packrun":"20190370763"
    // }
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/Boxlogisticsdochead",
            Datapass:docvar,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

   CarboyUpInsert(docvar) {
    //ตัวอย่างข้อมูลที่ส่ง type 2 DELETE 
    // type: 1,//1=edit,0=insert
    //     id: this.state.id,
    //     listname: this.state.listname,     
    //     nickname: this.state.nickname,
    //     phone: this.state.phone
    try {
      let bodypass=Array();
          bodypass={
            Urlpass:this.domain+"/logistics/updatecarboylist",
            Datapass:docvar,
            Methodpass:'POST'
          }
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "mis-access-token":this.token
        },
        body: JSON.stringify(bodypass)
      };
      return fetch(this.host_post, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  
}
