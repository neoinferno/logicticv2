export default class ApiService {
  constructor(domain) {
    this.domain = "http://172.18.0.135:8007";
    this.getbranch = this.getbranch.bind(this);
  }
  //API GET DATA
  gettransport() {
    try {
      var url = this.domain+"/logistics/ReciveTransport";
      var options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  getroute(carsup) {
    try {
      var url = this.domain+"/logistics/ReciveRouteAll/" + carsup;
      var options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  GetDetailBox() {
    try {
      var url = this.domain+"/logistics/Getbox";
      var options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  DDl_GetDetailBox() {
    try {
      var url = this.domain+"/logistics/ReciveBoxoption";
      var options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  ReciveBoxDetail(sup_id) {
    try {
      var url = this.domain+"/logistics/ReciveBoxDetail/" + sup_id;
      var options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  RecivegetGridWeight(sup_id) {
    try {
      var url =
        this.domain+"/logistics/RecivegetGridWeight/" + sup_id;
      var options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  DDl_GetSelectType() {
    try {
      var url = this.domain+"/logistics/GetType";
      var options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  TTB_GetBranch(route) {
    try {
      var url = this.domain+"/logistics/GetBranchSet/"+route;
      var options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  ReciveDocLoad(Docno) {
    try {
      var url = this.domain+"/logistics/ReciveDocLoad/"+encodeURIComponent(Docno);
      // console.log(encodeURIComponent(Docno));
      var options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  Deletepackrun(packrun) {
    try {
      var url = this.domain+"/logistics/Deletepackrun/"+encodeURIComponent(packrun);
      // console.log(encodeURIComponent(packrun));
      var options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }



  CancelDocjob(doc_code) {
    try {
      var url = this.domain+"/logistics/CancelDocjob/"+encodeURIComponent(doc_code);
      // console.log(encodeURIComponent(packrun));
      var options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  Loadprint() {
    try {
      var url = this.domain+"/logistics/loadprint";
      // console.log(encodeURIComponent(packrun));
      var options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  
  //API POST DATA
  getbranch(varSearch) {
    try {
      var url = this.domain+"/logistics/recivebranch";
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(varSearch)
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }
  UpdatePrice(dataupdate) {
    try {
      var url = this.domain+"/logistics/UpdatePrice";
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(dataupdate)
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  SetBox(boxarray) {
    try {
      var url = this.domain+"/logistics/setBox";
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(boxarray)
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  UpdatePriceBoxsize(dataupdate) {
    try {
      var url = this.domain+"/logistics/UpdatePriceBoxsize";
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(dataupdate)
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  UpdatePriceWeight(dataupdate) {
    try {
      var url = this.domain+"/logistics/UpdatePriceWeight";
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(dataupdate)
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

UpdateRoute(dataupdate) {
    try {
      var url = this.domain+"/logistics/updateRoute";
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(dataupdate)
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }



  //Regis

  InsertDoc(docvar) {
    try {
      var url = this.domain+"/logistics/Savelogisticsdoc";
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(docvar)
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }


  InsertDocHead(docvar) {
    try {
      var url = this.domain+"/logistics/Savelogisticsdochead";
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(docvar)
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  UpdateDoc(docvar) {
    try {
      var url = this.domain+"/logistics/SaveEndjob";
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(docvar)
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  GetdataLogisDoc(docvar) {
    try {
      var url = this.domain+"/logistics/getDocLogistics";
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(docvar)
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  Receivelogisticsdochead(docvar) {
    try {
      var url = this.domain+"/logistics/Receivelogisticsdochead";
      var options = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(docvar)
      };
      return fetch(url, options)
        .then(response => response.json())
        .then(responseData => {
          return Promise.resolve(responseData);
        });
    } catch (err) {
      return err;
    }
  }

  


  
}
