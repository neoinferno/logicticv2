import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Datagridhead extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();
  private selectedRowIndex = React.createRef<HTMLSpanElement>();
  constructor(props: any) {
      super(props);
      this.myGridOnRowSelect = this.myGridOnRowSelect.bind(this);
  }
  public render() {
    // const cellsrenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
    //     if (value === 0) {
    //         return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:red;"><strong>'+value+'</strong></div>';
    //     }
    //     else {
    //         return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:green;"><strong>'+value+'</strong></div>';
    //     }
    // };
    const columns: any =[
        { text: 'เส้นทาง-รอบ', pinned: true,editable: false, datafield: 'route', align: 'center', cellsalign: 'center',width: ' 15%' },
        { text: 'เลขเอกสาร ( LoadDoc )', pinned: true, editable: false,  datafield: 'loaddoc', cellsalign: 'center',align: 'center', width: '25%' },
        { text: 'บริษัทขนส่ง',editable: false, datafield: 'transportname', align: 'center', cellsalign: 'center',width: '30%' },
        { text: 'Pallet',editable: false, datafield: 'pallet', align: 'center', cellsalign: 'center',width: '10%' },
        { text: 'จำนวนกล่อง',editable: false, datafield: 'box_num', align: 'center', cellsalign: 'center',aggregates: ['sum'],width: '20%' }
    ]
    const source: any = {
      datafields: [
          { name: 'loaddoc', type: 'string' },
          { name: 'transportname', type: 'string' },
          { name: 'route', type: 'string' },
          { name: 'pallet', type: 'number' },
          { name: 'box_num', type: 'number' },
      ],
      datatype: 'array',
      localdata:this.props.jobdoclist,
  };
      return (
        <JqxGrid
              onRowselect={this.myGridOnRowSelect}
              ref={this.myGrid}
              theme="metrodark" 
              showstatusbar={true}
              width={'95%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={false}
              editmode ={'click'} height={380}
              enabletooltips={true} editable={true} selectionmode={'singlerow'}
              showaggregates={true}
        />
      );
  }

  private myGridOnRowSelect(event: any): void {
    let value = this.myGrid.current!.getrowdata(event.args.rowindex);
    this.props.setCode_body(value.loaddoc);
  };


}
export default Datagridhead;