import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Pre_datagridbody extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();

  constructor(props: any) {
      super(props);
      this.myGridOnRowData = this.myGridOnRowData.bind(this);
      const renderstatusbar = (statusbar: any): void => {
        const style: React.CSSProperties = { float: 'left', marginLeft: '5px' };
        const buttonsContainer = (
            <div style={{ overflow: 'hidden', position: 'relative', margin: '5px' }}>
                <div id={'addButton'} style={style} />
                <div id={'deleteButton'} style={style} />
                <div id={'reloadButton'} style={style} />
                <div id={'searchButton'} style={style} />
            </div>
        );
        
    };
      
     
  }
  componentWillUpdate(){
    this.myGrid.current!.clearselection()
  }
  public render() {
    // const cellsrenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
    //     if (value === 0) {
    //         return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:red;"><strong>'+value+'</strong></div>';
    //     }
    //     else {
    //         return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:green;"><strong>'+value+'</strong></div>';
    //     }
    // };
    const columns: any =[
        { text: 'รหัสกล่อง', pinned: true, editable: false,  datafield: 'packrun', cellsalign: 'center',align: 'center', width: '40%' },
        { text: 'รหัสสาขา',editable: false,filterable:false, datafield: 'branch', align: 'center', cellsalign: 'center',width: '10%' },
        { text: 'ชื่อสาขา',editable: false,filterable:false, datafield: 'branchname', align: 'center', cellsalign: 'center',width: '40%' },
        { text: 'เส้นทาง',editable: false,filterable:false, datafield: 'branchcode', align: 'center', cellsalign: 'center',width: '10%' },
      //   { text: 'เอาออก',editable: false,  align: 'center', cellsalign: 'center',width: '10%',columntype: 'button',cellsrenderer: function () {
      //     return 'ลบ';
      // }, buttonclick: function (row: any) {

   
      //  }
      // }
    ]
    const source: any = {
      datafields: [
          { name: 'packrun', type: 'string' },
          { name: 'branch', type: 'string' },
          { name: 'branchname', type: 'string' },
          { name: 'branchcode', type: 'string' },
          { name: 'palate', type: 'string' },
      ],
      datatype: 'array',
      localdata:this.props.jobdoclistbody,
    //   updaterow: (rowid: any, rowdata: any, commit: any): void => {
    //     this.props.onUpdatedata(rowdata);
    //     commit(true);
    // }
  };
      return (
        <JqxGrid
              onRowselect={this.myGridOnRowData}
              ref={this.myGrid}
              theme="metrodark"
              filterable={true}
              showstatusbar={true}
              width={'95%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={false}
              editmode ={'click'} height={380}
              enabletooltips={true} editable={true} selectionmode={'singlerow'}
              showaggregates={true} showfilterrow={true}
        />
      );
  }


  private myGridOnRowData(event: any): void {
      let value = this.myGrid.current!.getrowdata(event.args.rowindex);
      this.props.updateDelete(value.packrun);
  };


}
export default Pre_datagridbody;