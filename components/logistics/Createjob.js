import React, { Component } from "react";
import {
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBCardFooter
} from "mdbreact";
import { withSnackbar } from "notistack";
import Grid from "@material-ui/core/Grid";
import { MDBBtn, MDBIcon } from "mdbreact";
import LinearProgress from "@material-ui/core/LinearProgress";
import LoadingOverlay from "react-loading-overlay";
import Select from "react-select";
import { TextInput } from "grommet";
import { Box, RadioButton } from "grommet";
import ApiService from "../actions/apidata";
import AuthService from "../authlogin/AuthService";
import Pre_datagridhead from "./pre_datagridhead.tsx";
import Pre_datagridbody from "./pre_datagridbody.tsx";
import jiblogo from "../img/logo-logistics3.png";
import {
  MDBBtnGroup,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem
} from "mdbreact";

const languages = [
  {
    name: "C",
    year: 1972
  },
  {
    name: "Elm",
    year: 2012
  }
];
const getSuggestions = value => {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
  // const datadrive = this.state.driver
  // console.log(this.state.driver)
  return inputLength === 0
    ? []
    : languages.filter(
      lang => lang.name.toLowerCase().slice(0, inputLength) === inputValue
    );
};
const getSuggestionValue = suggestion => suggestion.name;
const renderSuggestion = suggestion => <div>{suggestion.name}</div>;
class Createjob extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      suggestions: [],
      completed: 0,
      active: false,
      activedoc: 0,
      activedoc1: 0,
      buffer: 10,
      transportdata: [],
      driverdatafill: [],
      driverdatafill2: [],
      doc_code: "",
      datacarname: "",
      datacarboy: "",
      datacarphone: "",
      datacarnumber: "",

      datasupname: "",
      dataroute: "",
      selected: "",
      typeoption: [],
      transelect: [],
      jobdoclist: [],
      headdata: [],
      dataex: [],
      rowdata: [],
      dataprint: [],
      car_number: "",
      car_name: "-- เลือกรายชื่อคนขับ--",
      car_name2: "",

      dateprint: "",
      car_phone: "",
      driverdata: [],
      carboydata: [],
      driver: [],
      driverselect: [],
      phonedata: [],
      phone: [],
      phoneselect: [],
      doc_number: "",

      count_branch: 0,
      count_box: 0,
      lastkey: "",
      loaddochis: "",
      btndelete: true,
      btnsavedoc: false,
      btn_deleteactive: true,
      txtdoc_number: true,
      doc_status: "ไม่มีเอกสาร",
      jobdoclistbody: [],
      deleteitem: "ไม่มีรายการ",
      profile: {}
    };
    this.setDatagrid = this.setDatagrid.bind(this);
    this.setHeadnow = this.setHeadnow.bind(this);
    this.carboy = this.carboy.bind(this);
    this.getGriddata = this.getGriddata.bind(this);
    this.setValueddl = this.setValueddl.bind(this);
    this.setValueddl2 = this.setValueddl2.bind(this);
    this.setCode_body = this.setCode_body.bind(this);
    this.updateDelete = this.updateDelete.bind(this);
    this.driverdata = this.driverdata.bind(this);
    this.carboydata = this.carboydata.bind(this);
    this.fildata = this.fildata.bind(this);
    this.getexcel = this.getexcel.bind(this);
    this.ApiCall = new ApiService();
    this.Auth = new AuthService();
  }

  componentDidMount() {
    if (!this.Auth.loggedIn()) {
      window.location.href = "/Login";
    } else {
      let profile = this.Auth.getProfile();
      this.setState({ profile: profile });
      // console.log(profile)
    }

    this.ApiCall.gettransport()
      .then(res => {
        if (res.status === true) {
          this.setState({ transportdata: res.data });
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });

    this.ApiCall.DDl_GetSelectType()
      .then(res => {
        if (res.status === true) {
          this.setState({ typeoption: res.data });
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });

    this.ApiCall.getlogregis()
      .then(res => {
        if (res.status === true) {
          this.setState({ driver: res.driver, phone: res.phone });
          // console.log(this.state.driver);
          // console.log(this.state.phone);
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
    this.driverdata();
    this.carboydata();
  }
  // clear() {
  //   this.setState(
  //     {
  //       searchdoc: "",
  //       searchsup: ""
  //     },
  //     () => {}
  //   );
  // }
  updateDelete(packrun) {
    this.setState({
      deleteitem: packrun
    });
  }

  setDatagrid() {
    var jobdoc = this.state.doc_number;
    let updatedList = this.state.jobdoclist.filter(function (item) {
      return item.loaddoc.toLowerCase().search(jobdoc.toLowerCase()) !== -1;
    });
    if (updatedList.length > 0) {
      this.props.enqueueSnackbar(
        "ไม่สามารถเพิ่มรายการ ที่มีอยู่แล้วได้..ตรวจสอบดูให้แน่ใจ",
        {
          variant: "error"
        }
      );
    } else {
      let docvar = Array();
      docvar = {
        loaddoc: this.state.doc_number,
        doc_code: this.state.doc_code
      };
      this.ApiCall.InsertDocHead(docvar)
        .then(res => {
          if (res.status === true) {
            this.getGriddata();
          } else {
            this.props.enqueueSnackbar(
              "ไม่พบข้อมูลงานนี้จากระบบ..ตรวจสอบดูให้แน่ใจ",
              {
                variant: "error"
              }
            );
          }
        })
        .catch(error => {
          console.error(error.message);
        });
    }
  }
  load_print() {
    var data = this.state.doc_code;

    this.getexcel(data);
    this.setState(
      {
        doccode: data.doc_code,
        carname: data.car_name,
        carphone: data.car_phone,
        datedoc: data.date_doc
      },
      () => {
        this.timeout = setTimeout(() => {
          var content = document.getElementById("printarea");
          var pri = document.getElementById("ifmcontentstoprint").contentWindow;
          pri.document.open();
          pri.document.write(content.innerHTML);
          pri.document.close();

          pri.print();
        }, 3000);
      }
    );
  }
  getexcel(data) {
    // var datadoc = data.doc_code;

    this.ApiCall.getPrintregister(data)
      .then(res => {
        if (res.status === true) {
          this.setState(
            {
              headdata: res.headdata,
              rowdata: res.rowdata,
              dataex: res.data,
              dataprint: res.docdata
            },
            () => { }
          );
          this.setState(
            {
              dateprint: this.state.dataprint[0].date_doc,
              datacarname: this.state.dataprint[0].car_name,
              datacarboy: this.state.dataprint[0].carboy,
              datacarphone: this.state.dataprint[0].car_phone,
              datacarnumber: this.state.dataprint[0].car_number,
              dataroute: this.state.dataprint[0].route,
              datacount: this.state.dataex[0].countbox,
              datasupname: this.state.dataprint[0].sup_name
            },
            () => { }
          );
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  upprice() {
    // var datadoc = data.doc_code;
    let docvar = Array();
    docvar = {
      doc_code: this.state.doc_code,
      sup_id: this.state.transelect.value,
      price_type: this.state.selected
    };
    this.ApiCall.Updatelogisticsprice(docvar)
      .then(res => {
        if (res.status === true) {
          console.log(docvar);
        }
        console.log(res.message);
      })
      .catch(error => {
        console.error(error.message);
      });
  }

  getGriddata() {
    let docvar = Array();
    docvar = {
      loaddoc: this.state.doc_number,
      doc_code: this.state.doc_code
    };
    this.setState({ active: true })
    this.ApiCall.Receivelogisticsdochead(docvar)
      .then(res => {
        if (res.status === true) {
          this.setState({ jobdoclist: res.Head }, () => {
            // this.doc_number.focus();
            setTimeout(() => {
              this.setState({ active: false })
            }, 3000)
          });
        } else {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
          setTimeout(() => {
            this.setState({ active: false })
          }, 3000)
        }
      })
      .catch(error => {
        console.error(error.message);
      });
    var data = this.state.doc_code;

    this.getexcel(data);
  }

  setCode_body(loaddoc) {
    let docvar = Array();
    if (typeof loaddoc === "undefined") {
      docvar = {
        loaddoc: this.state.loaddochis,
        doc_code: this.state.doc_code
      };
    } else {
      this.setState({ loaddochis: loaddoc });
      docvar = {
        loaddoc: this.state.loaddochis,
        doc_code: this.state.doc_code
      };
    }

    this.ApiCall.Receivelogisticsdochead(docvar)
      .then(res => {
        if (res.status === true) {
          this.setState({ jobdoclistbody: res.HBody }, () => { });
        } else {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  driverdata() {
    this.ApiCall.Showdriverlist()
      .then(res => {
        if (res.status === true) {
          this.setState({ driverdata: res.data });
          //    console.log(this.state.driverdata)
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  carboydata() {
    this.ApiCall.Showdriverlist()
      .then(res => {
        if (res.status === true) {
          this.setState({ carboydata: res.carboy });
          console.log(this.state.carboydata)
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  CancelDocjob() {
    this.ApiCall.CancelDocjob(this.state.doc_code)
      .then(res => {
        if (res.status === true) {
          window.location.reload();
          // this.props.enqueueSnackbar(
          //   "ระบบยกเลิกรายการ สำเร็จแล้ว ",
          //   {
          //     variant: "success"
          //   }
          // );
          // this.setTimeout(
          //     window.location.reload(),
          //   100000
          // );
        } else {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }

  deletePackrun() {
    if (this.state.deleteitem === "ไม่มีรายการ") {
      this.props.enqueueSnackbar("ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..", {
        variant: "error"
      });
    } else {
      this.ApiCall.Deletepackrun(this.state.deleteitem)
        .then(res => {
          if (res.status === true) {
            this.setState({ deleteitem: "ไม่มีรายการ" }, () => {
              this.setCode_body();
              this.getGriddata();
            });
            this.props.enqueueSnackbar("นำรายการออกสำเร็จแล้ว..", {
              variant: "success"
            });
          } else {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          }
        })
        .catch(error => {
          console.error(error.message);
        });
    }
  }

  updateHeadnow() {
    let docvar = Array();
    if (
      typeof this.state.transelect.value === "undefined" ||
      this.state.car_number === "" ||
      this.state.car_name === "" ||
      this.state.car_phone === ""
    ) {
      this.props.enqueueSnackbar(
        "โปรดป้อนข้อมูลให้ครบก่อน..ตรวจสอบดูให้แน่ใจ",
        {
          variant: "error"
        }
      );
    } else {
      docvar = {
        doc_code: this.state.doc_code,
        sup_id: this.state.transelect.value,
        sup_name: this.state.transelect.label,
        logis_type: this.state.selected,
        car_number: this.state.car_number,
        car_name: this.state.car_name,
        carboy: this.state.car_name2,
        car_phone: this.state.car_phone
      };
      console.log(docvar)
      this.ApiCall.UpdateDoc(docvar)
        .then(res => {
          if (res.status === true) {
            this.setState(
              {
                txtdoc_number: true,
                btndelete: true,
                btn_deleteactive: true,
                activedoc: 2,
                doc_status: "เพิ่มสินค้า"
              },
              () => {
                this.props.enqueueSnackbar(
                  "บันทึกรายการเรียบร้อยแล้วค่ะ กดออกจากระบบหรือเริ่มใหม่",
                  {
                    variant: "success"
                  }
                );
              },
              this.load_print()
            );
          } else {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          }
        })
        .catch(error => {
          console.error(error.message);
        });
    }
  }
  carboy() {
    this.setState(
      {
        activedoc1: 1
      },
      () => { }
    );
    console.log(this.state.carboydata)
  }
  setHeadnow() {
    let docvar = Array();
    if (
      typeof this.state.transelect.value === "undefined" ||
      this.state.car_number === "" ||
      this.state.car_name === "-- เลือกรายชื่อคนขับ--" ||
      this.state.car_phone === ""
    ) {
      this.props.enqueueSnackbar(
        "โปรดป้อนข้อมูลให้ครบก่อน..ตรวจสอบดูให้แน่ใจ",
        {
          variant: "error"
        }
      );
    } else {
      docvar = {
        sup_id: this.state.transelect.value,
        sup_name: this.state.transelect.label,
        logis_type: this.state.selected,
        car_number: this.state.car_number,
        car_name: this.state.car_name,
        carboy: this.state.car_name2,
        userlog: this.state.profile.username,
        car_phone: this.state.car_phone
      };
      console.log(docvar)
      this.ApiCall.InsertDoc(docvar)
        .then(res => {
          if (res.status === true) {
            this.setState(
              {
                txtdoc_number: false,
                btndelete: false,
                btn_deleteactive: false,
                activedoc: 1,
                doc_code: res.doc_code,
                doc_status: "เพิ่มสินค้า"
              },
              () => {
                this.doc_number.focus();
              }
            );
          } else {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          }
        })
        .catch(error => {
          console.error(error.message);
        });

      this.setState({ txtdoc_number: false }, () => {
        this.doc_number.focus();
      });
    }
  }
  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };
  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: getSuggestions(value)
    });
  };
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };
  setValueddl(listname) {
    this.setState(
      {
        car_name: listname.target.value
      },
      () => {
        var drive = this.state.car_name;
        let result = this.state.driverdata.filter(function (item) {
          return item.listname == drive;
        });

        this.setState({ car_phone: result[0].phone });
        console.log(this.state.car_name);
      }
    );
  }
  setValueddl2(listname) {
    this.setState(
      {
        car_name2: listname.target.value
      },
      () => {
        console.log(this.state.car_name2)
      }
    );

  }
  fildata() {
    var drive = this.state.transelect.label;
    let result = this.state.driverdata.filter(function (item) {
      return item.transportname == drive;
    });
    // console.log(result[0].phone)
    this.setState({ driverdatafill: result });
  }
  // fildata2() {

  //   this.setState({ driverdatafill: this.state.carboydata[0].listname });
  //    console.log(driverdatafill[0].listname)
  // }
  render() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: "Type a programming language",
      value,
      onChange: this.onChange
    };
    let test = Array();
    test = this.state.dataex;

    function checkvalue(rowdata, headdata) {
      for (var i = 0; i < test.length; i++) {
        if (test[i].branchname == rowdata && test[i].loadname == headdata) {
          return test[i].countbox;
        }
      }
    }
    const list = this.state.rowdata.map(rowdata => (
      <tr>
        <th>{rowdata.branchname}</th>
        <th />
        {this.state.headdata.map(function (headdata) {
          return <th>{checkvalue(rowdata.branchname, headdata.loadname)}</th>;
        })}
      </tr>
    ));

    const listtitle = this.state.rowdata.map(rowdata => (
      <tr align="center">
        <td> {rowdata.loaddoc} </td>
        <td> {rowdata.total} </td>
      </tr>
    ));

    const listdata = this.state.driverdatafill.map(driverdatafill => (
      <MDBDropdownItem
        onClick={this.setValueddl}
        value={driverdatafill.listname}
      >
        {driverdatafill.listname}
      </MDBDropdownItem>
    ));

    const listdata2 = this.state.carboydata.map(carboydata => (
      <MDBDropdownItem
        onClick={this.setValueddl2}
        value={carboydata.listname}
      >
        {carboydata.listname}
      </MDBDropdownItem>
    ));
    //   const listdata = this.state.valueddl.map(valueddl => (
    //     <MDBDropdownItem onClick={this.setValueddl} value={valueddl.label}>
    //         {valueddl.label}
    //     </MDBDropdownItem>
    // ));

    // let datetime = this.state.dataprint[0].date_doc;
    const { completed } = this.state;
    return (
      <MDBRow>
        <MDBCol md="12">
          <MDBCard>
            <MDBCardBody style={{ fontFamily: "Prompt" }}>
              <Grid container spacing={24}>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 3 }}
                >
                  <h4 style={{ color: "green" }}>
                    <MDBIcon icon="chalkboard" className="pink-text pr-3" />
                    &nbsp;สร้างรายการขนส่ง (Load To Car)
                  </h4>
                  <h5
                    style={{
                      color: "#8089a9",
                      textAlign: "right",
                      marginBottom: 3
                    }}
                  >
                    <MDBIcon icon="user-edit" className="cyan-text pr-3" />
                    &nbsp;คุณ {this.state.profile.fullname}(
                    {this.state.profile.username}){" "}
                    (ผู้ทำรายการ)&nbsp;&nbsp;&nbsp;
                  </h5>
                </Grid>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 10, paddingTop: 0 }}
                >
                  <LinearProgress
                    color="secondary"
                    variant="determinate"
                    value={completed}
                  />
                  <hr
                    style={{ paddingBottom: 0, marginBottom: 0, marginTop: 10 }}
                  />
                </Grid>
                <Grid
                  item
                  lg={4}
                  xl={4}
                  xs={4}
                  sm={4}
                  md={4}
                  style={{ padding: 10 }}
                >
                  <Grid container spacing={24} style={{ margin: 0 }}>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      xs={6}
                      sm={6}
                      md={6}
                      style={{ padding: 3 }}
                    >
                      <h5
                        style={{
                          color: "#8089a9",
                          textAlign: "right"
                        }}
                      >
                        เลือกบริษัทขนส่ง{" "}
                      </h5>
                    </Grid>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      xs={6}
                      sm={6}
                      md={6}
                      style={{ padding: 3 }}
                    >
                      <div className="select-up" style={{ textAlign: "left" }}>
                        <Select
                          value={this.state.transelect}
                          placeholder="-- รายชื่อบริษัทขนส่ง --"
                          size="xsmall"
                          options={this.state.transportdata}
                          onChange={event => {
                            this.setState(
                              { transelect: event, selected: event.defaultapp },
                              () => {
                                this.fildata();
                                // console.log(this.state.transelect.label)
                              }
                            );
                          }}
                        />
                      </div>
                    </Grid>
                    <Grid
                      item
                      lg={12}
                      xl={12}
                      xs={12}
                      sm={12}
                      md={12}
                      style={{ padding: 3, marginTop: 30 }}
                    >
                      <MDBBtn
                        color="danger"
                        disabled={this.state.btndelete}
                        onClick={() => {
                          this.CancelDocjob();
                        }}
                      >
                        <MDBIcon icon="undo" className="mr-1" /> ยกเลิกเอกสาร
                      </MDBBtn>
                      {this.state.activedoc === 0 && (
                        <MDBBtn
                          color="purple"
                          disabled={this.state.btnsavedoc}
                          onClick={() => {
                            this.setHeadnow();
                          }}
                        >
                          เพิ่มข้อมูลสินค้า{" "}
                          <MDBIcon icon="save" className="ml-1" />
                        </MDBBtn>
                      )}
                      {this.state.activedoc === 1 && (
                        <MDBBtn
                          color="dark-green"
                          disabled={this.state.btnsavedoc}
                          onClick={() => {
                            this.updateHeadnow();
                            this.upprice();
                          }}
                        >
                          ปิดจ๊อบ เริ่มเอกสารใหม่{" "}
                          <MDBIcon icon="save" className="ml-1" />
                        </MDBBtn>
                      )}
                      {this.state.activedoc === 2 && (
                        <MDBBtn
                          color="deep-purple"
                          // disabled={this.state.btnsavedoc}
                          onClick={() => {
                            window.location.reload();
                          }}
                        >
                          เริ่มเอกสารใหม่{" "}
                          <MDBIcon icon="clipboard-check" className="ml-1" />
                        </MDBBtn>
                      )}
                    </Grid>
                  </Grid>
                </Grid>
                <Grid
                  item
                  lg={4}
                  xl={4}
                  xs={4}
                  sm={4}
                  md={4}
                  style={{ padding: 10 }}
                >
                  <Grid container spacing={24}>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      xs={6}
                      sm={6}
                      md={6}
                      style={{ padding: 3 }}
                    >
                      <h5
                        style={{
                          color: "#8089a9",
                          textAlign: "right",
                          marginRight: 0,
                          paddingRight: 0,
                          marginTop: 0
                        }}
                      >
                        ประเภทของค่าขนส่ง{" "}
                      </h5>
                    </Grid>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      xs={6}
                      sm={6}
                      md={6}
                      style={{ padding: 10 }}
                    >
                      <Box align="start">
                        {this.state.typeoption.map(label => (
                          <Box
                            key={label.id}
                            margin={{ vertical: "small" }}
                            style={{ marginTop: 3, marginBottom: 3 }}
                          >
                            <RadioButton
                              name="prop"
                              checked={this.state.selected === label.id}
                              label={label.type_logis}
                              onChange={() =>
                                this.setState({ selected: label.id })
                              }
                            />
                          </Box>
                        ))}
                      </Box>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid
                  item
                  lg={4}
                  xl={4}
                  xs={4}
                  sm={4}
                  md={4}
                  style={{ padding: 10 }}
                >
                  <Grid container spacing={24}>
                    <Grid
                      item
                      lg={4}
                      xl={4}
                      xs={4}
                      sm={4}
                      md={4}
                      style={{ padding: 3 }}
                    >
                      <h5
                        style={{
                          color: "#8089a9",
                          textAlign: "right"
                        }}
                      >
                        ทะเบียนรถ{" "}
                      </h5>
                    </Grid>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      xs={6}
                      sm={6}
                      md={6}
                      style={{ padding: 3 }}
                    >
                      <TextInput
                        style={{
                          width: "100%",
                          fontSize: 15,
                          backgroundColor: "white",
                          color: "black"
                        }}
                        size="xsmall"
                        value={this.state.car_number}
                        ref={input => {
                          this.car_number = input;
                        }}
                        onChange={event =>
                          this.setState(
                            { car_number: event.target.value },
                            () => { }
                          )
                        }
                      />
                    </Grid>
                    <Grid item lg={2} xl={2} xs={2} sm={2} md={2} />
                    <Grid
                      item
                      lg={4}
                      xl={4}
                      xs={4}
                      sm={4}
                      md={4}
                      style={{ padding: 3 }}
                    >
                      <h5
                        style={{
                          color: "#8089a9",
                          textAlign: "right"
                        }}
                      >
                        ผู้ขับ{" "}
                      </h5>
                    </Grid>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      xs={6}
                      sm={6}
                      md={6}
                      style={{ padding: 3 }}
                    >
                      <MDBBtnGroup>
                        <MDBDropdown>
                          <MDBDropdownToggle
                            caret
                            color="primary "
                            outline
                            rounded
                            className="h-100"
                          >
                            {this.state.car_name}
                          </MDBDropdownToggle>
                          <MDBDropdownMenu basic color="danger">
                            {listdata}
                          </MDBDropdownMenu>
                        </MDBDropdown>
                      </MDBBtnGroup>
                    </Grid>

                    <Grid item lg={2} xl={2} xs={2} sm={2} md={2} />
                    <Grid
                      item
                      lg={4}
                      xl={4}
                      xs={4}
                      sm={4}
                      md={4}
                      style={{ padding: 3 }}
                    >
                      {this.state.transelect.value === 1 && (

                        <h5
                          style={{
                            color: "#8089a9",
                            textAlign: "right"
                          }}
                        >
                          เด็กติดรถ{" "}
                        </h5>
                      )}

                    </Grid>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      xs={6}
                      sm={6}
                      md={6}
                      style={{ padding: 3 }}
                    >
                      {this.state.transelect.value === 1 && (

                        //  <p>รายชื่อเด็กติดรถ</p>
                        <MDBBtnGroup>

                          <MDBDropdown>
                            <MDBDropdownToggle
                              caret
                              color="primary "
                              outline
                              rounded
                              className="h-100"

                            >
                              {this.state.car_name2}
                            </MDBDropdownToggle>
                            <MDBDropdownMenu basic color="danger">
                              {listdata2}
                            </MDBDropdownMenu>
                          </MDBDropdown>
                        </MDBBtnGroup>
                      )}

                    </Grid>

                    <Grid item lg={2} xl={2} xs={2} sm={2} md={2} />
                    <Grid
                      item
                      lg={4}
                      xl={4}
                      xs={4}
                      sm={4}
                      md={4}
                      style={{ padding: 3 }}
                    >
                      <h5
                        style={{
                          color: "#8089a9",
                          textAlign: "right"
                        }}
                      >
                        เบอร์โทร{" "}
                      </h5>
                    </Grid>
                    <Grid
                      item
                      lg={6}
                      xl={6}
                      xs={6}
                      sm={6}
                      md={6}
                      style={{ padding: 3 }}
                    >
                      <TextInput
                        style={{
                          width: "100%",
                          fontSize: 15,
                          backgroundColor: "white",
                          color: "black"
                        }}
                        size="xsmall"
                        value={this.state.car_phone}
                        // placeholder="exp 031"
                        ref={input => {
                          this.car_phone = input;
                        }}
                        onChange={event =>
                          this.setState(
                            { car_phone: event.target.value },
                            () => { }
                          )
                        }
                      // onKeyPress={event => {
                      //   if (event.key === "Enter") {
                      //     this.SearchData();
                      //   }
                      // }}
                      />
                    </Grid>
                  </Grid>
                </Grid>
                <Grid
                  item
                  lg={12}
                  xl={12}
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 10 }}
                >
                  <MDBCard
                    style={{ width: "100%", marginTop: "1rem" }}
                    className="text-center"
                  >
                    <MDBCardHeader
                      style={{
                        borderBottomWidth: "0px",
                        padding: 20,
                        backgroundColor: "#272262"
                      }}
                    >
                      <Grid container spacing={24}>
                        <Grid
                          item
                          lg={2}
                          xl={2}
                          xs={2}
                          sm={2}
                          md={2}
                          style={{ padding: 10 }}
                        >
                          <h5
                            style={{
                              color: "white",
                              textAlign: "right"
                            }}
                          >
                            เลขที่เอกสาร{" "}
                          </h5>
                        </Grid>
                        <Grid
                          item
                          lg={2}
                          xl={2}
                          xs={2}
                          sm={2}
                          md={2}
                          style={{ padding: 10 }}
                        >
                          <TextInput
                            style={{
                              width: "100%",
                              fontSize: 15,
                              backgroundColor: "white",
                              color: "black"
                            }}
                            size="xsmall"
                            disabled={this.state.txtdoc_number}
                            value={this.state.doc_number}
                            ref={input => {
                              this.doc_number = input;
                            }}
                            onChange={event =>
                              this.setState(
                                { doc_number: event.target.value },
                                () => {
                                  this.doc_number.focus();
                                }
                              )
                            }
                            onKeyPress={event => {
                              if (event.key === "Enter") {
                                console.log(this.state.doc_number);
                                this.setDatagrid();
                                this.setState({
                                  doc_number: "",
                                  lastkey: this.state.doc_number
                                });
                              }
                            }}
                          />
                        </Grid>
                        <Grid
                          item
                          lg={3}
                          xl={3}
                          xs={3}
                          sm={3}
                          md={3}
                          style={{ padding: 10, textAlign: "left" }}
                        >
                          ล่าสุด : {this.state.lastkey}
                        </Grid>
                        <Grid
                          item
                          lg={5}
                          xl={5}
                          xs={5}
                          sm={5}
                          md={5}
                          style={{ padding: 10, textAlign: "right" }}
                        >
                          {" "}
                          รหัสกล่อง :&nbsp;{" "}
                          <k style={{ color: "red" }}>
                            {this.state.deleteitem}
                          </k>
                          &nbsp;&nbsp;
                          <MDBBtn
                            color="danger"
                            size="sm"
                            disabled={this.state.btn_deleteactive}
                            onClick={() => {
                              this.deletePackrun();
                            }}
                          >
                            <MDBIcon icon="redo" className="mr-1" /> นำออก
                          </MDBBtn>
                        </Grid>
                      </Grid>
                    </MDBCardHeader>
                    <MDBCardBody
                      style={{ padding: 12, backgroundColor: "#272262" }}
                    >
                      <Grid container spacing={24}>
                        <Grid
                          item
                          lg={6}
                          xl={6}
                          xs={6}
                          sm={6}
                          md={6}
                          style={{ padding: 0 }}
                        >
                          <center>
                            <LoadingOverlay active={this.state.active} spinner text="กำลังโหลด...">
                              <Pre_datagridhead
                                setCode_body={this.setCode_body}
                                jobdoclist={this.state.jobdoclist}
                              />
                            </LoadingOverlay>
                          </center>
                        </Grid>
                        <Grid
                          item
                          lg={6}
                          xl={6}
                          xs={6}
                          sm={6}
                          md={6}
                          style={{ padding: 0, textAlign: "center" }}
                        >
                          <center>
                            <Pre_datagridbody
                              updateDelete={this.updateDelete}
                              jobdoclistbody={this.state.jobdoclistbody}
                            />
                          </center>
                        </Grid>
                      </Grid>
                    </MDBCardBody>
                    <MDBCardFooter
                      style={{
                        borderTopWidth: "0px",
                        backgroundColor: "#272262",
                        color: "white"
                      }}
                    >
                      สถานะเอกสาร&nbsp;:&nbsp;
                      {this.state.doc_status === "ไม่มีเอกสาร" && (
                        <k style={{ color: "red" }}> {this.state.doc_status}</k>
                      )}
                      {this.state.doc_status === "เพิ่มสินค้า" && (
                        <k style={{ color: "green" }}>
                          {" "}
                          {this.state.doc_status}
                        </k>
                      )}
                      &nbsp;&nbsp;&nbsp;Doc_Code &nbsp;:&nbsp;&nbsp;
                      <k style={{ color: "green" }}>{this.state.doc_code}</k>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      จำนวนการขนส่ง&nbsp;&nbsp;&nbsp;&nbsp;
                      {"     "}&nbsp;&nbsp;
                      <k style={{ color: "green" }}>
                        {this.state.jobdoclistbody.length}
                      </k>{" "}
                      กล่อง
                    </MDBCardFooter>
                  </MDBCard>
                </Grid>
              </Grid>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
        <div style={{ margin: "0px" }} style={{ display: "none" }}>
          <iframe
            id={"ifmcontentstoprint"}
            style={{
              margin: "0px",
              height: "0px",
              width: "0px",
              position: "absolute"
            }}
          />
          <div id={"printarea"} style={{ margin: 0 }}>
            <br />
            <table
              border="0"
              width="100%"
              style={{ "border-collapse": "collapse", fontSize: "14px" }}
            >
              <tr>
                <td align="left">
                  <img
                    style={{ height: 50 }}
                    alt="MDB React Logo"
                    className="img-fluid"
                    src={jiblogo}
                  />
                </td>
                <td align="right">
                  บริษัท เจ.ไอ.บี.คอมพิวเตอร์ กรุ๊ป จำกัด <br />
                  เลขที่21 ถ.พหลโยธิน แขวงสนามบิน เขตดอนเมือง กรุงเทพฯ
                </td>
              </tr>
              <br />

              <tr>
                <td />
                <td align="right" />
              </tr>
            </table>

            <br />

            <div className="full">
              <table
                align="center"
                border="0"
                width="100%"
                style={{ "border-collapse": "collapse", fontSize: "15px" }}
              >
                <tr align="left">
                  <td>ชื่อผู้ขับ:{this.state.datacarname}</td>
                  <td>เด็กติดรถ:{this.state.datacarboy}</td>
                  <td>เบอร์โทร:{this.state.datacarphone}</td>
                  <td>ทะเบียนรถ:{this.state.datacarnumber}</td>
                </tr>
                <tr align="left">
                  <td>เอกสาร:{this.state.doc_code}</td>
                  <td>บริษัท:{this.state.datasupname}</td>
                  <td>เวลาพิมพ์:{this.state.dateprint}</td>
                </tr>

                <tr align="left">
                  <td> เส้นทาง: {this.state.dataroute} </td>
                </tr>
              </table>

              <table
                align="center"
                border="1"
                width="100%"
                style={{ "border-collapse": "collapse", fontSize: "16px" }}
              >
                <tr>
                  <th>รหัส</th>
                  <th>จำนวนกล่อง</th>
                  {listtitle}
                  <td> </td>
                  <th> รวม( {this.state.datacount} )กล่อง</th>
                </tr>

                {/* {list} */}
              </table>
              <br />

              <br />
              <br />
              <br />
              <table width="100%">
                <tr>
                  <td align="center">_____________________</td>
                  <td align="center">_____________________</td>
                </tr>
                <tr>
                  <td align="center">ผู้ตรวจสอบ</td>
                  <td align="center">ผู้ขนส่ง</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </MDBRow>
    );
  }
}

export default withSnackbar(Createjob);
