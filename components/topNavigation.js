import React, { Component } from "react";
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavbarToggler,
  MDBCollapse,
  MDBNavItem,
  MDBNavLink,
  MDBIcon,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem
} from "mdbreact";
import { NavLink } from "react-router-dom";
import jiblogo from "./img/logo-logistics3.png";
import AuthService from "./authlogin/AuthService";

class TopNavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accessapp: [],
      accessadmin: 0,
      collapse: false
    };
    this.Auth = new AuthService();
    // this.getRoutetitle = this.getRoutetitle.bind(this);
  }

  componentDidMount() {
    let Access = this.Auth.getAccess();
    let Accessadmin = this.Auth.getAccessadmin();
    if (Access != 0) {
      let Accessapp = JSON.parse(Access);
      this.setState({ accessapp: Accessapp, accessadmin: Accessadmin }, () => {
        // console.log(this.state.accessapp);
      });
    } else {
      this.setState({ accessapp: [], accessadmin: [] }, () => {
        // console.log(this.state.accessapp);
      });
    }
  }

  onClick = () => {
    this.setState({
      collapse: !this.state.collapse
    });
  };

  toggle = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  };

  render() {
    return (
      <MDBNavbar
        style={{ background: "rgb(39, 34, 98)" }}
        // className="flexible-navbar"
        dark
        expand="md"
        scrolling
      >
        <MDBNavbarBrand href="/">
          <img
            style={{ height: 50, marginLeft: 10 }}
            alt="MDB React Logo"
            className="img-fluid"
            src={jiblogo}
          />
        </MDBNavbarBrand>
        <MDBNavbarToggler onClick={this.onClick} />
        <MDBCollapse isOpen={this.state.collapse} navbar>
          <MDBNavbarNav left>
            <MDBNavItem>
              <MDBDropdown>
                {this.state.accessapp.length != 0 && this.state.accessapp[0].views == 1 && this.state.accessapp[1].views == 1 && (
                  <MDBDropdownToggle nav caret style={{ fontFamily: "Prompt" }}>
                    <div className="d-none d-md-inline">
                      &nbsp;&nbsp;รายการขนส่งสินค้า
                    </div>
                  </MDBDropdownToggle>
                )}
                {this.state.accessadmin == 1 && (
                  <MDBDropdownToggle nav caret style={{ fontFamily: "Prompt" }}>
                    <div className="d-none d-md-inline">
                      &nbsp;&nbsp;รายการขนส่งสินค้า
                    </div>
                  </MDBDropdownToggle>
                )}

                <MDBDropdownMenu
                  className="dropdown-default"
                  style={{ padding: 0, fontFamily: "Prompt" }}
                >
                  {this.state.accessapp.length != 0 &&
                    this.state.accessapp[0].views == 1 && (
                      <MDBDropdownItem>
                        {" "}
                        <MDBNavLink
                          to="/createLogis"
                          activeClassName="activeClass"
                          style={{ color: "black" }}
                        >
                          <MDBIcon icon="table" className="mr-3" />{" "}
                          สร้างรายการขนส่ง
                        </MDBNavLink>
                      </MDBDropdownItem>
                    )}
                  {this.state.accessadmin == 1 && (
                    <MDBDropdownItem>
                      {" "}
                      <MDBNavLink
                        to="/createLogis"
                        activeClassName="activeClass"
                        style={{ color: "black" }}
                      >
                        <MDBIcon icon="user" className="mr-3" />{" "}
                        สร้างรายการขนส่ง
                      </MDBNavLink>
                    </MDBDropdownItem>
                  )}
                  {this.state.accessapp.length != 0 &&
                    this.state.accessapp[1].views == 1 && (
                      <MDBDropdownItem>
                        {" "}
                        <MDBNavLink
                          to="/logisDoc"
                          activeClassName="activeClass"
                          style={{ color: "black" }}
                        >
                          <MDBIcon icon="table" className="mr-3" />{" "}
                          รายการใบขนส่งสินค้า
                        </MDBNavLink>
                      </MDBDropdownItem>
                    )}
                  {this.state.accessadmin == 1 && (
                    <MDBDropdownItem>
                      {" "}
                      <MDBNavLink
                        to="/logisDoc"
                        activeClassName="activeClass"
                        style={{ color: "black" }}
                      >
                        <MDBIcon icon="table" className="mr-3" />{" "}
                        รายการใบขนส่งสินค้า
                      </MDBNavLink>
                    </MDBDropdownItem>
                  )}
                </MDBDropdownMenu>
              </MDBDropdown>
            </MDBNavItem>
            <MDBNavItem>
              <MDBDropdown>
                {this.state.accessapp.length != 0 && this.state.accessapp[2].views == 1 && this.state.accessapp[3].views == 1 &&(
                  <MDBDropdownToggle nav caret style={{ fontFamily: "Prompt" }}>
                    <div className="d-none d-md-inline">
                      &nbsp;&nbsp;ตั้งค่าขนส่ง
                    </div>
                  </MDBDropdownToggle>
                )}

                {this.state.accessadmin == 1 && (
                  <MDBDropdownToggle nav caret style={{ fontFamily: "Prompt" }}>
                    <div className="d-none d-md-inline">
                      &nbsp;&nbsp;ตั้งค่าขนส่ง
                    </div>
                  </MDBDropdownToggle>
                )}

                <MDBDropdownMenu
                  className="dropdown-default"
                  style={{ padding: 0, fontFamily: "Prompt" }}
                >
                  {this.state.accessapp.length != 0 &&
                    this.state.accessapp[2].views == 1 && (
                      <MDBDropdownItem>
                        {" "}
                        <MDBNavLink
                          to="/config"
                          activeClassName="activeClass"
                          style={{ color: "black" }}
                        >
                          <MDBIcon icon="user" className="mr-3" />{" "}
                          ตั้งราคาค่าขนส่ง
                        </MDBNavLink>
                      </MDBDropdownItem>
                    )}
                  {this.state.accessadmin == 1 && (
                    <MDBDropdownItem>
                      {" "}
                      <MDBNavLink
                        to="/config"
                        activeClassName="activeClass"
                        style={{ color: "black" }}
                      >
                        <MDBIcon icon="user" className="mr-3" />{" "}
                        ตั้งราคาค่าขนส่ง
                      </MDBNavLink>
                    </MDBDropdownItem>
                  )}
                  {this.state.accessapp.length != 0 &&
                    this.state.accessapp[3].views == 1 && (
                      <MDBDropdownItem>
                        {" "}
                        <MDBNavLink
                          to="/settingsys"
                          activeClassName="activeClass"
                          style={{ color: "black" }}
                        >
                          <MDBIcon icon="table" className="mr-3" />{" "}
                          ตั้งค่าระบบขนส่ง
                        </MDBNavLink>
                      </MDBDropdownItem>
                    )}
                  {this.state.accessadmin == 1 && (
                    <MDBDropdownItem>
                      {" "}
                      <MDBNavLink
                        to="/settingsys"
                        activeClassName="activeClass"
                        style={{ color: "black" }}
                      >
                        <MDBIcon icon="table" className="mr-3" />{" "}
                        ตั้งค่าระบบขนส่ง
                      </MDBNavLink>
                    </MDBDropdownItem>
                  )}
                    {this.state.accessapp.length != 0 &&
                    this.state.accessapp[3].views == 1 && (
                      <MDBDropdownItem>
                        {" "}
                        <MDBNavLink
                          to="/settingpilot"
                          activeClassName="activeClass"
                          style={{ color: "black" }}
                        >
                          <MDBIcon icon="user" className="mr-3" />{" "}
                          ตั้งค่าผู้ขับ
                        </MDBNavLink>
                      </MDBDropdownItem>
                    )}
                  {this.state.accessadmin == 1 && (
                    <MDBDropdownItem>
                      {" "}
                      <MDBNavLink
                        to="/settingpilot"
                        activeClassName="activeClass"
                        style={{ color: "black" }}
                      >
                        <MDBIcon icon="user" className="mr-3" />{" "}
                        ตั้งค่าผู้ขับ
                      </MDBNavLink>
                    </MDBDropdownItem>
                  )}
                </MDBDropdownMenu>
              </MDBDropdown>
            </MDBNavItem>
            {this.state.accessapp.length != 0 &&
              this.state.accessapp[4].views == 1 && (
                <MDBNavItem>
                  <MDBNavLink to="/loadprint">
                    &nbsp;&nbsp;{" "}
                    <MDBIcon icon="fas fa-print" className="mr-3" />
                    StockLoad
                  </MDBNavLink>
                </MDBNavItem>
              )}
            {this.state.accessadmin == 1 && (
              <MDBNavItem>
                <MDBNavLink to="/loadprint">
                  &nbsp;&nbsp; <MDBIcon icon="fas fa-print" className="mr-3" />
                  StockLoad
                </MDBNavLink>
              </MDBNavItem>
            )}
             <MDBNavItem>
              <MDBDropdown>
                {this.state.accessapp.length != 0 && this.state.accessapp[0].views == 1 && this.state.accessapp[1].views == 1 && (
                  <MDBDropdownToggle nav caret style={{ fontFamily: "Prompt" }}>
                    <div className="d-none d-md-inline">
                      &nbsp;&nbsp;สินค้ากลับ
                    </div>
                  </MDBDropdownToggle>
                )}
                {this.state.accessadmin == 1 && (
                  <MDBDropdownToggle nav caret style={{ fontFamily: "Prompt" }}>
                    <div className="d-none d-md-inline">
                      &nbsp;&nbsp;สินค้ากลับ
                    </div>
                  </MDBDropdownToggle>
                )}

                <MDBDropdownMenu
                  className="dropdown-default"
                  style={{ padding: 0, fontFamily: "Prompt" }}
                >
                  {this.state.accessapp.length != 0 &&
                    this.state.accessapp[0].views == 1 && (
                      <MDBDropdownItem>
                        {" "}
                        <MDBNavLink
                          to="/scanitem"
                          activeClassName="activeClass"
                          style={{ color: "black" }}
                        >
                          <MDBIcon icon="user" className="mr-3" />{" "}
                          ยิงสินค้ากลับเข้าระบบ
                        </MDBNavLink>
                      </MDBDropdownItem>
                    )}
                  {this.state.accessadmin == 1 && (
                    <MDBDropdownItem>
                      {" "}
                      <MDBNavLink
                        to="/scanitem"
                        activeClassName="activeClass"
                        style={{ color: "black" }}
                      >
                        <MDBIcon icon="user" className="mr-3" />{" "}
                        ยิงสินค้ากลับเข้าระบบ
                      </MDBNavLink>
                    </MDBDropdownItem>
                  )}
                  {this.state.accessapp.length != 0 &&
                    this.state.accessapp[1].views == 1 && (
                      <MDBDropdownItem>
                        {" "}
                        <MDBNavLink
                          to="/return"
                          activeClassName="activeClass"
                          style={{ color: "black" }}
                        >
                          <MDBIcon icon="table" className="mr-3" />{" "}
                          รายการสินค้ากลับ
                        </MDBNavLink>
                      </MDBDropdownItem>
                    )}
                  {this.state.accessadmin == 1 && (
                    <MDBDropdownItem>
                      {" "}
                      <MDBNavLink
                        to="/return"
                        activeClassName="activeClass"
                        style={{ color: "black" }}
                      >
                        <MDBIcon icon="table" className="mr-3" />{" "}
                        รายการสินค้ากลับ
                      </MDBNavLink>
                    </MDBDropdownItem>
                  )}
                </MDBDropdownMenu>
              </MDBDropdown>
            </MDBNavItem>
          </MDBNavbarNav>

          
          <MDBNavbarNav right>
            {this.state.accessapp.length != 0 && (
              <MDBNavLink to="/Login">
                {" "}
                <MDBIcon icon="user-times" className="mr-3" />
                ออกจากระบบ
              </MDBNavLink>
            )}
            {this.state.accessadmin == 1 && (
              <MDBNavLink to="/Login">
                {" "}
                <MDBIcon icon="user-times" className="mr-3" />
                ออกจากระบบ
              </MDBNavLink>
            )}
          </MDBNavbarNav>
        </MDBCollapse>
      </MDBNavbar>
    );
  }
}

export default TopNavigation;
