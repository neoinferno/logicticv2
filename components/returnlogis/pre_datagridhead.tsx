import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Datagridhead extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();
  private selectedRowIndex = React.createRef<HTMLSpanElement>();
  constructor(props: any) {
      super(props);
      this.myGridOnRowSelect = this.myGridOnRowSelect.bind(this);
  }
  public render() {
   
    const columns: any =[
       
        { text: 'เลขกล่อง ( Packrun )', pinned: true, editable: false,  datafield: 'loaddoc', cellsalign: 'center',align: 'center', width: '50%' },
        { text: 'จำนวน',editable: false, datafield: 'transportname', align: 'center', cellsalign: 'center',width: '50%' },
       
    ]
    const source: any = {
      datafields: [
          { name: 'loaddoc', type: 'string' },
          { name: 'transportname', type: 'string' },
       
      ],
      datatype: 'array',
      localdata:this.props.jobdoclist,
  };
      return (
        <JqxGrid
              onRowselect={this.myGridOnRowSelect}
              ref={this.myGrid}
              theme="metrodark" 
              showstatusbar={true}
              width={'50%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={false}
              editmode ={'click'} height={380}
              enabletooltips={true} editable={true} selectionmode={'singlerow'}
              showaggregates={true}
        />
      );
  }

  private myGridOnRowSelect(event: any): void {
    let value = this.myGrid.current!.getrowdata(event.args.rowindex);
    this.props.setCode_body(value.loaddoc);
  };


}
export default Datagridhead;