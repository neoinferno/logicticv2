import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";
// import { MDBIcon } from "mdbreact";
import red from "./red0.png";
import yellow from "./yellow1.png";
import green from "./green2.png";

import JqxGrid, {
    IGridProps,
    jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Datagrid extends React.PureComponent<any, IGridProps> {
    private myGrid = React.createRef<JqxGrid>();


    constructor(props: any) {
        super(props);


    }

    public render() {
        const cellsrenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
            if (value === "ปิดงานแล้ว") {
                return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:orange;"><strong>' + value + '</strong></div>';
            } else if (value === "ยกเลิกเอกสาร") {
                return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:red;"><strong>' + value + '</strong></div>';
            }
            else {
                return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:green;"><strong>' + value + '</strong></div>';
            }
        };
        const imagerenderer = (row: number, datafield: string, value: number, rowdata: any, columnproperties: any, data: any): any => {
            // console.log('totalsign ' + data.totalsign);
            // console.log('nullsign ' + data.nullsign);
            console.log(data);

            // return '<img style="margin: 5px;" height=80% width=80% src="./' + right + '"/>';
            if (data.nullsign == 0) {
                return '<div style="text-align:center"><img style="width:55%;margin-Top:5%" src="' + green + '"/></div>';
            } else {
                if (data.nullsign < data.totalsign) {
                    return '<div style="text-align:center"><img style="width:55%;margin-Top:5%" src="' + yellow + '"/></div>';
                } else {
                    if (data.nullsign == data.totalsign) {
                        return '<div style="text-align:center"><img style="width:55%;margin-Top:5%" src="' + red + '"/></div>';

                    } else {
                        console.log('###');
                    }
                }
            }

        };
        const columns: any = [
            { text: 'เซ็นรับ', editable: false, datafield: 'totalsign', filterable: false, cellsrenderer: imagerenderer, align: 'center', cellsalign: 'center', width: '3%' },
            { text: 'เลขที่เอกสาร', editable: false, datafield: 'doc_code', cellsalign: 'center', align: 'center', width: '7%', aggregates: ['count'] },
            { text: 'ขนส่ง', editable: false, datafield: 'type_logis', align: 'center', cellsalign: 'center', width: '6%' },
            { text: 'id', editable: false, datafield: 'sup_id', hidden: true },
            { text: 'วันที่เอกสาร', editable: false, datafield: 'date_doc', align: 'center', cellsalign: 'center', width: '7%' },
            { text: 'บริษัทขนส่ง', editable: false, datafield: 'sup_name', align: 'center', cellsalign: 'center', width: '7%' },
            { text: 'ทะเบียนรถ', editable: false, datafield: 'car_number', align: 'center', cellsalign: 'center', width: '5%' },
            { text: 'ชื่อผู้ขับ', editable: false, datafield: 'car_name', align: 'center', cellsalign: 'center', width: '10%' },
            { text: 'เด็กติดรถ', editable: false, datafield: 'carboy', align: 'center', cellsalign: 'center', width: '10%' },
            { text: 'สายรถ', editable: false, datafield: 'car_rout', align: 'center', cellsalign: 'center', width: '5%' },
            { text: 'เบอร์โทร', editable: false, datafield: 'car_phone', align: 'center', cellsalign: 'center', width: '5%' },
            { text: 'สถานะ', editable: false, datafield: 'doc_status', cellsrenderer, align: 'center', cellsalign: 'center', width: '5%' },
            { text: 'ค่าขนส่ง', editable: false, datafield: 'logis_price', cellsformat: 'n', align: 'center', cellsalign: 'center', width: '5%', aggregates: ["sum"] },
            { text: 'จำนวนสาขา', editable: false, datafield: 'branch', align: 'center', cellsalign: 'center', width: '5%', aggregates: ["sum"] },
            { text: 'จำนวนกล่อง', editable: false, datafield: 'box', align: 'center', cellsalign: 'center', width: '5%', aggregates: ['sum'] },
            {
                text: 'พิมพ์ใบ', editable: false, filterable: false, columntype: 'button', buttonclick: (row: number): void => {

                    this.props.loadprint(this.myGrid.current!.getrowdata(row));

                },
                cellsrenderer: (): string => {
                    return 'พิมพ์';
                }, align: 'center', cellsalign: 'center', width: '5%'
            },
            {
                text: 'แก้ไข', editable: false, filterable: false, columntype: 'button', buttonclick: (row: number): void => {

                    this.props.toggle(this.myGrid.current!.getrowdata(row));

                },
                cellsrenderer: (): string => {
                    return 'แก้ไข';
                }, align: 'center', cellsalign: 'center', width: '5%'
            },
            {
                text: 'ยกเลิก', editable: false, filterable: false, columntype: 'button', buttonclick: (row: number): void => {
                    var r = confirm("คุณต้องการยกเลิกเอกสารนี้ใช่หรือไม่ ?");
                    if (r == true) {
                        this.props.CancelDocjob(this.myGrid.current!.getrowdata(row));
                    }


                },
                cellsrenderer: (): string => {
                    return 'ยกเลิก';
                }, align: 'center', cellsalign: 'center', width: '5%'
            },

        ]
        const source: any = {
            datafields: [
                { name: 'totalsign', type: 'number' },
                { name: 'nullsign', type: 'number' },
                { name: 'doc_code', type: 'string' },
                { name: 'type_logis', type: 'string' },
                { name: 'date_doc', type: 'string' },
                { name: 'sup_name', type: 'string' },
                { name: 'sup_id', type: 'string' },
                { name: 'car_number', type: 'string' },
                { name: 'car_name', type: 'string' },
                { name: 'carboy', type: 'string' },
                { name: 'car_phone', type: 'string' },
                { name: 'doc_status', type: 'string' },
                { name: 'logis_price', type: 'number' },
                { name: 'branch', type: 'number' },
                { name: 'box', type: 'number' },
                { name: 'car_rout', type: 'string' },

            ],
            datatype: 'array',
            localdata: this.props.datadoc,
        };

        return (

            <JqxGrid
                ref={this.myGrid}

                theme="metrodark"
                showstatusbar={true}
                width={'99%'} source={new jqx.dataAdapter(source)} columns={columns}
                pageable={false} autoheight={false} sortable={false} altrows={true}
                editmode={'click'} height={450}
                enabletooltips={true} editable={true} selectionmode={'singlecell'}
                showaggregates={true}
            />

        );
    }





}
export default Datagrid;