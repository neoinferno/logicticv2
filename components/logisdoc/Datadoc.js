import React, { Component } from "react";
import {
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBCardFooter
} from "mdbreact";
import { withSnackbar } from "notistack";
import { Box, RadioButton } from "grommet";
import Grid from "@material-ui/core/Grid";
import { MDBBtn, MDBIcon, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBBtnGroup, MDBDropdown, MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem } from "mdbreact";
import LinearProgress from "@material-ui/core/LinearProgress";
import LoadingOverlay from "react-loading-overlay";
import { TextInput } from "grommet";
import {
  ExcelExport,
  ExcelExportColumn
} from "@progress/kendo-react-excel-export";
import Datagridhead from "./datagridhead.tsx";
import Select from "react-select";
import Datagrid from "./datagrid.tsx";
import ApiService from "../actions/apidata";
import AuthService from "../authlogin/AuthService";
import jiblogo from "../img/logo-logistics3.png";
import Datetime1 from "./datetime1.tsx";
import Datetime2 from "./datetime2.tsx";

class Datadoc extends Component {
  _exporter;
  constructor(props) {
    super(props);
    this.senddata = this.senddata.bind(this);
    this.searchdata = this.searchdata.bind(this);
    this.export = this.export.bind(this);
    this.export2 = this.export2.bind(this);
    this.toggle = this.toggle.bind(this);
    this.toggle2 = this.toggle2.bind(this);
    this.setValueddl = this.setValueddl.bind(this);
    this.setValueddl2 = this.setValueddl2.bind(this);
    this.setValueddl3 = this.setValueddl3.bind(this);
    this.CancelDocjob = this.CancelDocjob.bind(this);
    this.Updatedata = this.Updatedata.bind(this);
    this.getDatenow = this.getDatenow.bind(this);
    this.load_print = this.load_print.bind(this);
    this.driverdata = this.driverdata.bind(this);
    this.fildata = this.fildata.bind(this);
    // this.sendttx = this.sendttx.bind(this);
    this.savedata = this.savedata.bind(this);
    this.setDatagrid = this.setDatagrid.bind(this);
    this.setDatagrid2 = this.setDatagrid2.bind(this);
    this.getGriddata = this.getGriddata.bind(this);
    this.carboydata = this.carboydata.bind(this);

    this.state = {
      completed: 0,
      active: false,

      typeoption: [],
      driverdatafill: [],
      car_name: "-- เลือกรายชื่อคนขับ--",
      modal: false,
      buffer: 10,
      mdata: [],
      headdata: [],
      dataex: [],
      rowdata: [],
      beedata:[],
      selected: "",
      dataprint: [],
      transportdata: [],
      mdata: [],
      carboydata: [],
      transelect: [],
      tran: "-- เลือกรายชื่อบริษัท--",
      tran2: "",
      jobdoclistbody: [],
      jobdoclist: [],
      driverdata: [],
      profile: {},
      doccode: "",
      lastkey: "",
      lastkey2: "",
      personalname: "",
      carname: "",
      carnumber: "",
      car_phone: "",
      typelogis: "",
      supname: "",
      car_name2: "",
      datedoc: "",
      carphone: "",
      dateprint: "",
      datacarname: "",
      datacarboy: "",
      datacarphone: "",
      doc_pack: "",
      doc_number: "",
      datacarnumber: "",
      datatotalload: "",
      dataload: "",
      dccode: "",
      idsup: "",
      dataroute: "",
      datasupname: "",

      searchsup: "",
      searchdoc: "",
      searchroute: "",
      dateselect1: this.getDatenow(),
      dateselect2: this.getDatenow()
    };
    this.Auth = new AuthService();
    this.ApiCall = new ApiService();
  }
  carboydata() {
    this.ApiCall.Showdriverlist()
      .then(res => {
        if (res.status === true) {
          this.setState({ carboydata: res.carboy });
          console.log(this.state.carboydata)
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  toggle = (data) => {
    this.setState({
      modal: !this.state.modal,
      dccode: data.doc_code,
      carnumber: data.car_number,
      supname: data.sup_name,
      typelogis: data.type_logis,
      tran: data.sup_name,
      tran2: data.sup_id,
      car_name2: data.carboy,

      car_name: data.car_name,
      car_phone: data.car_phone
    });
    this.fildata()
    this.getGriddata()
    this.carboydata();
    // console.log(this.state.car_name)
  }
  toggle2 = (data) => {
    this.setState({
      modal: !this.state.modal

    });

  }
  export() {
    this._exporter.save();
  }
  export2() {
    this._exporter2.save();
  }
  beedata() {
    var excels = Array();
    excels = {

      service: "ND",

      canton: 1
    };
    this.setState({
      beedata: excels

    });
    console.log(this.state.beedata)
  }


  getDatenow() {
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + month + "-" + day;
    return today;
  }
  componentDidMount() {
    if (!this.Auth.loggedIn()) {
      window.location.href = "/Login";
    } else {
      let profile = this.Auth.getProfile();
      this.setState({ profile: profile });
      // console.log(profile)
    }
    this.senddata();

    this.ApiCall.DDl_GetSelectType()
      .then(res => {
        if (res.status === true) {
          this.setState({ typeoption: res.data });
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });

    this.ApiCall.gettransport()
      .then(res => {
        if (res.status === true) {
          this.setState({ transportdata: res.data });
          // console.log(this.state.transportdata)
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
    this.driverdata()

  }

  Updatedata(typedate, rows) {
    if (typedate === 0) {
      this.setState(
        {
          dateselect1: rows
        },
        () => {
          // console.log(this.state.dateselect1);
          // this.Getreportnow();
        }
      );
    } else {
      this.setState(
        {
          dateselect2: rows
        },
        () => {
          // console.log(this.state.dateselect2);
          // console.log(this.state.dateselect1)
          // this.Getreportnow();
        }
      );
    }
  }

  searchdata() {
    this.senddata();
  }
  senddata() {
    var docvar = {
      doc_code: this.state.searchdoc,
      sup_name: this.state.searchsup,
      route: this.state.searchroute,
      dateselect1: this.state.dateselect1,
      dateselect2: this.state.dateselect2
    };
    this.setState({ active: true })
    this.ApiCall.GetdataLogisDoc(docvar)
      .then(res => {
        if (res.status === true) {
          this.setState({ mdata: res.data }, () => {
            // this.doc_number.focus();
            setTimeout(() => {
              this.setState({ active: false })
            }, 3000)
          });
        } else {
          console.log(res.message);
          this.setState({ active: false })
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  clear() {
    this.setState(
      {
        searchdoc: "",
        searchsup: ""
      },
      () => { }
    );
  }
  // sendttx() {
  //   if (window.confirm("คุณต้องการส่งAPIใช่หรือไม่")) {
  //     window.open("http://172.18.0.30/runjob/index.php?r=testconn/Logistics");
  //   }
   
   
    
  // }
  load_print(data) {
    this.getexcel(data);
    this.setState(
      {
        doccode: data.doc_code,
        carname: data.car_name,
        carphone: data.car_phone,
        datedoc: data.date_doc
      },
      () => {
        this.timeout = setTimeout(() => {
          var content = document.getElementById("printarea");
          var pri = document.getElementById("ifmcontentstoprint").contentWindow;
          pri.document.open();
          pri.document.write(content.innerHTML);
          pri.document.close();

          pri.print();
        }, 500);
      }
    );
  }

  CancelDocjob(jobdoc) {
    // console.log(jobdoc.doc_code)
    this.ApiCall.CancelDocjob(jobdoc.doc_code)
      .then(res => {
        if (res.status === true) {
          // window.location.reload();
          this.props.enqueueSnackbar("ระบบยกเลิกรายการ สำเร็จแล้ว ", {
            variant: "success"
          });
          this.searchdata();
          // this.setTimeout(
          //     window.location.reload(),
          //   100000
          // );
        } else {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  getGriddata() {
    let docvar = Array();
    docvar = {

      doc_code: this.state.dccode
    };

    this.ApiCall.Receivelogisticsdochead(docvar)
      .then(res => {
        if (res.status === true) {
          this.setState({ jobdoclist: res.Head }, () => {
            // this.doc_number.focus();
          });
        } else {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });
    var data = this.state.doc_code;


  }
  getexcel(data) {
    var datadoc = data.doc_code;

    this.ApiCall.getPrintregister(datadoc)
      .then(res => {
        if (res.status === true) {
          this.setState(
            {
              headdata: res.headdata,
              rowdata: res.rowdata,
              dataex: res.data,
              dataprint: res.docdata
            },
            () => { }
          );
          this.setState(
            {
              dateprint: this.state.dataprint[0].date_doc,
              datacarname: this.state.dataprint[0].car_name,
              datacarboy: this.state.dataprint[0].carboy,
              datacarphone: this.state.dataprint[0].car_phone,
              datacarnumber: this.state.dataprint[0].car_number,
              dataroute: this.state.dataprint[0].route,
              datacount: this.state.dataex[0].countbox,
              dataload: this.state.rowdata[0].loaddoc,
              datatotalload: this.state.rowdata[0].total,
              datasupname: this.state.dataprint[0].sup_name
            },
            () => { }
          );
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });


  }
  setValueddl(listname) {



    this.setState(
      {

        car_name: listname.target.value,

      }, () => {
        var drive = this.state.car_name;
        let result = this.state.driverdata.filter(function (item) {
          return item.listname == (drive);

        });

        this.setState({ car_phone: result[0].phone });
        console.log(this.state.car_name)
      }

    )

  }
  setValueddl3(listname) {
    this.setState(
      {
        car_name2: listname.target.value
      },
      () => {
        console.log(this.state.car_name2)
      }
    );

  }
  setValueddl2(label) {

    // console.log(label.target.id)

    this.setState(
      {

        tran: label.target.value,
        tran2: label.target.id
        // tranname:label.target.value
        // console.log(this.state.tran)

      }, () => {
        //   var drive =this.state.tran;
        //   let result = this.state.driverdata.filter(function(item) {
        //   return item.listname==(drive);

        // });

        // // this.setState({ car_phone:result[0].phone });
        // console.log(this.state.tran2)
        // console.log(this.state.tran)
      }

    )

  }
  fildata() {

    var drive = this.state.tran;
    let result = this.state.driverdata.filter(function (item) {
      return item.transportname == (drive);

    });
    // console.log(result[0].phone)
    this.setState({ driverdatafill: result });
    // console.log(this.state.driverdatafill)
    // console.log(this.state.transelect[0].value)


  }
  driverdata() {
    this.ApiCall.Showdriverlist()
      .then(res => {
        if (res.status === true) {
          this.setState({ driverdata: res.data });
          //  console.log(this.state.driverdata)
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  savedata() {
    let docvar = Array();
    if (
      this.state.car_name === "-- เลือกรายชื่อบริษัท--" ||
      this.state.car_number === "" ||
      this.state.selected === "" ||
      this.state.car_name === "-- เลือกรายชื่อคนขับ--" ||
      this.state.car_phone === ""
    ) {
      this.props.enqueueSnackbar(
        "โปรดป้อนข้อมูลให้ครบก่อน..ตรวจสอบดูให้แน่ใจ",
        {
          variant: "error"
        }
      );
    } else {
      docvar = {
        doc_code: this.state.dccode,
        sup_id: this.state.tran2,
        sup_name: this.state.tran,
        logis_type: this.state.selected,
        car_number: this.state.carnumber,
        car_name: this.state.car_name,
        userlog: this.state.profile.username,
        carboy: this.state.car_name2,
        car_phone: this.state.car_phone
      };

      console.log(docvar)
      this.ApiCall.UpdateDoc(docvar)
        .then(res => {
          if (res.status === true) {
            this.props.enqueueSnackbar(
              "บันทึกรายการเรียบร้อยแล้วค่ะ ",
              {
                variant: "success"
              }
            );
            this.searchdata()
            this.toggle2()
          } else {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          }
        })
        .catch(error => {
          console.error(error.message);
        });

    }
  }
  setDatagrid2() {

    let docvar = Array();
    docvar = {

      packdoc: this.state.doc_pack,
      doc_code: this.state.dccode
    };

    this.ApiCall.PackGetBoxInsert(docvar)
      .then(res => {
        if (res.status === true) {
          this.getGriddata();
        } else {
          this.props.enqueueSnackbar(
            "ไม่พบข้อมูลงานนี้จากระบบ..ตรวจสอบดูให้แน่ใจ",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  setDatagrid() {


    let docvar = Array();
    docvar = {

      loaddoc: this.state.doc_number,

      doc_code: this.state.dccode
    };



    this.ApiCall.InsertDocHead(docvar)
      .then(res => {
        if (res.status === true) {
          this.getGriddata();
        } else {
          this.props.enqueueSnackbar(
            "ไม่พบข้อมูลงานนี้จากระบบ..ตรวจสอบดูให้แน่ใจ",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });

  }
  render() {
   
   
    let test = Array();
    test = this.state.dataex;

    function checkvalue(rowdata, headdata) {
      for (var i = 0; i < test.length; i++) {
        if (test[i].branchname == rowdata && test[i].loadname == headdata) {
          return test[i].countbox;
        }
      }
    }
    const list = this.state.rowdata.map(rowdata => (
      <tr>
        <th>{rowdata.branchname}</th>
        <th />
        {this.state.headdata.map(function (headdata) {
          return <th>{checkvalue(rowdata.branchname, headdata.loadname)}</th>;
        })}
      </tr>
    ));

    const listtitle = this.state.rowdata.map(rowdata => (
      <tr align="center">
        <td> {rowdata.loaddoc} </td>
        <td> {rowdata.total} </td>
      </tr>
    ));

    const listdata = this.state.driverdatafill.map(driverdatafill => (
      <MDBDropdownItem onClick={this.setValueddl} value={driverdatafill.listname}>
        {driverdatafill.listname}

      </MDBDropdownItem>
    ));
    const listdata2 = this.state.carboydata.map(carboydata => (
      <MDBDropdownItem
        onClick={this.setValueddl3}
        value={carboydata.listname}
      >
        {carboydata.listname}
      </MDBDropdownItem>
    ));
    const listdatatran = this.state.transportdata.map(transportdata => (
      <MDBDropdownItem onClick={this.setValueddl2} value={transportdata.label} id={transportdata.value}>
        {transportdata.label}

      </MDBDropdownItem>
    ));

    // const listtitle = this.state.valueddl.map(valueddl => (
    //   <tr align="center">
    //     <td> {valueddl.loaddoc} </td>
    //     <td> {valueddl.total} </td>
    //   </tr>
    // ));

    const { completed } = this.state;

    return (
      <div>
        <MDBRow>

          <MDBCol md="12">
            <MDBCard>
              <MDBCardBody style={{ fontFamily: "Prompt" }}>
                <Grid container spacing={24}>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 3 }}
                  >
                    <h4 style={{ color: "green" }}>
                      <MDBIcon icon="paste" className="pink-text pr-3" />
                      &nbsp;ตารางข้อมูลขนส่ง (Data Doc)
                    </h4>
                    <h5 style={{ color: "#8089a9", textAlign: "right" }}>
                      <MDBIcon icon="user-edit" className="cyan-text pr-3" />
                      &nbsp;
                    </h5>
                  </Grid>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0 }}
                  >
                    <LinearProgress
                      color="secondary"
                      variant="determinate"
                      value={completed}
                    />
                    <hr />
                  </Grid>

                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0 }}
                  >
                    <MDBCard
                      style={{ width: "100%", marginTop: "0" }}
                      className="text-center"
                    >
                      <MDBCardHeader
                        style={{
                          borderBottomWidth: "0px",
                          backgroundColor: "#272262"
                        }}
                      >
                        <Grid container spacing={24}>
                          <Grid
                            item
                            lg={1}
                            xl={1}
                            xs={1}
                            sm={1}
                            md={1}
                            style={{
                              padding: 5,
                              marginLeft: 0,
                              paddingTop: 15
                            }}
                          >
                            <h5 style={{ color: "white", fontSize: 16 }}>
                              วันที่เอกสาร
                            </h5>
                          </Grid>
                          <Grid
                            item
                            lg={1}
                            xl={1}
                            xs={1}
                            sm={1}
                            md={1}
                            style={{
                              padding: 0,
                              marginLeft: 5,
                              paddingTop: 15
                            }}
                          >
                            <Datetime1 Updatedata={this.Updatedata} />
                          </Grid>
                          <Grid
                            item
                            lg={1}
                            xl={1}
                            xs={1}
                            sm={1}
                            md={1}
                            style={{
                              padding: 0,
                              marginLeft: 5,
                              paddingTop: 15
                            }}
                          >
                            <Datetime2 Updatedata={this.Updatedata} />
                          </Grid>
                          <Grid
                            item
                            lg={1}
                            xl={1}
                            xs={1}
                            sm={1}
                            md={1}
                            style={{ padding: 10, marginLeft: 20 }}
                          >
                            <TextInput
                              style={{
                                width: "100%",
                                fontSize: 15,
                                backgroundColor: "white",
                                color: "black"
                              }}
                              size="xsmall"
                              placeholder="ค้นหาเลขที่เอกสาร"
                              value={this.state.searchdoc}
                              ref={input => {
                                this.searchdoc = input;
                              }}
                              onChange={event =>
                                this.setState(
                                  { searchdoc: event.target.value },
                                  () => { }
                                )
                              }
                              onKeyPress={event => {
                                if (event.key === "Enter") {
                                  {
                                    this.uppss();
                                  }
                                }
                              }}
                            />
                          </Grid>
                          <Grid
                            item
                            lg={1}
                            xl={1}
                            xs={1}
                            sm={1}
                            md={1}
                            style={{ padding: 10 }}
                          >
                            <TextInput
                              style={{
                                width: "100%",
                                fontSize: 15,
                                backgroundColor: "white",
                                color: "black"
                              }}
                              size="xsmall"
                              placeholder="ค้นหาบริษัทขนส่ง"
                              value={this.state.searchsup}
                              ref={input => {
                                this.searchsup = input;
                              }}
                              onChange={event =>
                                this.setState(
                                  { searchsup: event.target.value },
                                  () => { }
                                )
                              }
                              onKeyPress={event => {
                                if (event.key === "Enter") {
                                  {
                                  }
                                }
                              }}
                            />
                          </Grid>
                          <Grid
                            item
                            lg={1}
                            xl={1}
                            xs={1}
                            sm={1}
                            md={1}
                            style={{ padding: 10 }}
                          >
                            <TextInput
                              style={{
                                width: "100%",
                                fontSize: 15,
                                backgroundColor: "white",
                                color: "black"
                              }}
                              size="xsmall"
                              placeholder="ค้นหาจากสายรถ"
                              value={this.state.searchroute}
                              ref={input => {
                                this.searchroute = input;
                              }}
                              onChange={event =>
                                this.setState(
                                  { searchroute: event.target.value },
                                  () => { }
                                )
                              }
                              onKeyPress={event => {
                                if (event.key === "Enter") {
                                  {
                                  }
                                }
                              }}
                            />
                          </Grid>
                          <Grid
                            item
                            lg={5}
                            xl={5}
                            xs={5}
                            sm={5}
                            md={5}
                            style={{ padding: 10 }}
                          >
                            <MDBBtn
                              color="default"
                              onClick={() => {
                                this.searchdata();
                              }}
                            >
                              ค้นหา <MDBIcon icon="search" className="ml-1" />
                            </MDBBtn>
                            <MDBBtn
                              color="primary"
                              onClick={() => {
                                this.clear();
                              }}
                            >
                              <MDBIcon icon="undo" className="mr-1" /> ล้างข้อมูล
                          </MDBBtn>
                            <MDBBtn
                              color="secondary"
                              onClick={() => {
                                this.export();
                              }}
                            >
                              <MDBIcon
                                icon="calendar-alt"
                                className="mr-1"
                                size="sm"
                              />{" "}
                              Excel
                          </MDBBtn>
                          <MDBBtn
                              color="red"
                              onClick={() => {
                                this.export2();
                              }}
                            >
                              <MDBIcon
                                icon="calendar-alt"
                                className="mr-1"
                                size="sm"
                              />{" "}
                              ExcelMRBEE
                          </MDBBtn>
                          {/* <MDBBtn
                              color="red"
                              onClick={() => {
                                this.sendttx();
                                
                              }}
                            
                            >
                              <MDBIcon
                                icon="calendar-alt"
                                className="mr-1"
                                size="sm"
                              />{" "}
                              API
                          </MDBBtn> */}
                          </Grid>

                        </Grid>
                      </MDBCardHeader>
                      <MDBCardBody
                        style={{ padding: 12, backgroundColor: "#272262" }}
                      >
                        <Grid container spacing={24}>
                          <Grid
                            item
                            lg={12}
                            xl={12}
                            xs={12}
                            sm={12}
                            md={12}
                            style={{ padding: 0 }}
                          >
                            <center>
                              <LoadingOverlay active={this.state.active} spinner text="กำลังโหลด...">
                                <Datagrid
                                  datadoc={this.state.mdata}
                                  loadprint={this.load_print}
                                  CancelDocjob={this.CancelDocjob}
                                  toggle={this.toggle}
                                />
                              </LoadingOverlay>
                            </center>
                          </Grid>
                        </Grid>
                      </MDBCardBody>
                      <MDBCardFooter
                        style={{
                          borderTopWidth: "0px",
                          backgroundColor: "#272262",
                          color: "white"
                        }}
                      />
                    </MDBCard>
                  </Grid>
                </Grid>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>

          <div style={{ margin: "0px" }} style={{ display: "none" }}>
            <iframe
              id={"ifmcontentstoprint"}
              style={{
                margin: "0px",
                height: "0px",
                width: "0px",
                position: "absolute"
              }}
            />
            <div id={"printarea"} style={{ margin: 0 }}>
              {/* <br /> */}
              {/* <table
                align="center"
                border="0"
                width="50%"
                style={{ "border-collapse": "collapse", fontSize: "24px" }}
              >
                <tr>
                  <th>ใบแสดงจำนวนรายการขนส่ง</th>

                
                </tr>
               
              </table> */}
              <br />
              <table
                border="0"
                width="100%"
                style={{ "border-collapse": "collapse", fontSize: "12px" }}
              >
                <tr>
                  <td align="left">
                    <img
                      style={{ height: 50 }}
                      alt="MDB React Logo"
                      className="img-fluid"
                      src={jiblogo}
                    />
                  </td>
                  <td align="right">
                    บริษัท เจ.ไอ.บี.คอมพิวเตอร์ กรุ๊ป จำกัด <br />
                    เลขที่21 ถ.พหลโยธิน แขวงสนามบิน เขตดอนเมือง กรุงเทพฯ
                  </td>
                </tr>
                <br />

                <tr>
                  <td />
                  <td align="right" />
                </tr>
              </table>

              <br />

              <div className="full">
                <table
                  align="center"
                  border="0"
                  width="100%"
                  style={{ "border-collapse": "collapse", fontSize: "14px" }}
                >
                  <tr align="left">
                    <td>ชื่อผู้ขับ:{this.state.datacarname}</td>
                    <td>เด็กติดรถ:{this.state.datacarboy}</td>
                    <td>เบอร์โทร:{this.state.datacarphone}</td>
                    <td>ทะเบียนรถ:{this.state.datacarnumber}</td>
                  </tr>
                  <tr align="left">
                    <td>เอกสาร:{this.state.doccode}</td>
                    <td>บริษัท:{this.state.datasupname}</td>
                    <td>เวลาพิมพ์:{this.state.dateprint}</td>
                  </tr>

                  <tr align="left">
                    <td> เส้นทาง: {this.state.dataroute} </td>
                  </tr>
                </table>

                <table
                  align="center"
                  border="1"
                  width="100%"
                  style={{ "border-collapse": "collapse", fontSize: "12px" }}
                >
                  <tr>
                    <th>รหัส</th>
                    <th>จำนวนกล่อง</th>
                    {listtitle}
                    <td> </td>
                    <th> รวม( {this.state.datacount} )กล่อง</th>
                  </tr>

                  {/* {list} */}
                </table>
                <br />

                <br />
                <br />
                <br />
                <table width="100%">
                  <tr>
                    <td align="center">_____________________</td>
                    <td align="center">_____________________</td>
                  </tr>
                  <tr>
                    <td align="center">ผู้ตรวจสอบ</td>
                    <td align="center">ผู้ขนส่ง</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </MDBRow>

        <ExcelExport
        
          data={this.state.mdata}
          // group={group}
          fileName={
            "ข้อมูลขนส่ง " +
            this.state.dateselect1 +
            "_" +
            this.state.dateselect2 +
            ".xlsx"
          }
          ref={exporter => {
            this._exporter = exporter;
          }}
        >
          <ExcelExportColumn
            field="doc_code"
            title="เลขที่เอกสาร"
            locked={true}
            width={150}
          />
          <ExcelExportColumn field="type_logis" title="ขนส่ง" width={150} />
          <ExcelExportColumn field="date_doc" title="วันที่ขนส่ง" width={150} />
          <ExcelExportColumn field="sup_name" title="บริษัท" width={200} />
          <ExcelExportColumn field="car_number" title="ทะเบียนรถ" width={200} />
          <ExcelExportColumn field="car_rout" title="เส้นทาง" width={150} />
          <ExcelExportColumn field="sup_name" title="บริษัท" width={200} />
          <ExcelExportColumn field="car_number" title="ทะเบียนรถ" width={100} />
          <ExcelExportColumn field="car_name" title="ชื่อคนขับ" width={100} />
          <ExcelExportColumn field="car_phone" title="เบอร์โทร" width={100} />
          <ExcelExportColumn field="doc_status" title="สถานะ" width={100} />
          <ExcelExportColumn field="logis_price" title="ค่าขนส่ง" width={100} />
          <ExcelExportColumn field="branch" title="จำนวนสาขา" width={100} />
          <ExcelExportColumn field="box" title="จำนวนกล่อง" width={100}  />
        </ExcelExport>
        

     
     
     
     
        <ExcelExport
           data={this.state.mdata}
          
          // group={group}
          fileName={
            "ข้อมูลขนส่งMRbee " +
            this.state.dateselect1 +
            ".xlsx"
          }
          ref={exporter => {
            this._exporter2 = exporter;
          }}
        >
          <ExcelExportColumn
            field=""
            title="Consignname"
            locked={true}
            width={150}
          />
          <ExcelExportColumn field="" title="ACC ID" width={150} />
          <ExcelExportColumn field="" title="Customer" width={150} />
          <ExcelExportColumn field="" title="Contact" width={200} />
          <ExcelExportColumn field="" title="Address" width={200} />
          <ExcelExportColumn field="" title="Zipcode" width={150} />
          <ExcelExportColumn field="carton" title="Carton" width={200} />
          <ExcelExportColumn field="service" title="Service" width={100}/>
         
        </ExcelExport>



        <MDBModal isOpen={this.state.modal} toggle={this.toggle} size="fluid" >
          <MDBModalHeader toggle={this.toggle}>แก้ไขเอกสาร({this.state.dccode})</MDBModalHeader>
          <MDBModalBody>

            <MDBRow>
              <MDBCol md="12">
                <MDBCard>
                  <MDBCardBody style={{ fontFamily: "Prompt" }}>
                    <Grid container spacing={24}>
                      <Grid
                        item
                        lg={12}
                        xl={12}
                        xs={12}
                        sm={12}
                        md={12}
                        style={{ padding: 3 }}
                      >

                      </Grid>
                      <Grid
                        item
                        lg={12}
                        xl={12}
                        xs={12}
                        sm={12}
                        md={12}
                        style={{ padding: 10, paddingTop: 0 }}
                      >
                        <LinearProgress
                          color="secondary"
                          variant="determinate"
                          value={completed}
                        />
                        <hr
                          style={{ paddingBottom: 0, marginBottom: 0, marginTop: 10 }}
                        />
                      </Grid>
                      <Grid
                        item
                        lg={4}
                        xl={4}
                        xs={4}
                        sm={4}
                        md={4}
                        style={{ padding: 10 }}
                      >
                        <Grid container spacing={24} style={{ margin: 0 }}>
                          <Grid
                            item
                            lg={6}
                            xl={6}
                            xs={6}
                            sm={6}
                            md={6}
                            style={{ padding: 3 }}
                          >
                            <h5
                              style={{
                                color: "#8089a9",
                                textAlign: "right"
                              }}
                            >
                              เลือกบริษัทขนส่ง{" "}
                            </h5>
                          </Grid>
                          <Grid
                            item
                            lg={6}
                            xl={6}
                            xs={6}
                            sm={6}
                            md={6}
                            style={{ padding: 3 }}
                          >
                            <MDBBtnGroup>
                              <MDBDropdown>
                                <MDBDropdownToggle
                                  caret
                                  color="primary " outline rounded
                                  className="h-100"
                                >
                                  {this.state.tran}
                                </MDBDropdownToggle>
                                <MDBDropdownMenu basic color="danger">
                                  {listdatatran}
                                </MDBDropdownMenu>
                              </MDBDropdown>
                            </MDBBtnGroup>

                          </Grid>
                          <Grid
                            item
                            lg={12}
                            xl={12}
                            xs={12}
                            sm={12}
                            md={12}
                            style={{ padding: 3, marginTop: 30 }}
                          >

                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid
                        item
                        lg={4}
                        xl={4}
                        xs={4}
                        sm={4}
                        md={4}
                        style={{ padding: 10 }}
                      >
                        <Grid container spacing={24}>
                          <Grid
                            item
                            lg={6}
                            xl={6}
                            xs={6}
                            sm={6}
                            md={6}
                            style={{ padding: 3 }}
                          >
                            <h5
                              style={{
                                color: "#8089a9",
                                textAlign: "right",
                                marginRight: 0,
                                paddingRight: 0,
                                marginTop: 0
                              }}
                            >
                              ประเภทของค่าขนส่ง{" "}
                            </h5>
                          </Grid>
                          <Grid
                            item
                            lg={6}
                            xl={6}
                            xs={6}
                            sm={6}
                            md={6}
                            style={{ padding: 10 }}
                          >
                            <Box align="start">
                              {this.state.typeoption.map(label => (
                                <Box
                                  key={label.id}
                                  margin={{ vertical: "small" }}
                                  style={{ marginTop: 3, marginBottom: 3 }}
                                >
                                  <RadioButton
                                    name="prop"
                                    checked={this.state.selected === label.id}
                                    label={label.type_logis}
                                    onChange={() =>

                                      this.setState({ selected: label.id })

                                    }
                                  />
                                </Box>
                              ))}
                            </Box>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid
                        item
                        lg={4}
                        xl={4}
                        xs={4}
                        sm={4}
                        md={4}
                        style={{ padding: 10 }}
                      >
                        <Grid container spacing={24}>
                          <Grid
                            item
                            lg={4}
                            xl={4}
                            xs={4}
                            sm={4}
                            md={4}
                            style={{ padding: 3 }}
                          >
                            <h5
                              style={{
                                color: "#8089a9",
                                textAlign: "right"
                              }}
                            >
                              ทะเบียนรถ{" "}
                            </h5>
                          </Grid>
                          <Grid
                            item
                            lg={6}
                            xl={6}
                            xs={6}
                            sm={6}
                            md={6}
                            style={{ padding: 3 }}
                          >
                            <TextInput
                              style={{
                                width: "100%",
                                fontSize: 15,
                                backgroundColor: "white",
                                color: "black"
                              }}
                              size="xsmall"
                              value={this.state.carnumber}
                              ref={input => {
                                this.carnumber = input;
                              }}
                              onChange={event =>
                                this.setState(
                                  { carnumber: event.target.value },
                                  () => { }
                                )
                              }
                            />
                          </Grid>
                          <Grid item lg={2} xl={2} xs={2} sm={2} md={2} />
                          <Grid
                            item
                            lg={4}
                            xl={4}
                            xs={4}
                            sm={4}
                            md={4}
                            style={{ padding: 3 }}
                          >
                            <h5
                              style={{
                                color: "#8089a9",
                                textAlign: "right"
                              }}
                            >
                              ผู้ขับ{" "}
                            </h5>
                          </Grid>
                          <Grid
                            item
                            lg={6}
                            xl={6}
                            xs={6}
                            sm={6}
                            md={6}
                            style={{ padding: 3 }}
                          >
                            <MDBBtnGroup>
                              <MDBDropdown>
                                <MDBDropdownToggle
                                  caret
                                  color="primary " outline rounded
                                  className="h-100"
                                >
                                  {this.state.car_name}
                                </MDBDropdownToggle>
                                <MDBDropdownMenu basic color="danger">
                                  {listdata}
                                </MDBDropdownMenu>
                              </MDBDropdown>
                            </MDBBtnGroup>


                          </Grid>
                          <Grid item lg={2} xl={2} xs={2} sm={2} md={2} />
                          <Grid
                            item
                            lg={4}
                            xl={4}
                            xs={4}
                            sm={4}
                            md={4}
                            style={{ padding: 3 }}
                          >
                            <h5
                              style={{
                                color: "#8089a9",
                                textAlign: "right"
                              }}
                            >
                              เด็กติดรถ{" "}
                            </h5>
                          </Grid>
                          <Grid
                            item
                            lg={6}
                            xl={6}
                            xs={6}
                            sm={6}
                            md={6}
                            style={{ padding: 3 }}
                          >
                            <MDBBtnGroup>
                              <MDBDropdown>
                                <MDBDropdownToggle
                                  caret
                                  color="primary " outline rounded
                                  className="h-100"
                                >
                                  {this.state.car_name2}
                                </MDBDropdownToggle>
                                <MDBDropdownMenu basic color="danger">
                                  {listdata2}
                                </MDBDropdownMenu>
                              </MDBDropdown>
                            </MDBBtnGroup>


                          </Grid>
                          <Grid item lg={2} xl={2} xs={2} sm={2} md={2} />
                          <Grid
                            item
                            lg={4}
                            xl={4}
                            xs={4}
                            sm={4}
                            md={4}
                            style={{ padding: 3 }}
                          >
                            <h5
                              style={{
                                color: "#8089a9",
                                textAlign: "right"
                              }}
                            >
                              เบอร์โทร{" "}
                            </h5>
                          </Grid>
                          <Grid
                            item
                            lg={6}
                            xl={6}
                            xs={6}
                            sm={6}
                            md={6}
                            style={{ padding: 3 }}
                          >
                            <TextInput
                              style={{
                                width: "100%",
                                fontSize: 15,
                                backgroundColor: "white",
                                color: "black"
                              }}
                              size="xsmall"
                              value={this.state.car_phone}
                              // placeholder="exp 031"
                              ref={input => {
                                this.phone = input;
                              }}
                              onChange={event =>
                                this.setState(
                                  { car_phone: event.target.value },
                                  () => { }
                                )
                              }
                            // onKeyPress={event => {
                            //   if (event.key === "Enter") {
                            //     this.SearchData();
                            //   }
                            // }}
                            />
                          </Grid>

                        </Grid>

                      </Grid>
                      <MDBBtn color="secondary" onClick={this.toggle2}>ปิด</MDBBtn>
                      <MDBBtn color="primary" onClick={this.savedata}>บันทึก</MDBBtn>
                      <Grid
                        item
                        lg={12}
                        xl={12}
                        xs={12}
                        sm={12}
                        md={12}
                        style={{ padding: 10 }}
                      >
                        <MDBCard
                          style={{ width: "100%", marginTop: "1rem" }}
                          className="text-center"
                        >
                          <MDBCardHeader
                            style={{
                              borderBottomWidth: "0px",
                              padding: 20,
                              backgroundColor: "#272262"
                            }}
                          >

                            <Grid container spacing={24}>
                              <Grid
                                item
                                lg={2}
                                xl={2}
                                xs={2}
                                sm={2}
                                md={2}
                                style={{ padding: 10 }}
                              >
                                <h5
                                  style={{
                                    color: "white",
                                    textAlign: "right"
                                  }}
                                >
                                  เลขพาเลซ{" "}
                                </h5>
                              </Grid>
                              <Grid
                                item
                                lg={2}
                                xl={2}
                                xs={2}
                                sm={2}
                                md={2}
                                style={{ padding: 10 }}
                              >
                                <TextInput
                                  style={{
                                    width: "100%",
                                    fontSize: 15,
                                    backgroundColor: "white",
                                    color: "black"
                                  }}
                                  size="xsmall"
                                  // disabled={this.state.txtdoc_number}
                                  value={this.state.doc_number}
                                  ref={input => {
                                    this.doc_number = input;
                                  }}
                                  onChange={event =>
                                    this.setState(
                                      { doc_number: event.target.value },
                                      () => {
                                        this.doc_number.focus();
                                      }
                                    )
                                  }
                                  onKeyPress={event => {
                                    if (event.key === "Enter") {
                                      // console.log(this.state.doc_number);
                                      this.setDatagrid();
                                      this.setState({
                                        doc_number: "",
                                        lastkey: this.state.doc_number
                                      });
                                    }
                                  }}
                                />
                              </Grid>

                              <Grid
                                item
                                lg={2}
                                xl={2}
                                xs={2}
                                sm={2}
                                md={2}
                                style={{ padding: 10 }}
                              >
                                <h5
                                  style={{
                                    color: "white",
                                    textAlign: "right"
                                  }}
                                >
                                  เลขกล่อง{" "}
                                </h5>
                              </Grid>
                              <Grid
                                item
                                lg={2}
                                xl={2}
                                xs={2}
                                sm={2}
                                md={2}
                                style={{ padding: 10 }}
                              >
                                <TextInput
                                  style={{
                                    width: "100%",
                                    fontSize: 15,
                                    backgroundColor: "white",
                                    color: "black"
                                  }}
                                  size="xsmall"
                                  // disabled={this.state.txtdoc_number}
                                  value={this.state.doc_pack}
                                  ref={input => {
                                    this.doc_pack = input;
                                  }}
                                  onChange={event =>
                                    this.setState(
                                      { doc_pack: event.target.value },
                                      () => {
                                        this.doc_pack.focus();
                                      }
                                    )
                                  }
                                  onKeyPress={event => {
                                    if (event.key === "Enter") {
                                      // console.log(this.state.doc_number);
                                      this.setDatagrid2();
                                      this.setState({
                                        doc_pack: "",
                                        lastkey2: this.state.doc_pack
                                      });
                                    }
                                  }}
                                />
                              </Grid>


                            </Grid>
                          </MDBCardHeader>
                          <MDBCardBody
                            style={{ padding: 12, backgroundColor: "#272262" }}
                          >

                            <Grid container spacing={24}>
                              <Grid
                                item
                                lg={12}
                                xl={12}
                                xs={12}
                                sm={12}
                                md={12}
                                style={{ padding: 0 }}
                              >
                                <center>
                                  <Datagridhead
                                    // setCode_body={this.setCode_body}
                                    jobdoclist={this.state.jobdoclist}
                                  />
                                </center>
                              </Grid>

                            </Grid>
                          </MDBCardBody>

                        </MDBCard>
                      </Grid>
                    </Grid>
                  </MDBCardBody>
                </MDBCard>
              </MDBCol>
            </MDBRow>
          </MDBModalBody>
          <MDBModalFooter>

          </MDBModalFooter>
        </MDBModal>




      </div>


    );
  }
}
export default withSnackbar(Datadoc);
