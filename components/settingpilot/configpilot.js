import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import LinearProgress from "@material-ui/core/LinearProgress";
import { TextInput } from "grommet";
import { withSnackbar } from "notistack";
import { MDBBtn, MDBIcon,MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from "mdbreact";
import Datagrid from "./datagrid.tsx";
import Select from "react-select";
//API connect
import ApiService from "../actions/apidata";
class Configpilot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      completed: 0,
      modal: false,
      buffer: 10,
      listname: "",
      nickname: "",
      phone: "",
      id: "",
      supid:"",
     driverdata:[],
      transportdata:[],
      transelect:[]
    
    };
    // this.getlistdriver = this.getlistdriver.bind(this);
    this.Canceldoc = this.Canceldoc.bind(this);
    this.toggle = this.toggle.bind(this);
    this.setpilot = this.setpilot.bind(this);
    this.seteditpilot = this.seteditpilot.bind(this);
    this.ApiCall = new ApiService();
  }
  componentDidMount() {
  
    this.ApiCall.gettransport()
    .then(res => {
      if (res.status === true) {
        this.setState({ transportdata: res.data });
        // console.log(this.state.transportdata)
      } else {
        console.log(res.message);
      }
    })
    .catch(error => {
      console.error(error.message);
    });
    // console.log("dd")
    this.ApiCall.Showdriverlist()
    .then(res => {
      if (res.status === true) {
        this.setState({ driverdata: res.data });
    //    console.log(this.state.driverdata)
      } else {
        console.log(res.message);
      }
    })
    .catch(error => {
      console.error(error.message);
    });
    // this.getlistdriver()

  }
  Canceldoc(jobdoc) {
    let docvar = Array();
    docvar = {
      type: 2,
      listname: jobdoc.listname

    };
    // console.log(docvar)
    this.ApiCall.Deletedriverlist(docvar)
      .then(res => {
        if (res.status === true) {
          // window.location.reload();
          this.props.enqueueSnackbar("ระบบยกเลิกรายการ สำเร็จแล้ว ", {
            variant: "success"
          });
         this.componentDidMount()
          // this.setTimeout(
          //     window.location.reload(),
          //   100000
          // );
        } else {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  toggle(jobdoc) {
    this.setState(
      {
        modal: !this.state.modal,
        listname:jobdoc.listname,
        nickname:jobdoc.nickname,
        phone:jobdoc.phone,
        id:jobdoc.id,
       
        transelect:[{
         'value': jobdoc.supid,
         'label': jobdoc.transportname
       }],
      },
      () => {
      
        // console.log(this.state.transelect[0].value)
        // this.clearvolume();
      }
    );
   
   
   
 
  }
  getlistdriver(){
    this.ApiCall.Showdriverlist()
    .then(res => {
      if (res.status === true) {
        this.setState({
            listdriver:res.data
        })
      } 
    //   console.log(this.state.listdriver)
    })
  
    .catch(error => {
      console.error(error.message);
    });
  }
  seteditpilot() {
    if (
      this.state.listname === "" ||
      this.state.nickname === "" || 
      // this.state.transelect.value === "undefined" ||
      
      this.state.phone === ""
    ) {
      this.props.enqueueSnackbar(
        "Warning กรุณา ระบุใส่ข้อมูลให้ถูกต้อง ครบถ้วน??",
        {
          variant: "warning"
        }
      );
    } else {
      let docvar = Array();
    
      docvar = {
        type: 1,
        id: this.state.id,
        listname: this.state.listname,
        supid: this.state.transelect[0].value,
        nickname: this.state.nickname,
        phone: this.state.phone
      
      };
// console.log(docvar)
      this.ApiCall.Updatedriverlist(docvar)
        .then(res => {
          console.log(res);
          if (res.status === true) {
            this.props.enqueueSnackbar("บันทึกข้อมูล สำเร็จแล้ว", {
              variant: "success"
            });
            this.setState({
              modal: !this.state.modal,
           
            });
           
            this.componentDidMount();
            this.clearvolume();
          } else if (res.status === false) {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการให้สำเร็จได้ หรือ มีรายการนี้อยู่แล้ว ลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          } else {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          }
        })
        .catch(error => {
          console.error(error.message);
        });
    }
  }
setpilot() {
    if (
      this.state.listname === "" ||
      this.state.nickname === "" ||
      this.state.transelect.value === "" ||
      this.state.phone === ""
    ) {
      this.props.enqueueSnackbar(
        "Warning กรุณา ระบุใส่ข้อมูลให้ถูกต้อง ครบถ้วน??",
        {
          variant: "warning"
        }
      );
    } else {
      let docvar = Array();
    
      docvar = {
        type: 0,
        listname: this.state.listname,
        supid: this.state.transelect.value,
        nickname: this.state.nickname,
        phone: this.state.phone
      
      };
// console.log(docvar)
      this.ApiCall.Updatedriverlist(docvar)
        .then(res => {
          console.log(res);
          if (res.status === true) {
            this.props.enqueueSnackbar("บันทึกข้อมูล สำเร็จแล้ว", {
              variant: "success"
            });
          
            this.clearvolume();
            this.componentDidMount();
          } else if (res.status === false) {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการให้สำเร็จได้ หรือ มีรายการนี้อยู่แล้ว ลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          } else {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          }
        })
        .catch(error => {
          console.error(error.message);
        });
    }
  }
  clearvolume() {
    this.setState(
      {
       
        listname: "",
        nickname: "",
        transelect: "",
        phone: ""
      
      },
      () => {
      }
    );
  }
  

  render() {
    const { completed } = this.state;
    return (
      <div style={{ fontFamily: "Prompt" }}>
        <Grid container spacing={24}>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 0 }}
          />
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <h4 style={{ color: "green" }}>
              <br />
              <MDBIcon icon="cogs" className="cyan-text pr-3" />&nbsp;รายละเอียดผู้ขับ
            </h4>
          </Grid>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <LinearProgress
              color="secondary"
              variant="determinate"
              value={completed}
            />
            <hr />
          </Grid>
          <Grid item lg={6} xl={6} xs={6} sm={6} md={6} style={{ padding: 3 }}>
            <Grid container spacing={24}>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >
                <h5
                  style={{
                    color: "#8089a9",
                    textAlign: "left",
                    marginLeft: 10
                  }}
                >
                  เพิ่มผู้ขับใหม่{" "}
                  {/* <i style={{ color: "#596eb9" }}>{this.state.code_route}</i> */}
                </h5>
              </Grid>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              />
              
              
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}>รายชื่อบริษัท</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              >
                  <div className="select-up" style={{ textAlign: "left" }}>
                        <Select
                          value={this.state.transelect}
                          placeholder="-- รายชื่อบริษัทขนส่ง --"
                          size="xsmall"
                          options={this.state.transportdata}
                          onChange={event => {
                            this.setState(
                              { transelect: event, selected: event.defaultapp },
                              () => {
                                // console.log(this.state.transelect);
                              }
                            );
                          }}
                        />
                      </div>
               
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              />
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}>ชื่อ-นามสกุล</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              >
                <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.listname}
                  // placeholder="exp A1"
                  ref={input => {
                    this.listname = input;
                  }}
                  onChange={event =>
                    this.setState({ listname: event.target.value }, () => {})
                  }
                />
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              />
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}>ชื่อเล่น</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              >   <TextInput
              style={{ width: "100%", fontSize: 14 }}
              size="xsmall"
              value={this.state.nickname}
              // placeholder="exp A1"
              ref={input => {
                this.nickname = input;
              }}
              onChange={event =>
                this.setState({ nickname: event.target.value }, () => {})
              }
            />
              
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              />
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}>เบอร์โทรศัพท์</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              >
               <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.phone}
                  // placeholder="exp A1"
                  ref={input => {
                    this.phone = input;
                  }}
                  onChange={event =>
                    this.setState({ phone: event.target.value }, () => {})
                  }
                />
              </Grid>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 0 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 10 }}
              >
                <MDBBtn
                  color="primary"
                  onClick={() => {
                    this.clearvolume();
                  }}
                >
                  <MDBIcon icon="undo" className="mr-1" /> ล้างข้อมูล
                </MDBBtn>
                <MDBBtn
                  color="default"
                  onClick={() => {
                    this.setpilot();
                  }}
                >
                  บันทึกข้อมูลผู้ขับ <MDBIcon icon="save" className="ml-1" />
                </MDBBtn>
              </Grid>
            </Grid>
          </Grid>
          <Grid item lg={6} xl={6} xs={6} sm={6} md={6} style={{ padding: 3 }}>
            <Grid container spacing={24}>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >
                <Grid
                  item
                  lg={12}
                  xl={12}
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 3 }}
                >
                  <h5
                    style={{
                      color: "#8089a9",
                      textAlign: "left",
                      marginLeft: 10
                    }}
                  >
                    ตารางข้อมูลผู้ขับ{" "}
                    <i style={{ color: "#596eb9" }}>{this.state.code_route}</i>
                  </h5>
                </Grid>
                <Grid item lg={12} xl={12} xs={12} sm={12} md={12}>
                <Datagrid driverdata={this.state.driverdata}  Canceldoc={this.Canceldoc} toggle={this.toggle}/>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <MDBModal isOpen={this.state.modal} toggle={this.toggle}>
        <MDBModalHeader toggle={this.toggle}>แก้ไขข้อมูลคนขับ</MDBModalHeader>
        <MDBModalBody>
          
          
              
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "left", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}>รายชื่อบริษัท</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >
                  <div className="select-up" style={{ textAlign: "left" }}>
                        <Select
                          value={this.state.transelect}
                          placeholder="-- รายชื่อบริษัทขนส่ง --"
                          size="xsmall"
                          options={this.state.transportdata}
                          onChange={event => {
                            this.setState(
                              { transelect:[{
                                'value': event.value,
                                'label': event.label
                              }]},
                              () => {
                             
                              }
                            );
                          }}
                        />
                      </div>
               
              </Grid>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "left", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}>ชื่อ-นามสกุล</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >
                <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.listname}
                  // placeholder="exp A1"
                  ref={input => {
                    this.listname = input;
                  }}
                  onChange={event =>
                    this.setState({ listname: event.target.value }, () => {})
                  }
                />
              </Grid>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "left", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}>ชื่อเล่น</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >   <TextInput
              style={{ width: "100%", fontSize: 14 }}
              size="xsmall"
              value={this.state.nickname}
              // placeholder="exp A1"
              ref={input => {
                this.nickname = input;
              }}
              onChange={event =>
                this.setState({ nickname: event.target.value }, () => {})
              }
            />
              
              </Grid>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "left", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}>เบอร์โทรศัพท์</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >
               <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.phone}
                  // placeholder="exp A1"
                  ref={input => {
                    this.phone = input;
                  }}
                  onChange={event =>
                    this.setState({ phone: event.target.value }, () => {})
                  }
                />
              </Grid>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="secondary" onClick={this.toggle}>Close</MDBBtn>
          <MDBBtn color="primary" onClick={this.seteditpilot}>Save changes</MDBBtn>
        </MDBModalFooter>
      </MDBModal>
        </Grid>
      </div>
    );
  }
}

export default withSnackbar(Configpilot);
