import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Datagridcarboy extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();
  constructor(props: any) {
      super(props);
  }
 
  public render() {
   
    const columns: any =[
      { text: 'id',datafield:'id', editable: false,   cellsalign: 'center',align: 'center',  filterable:false },
        { text: 'ชื่อ-นามสกุล',datafield:'listname', editable: false,   cellsalign: 'center',align: 'center' },
        { text: 'ชื่อเล่น',datafield:'nickname', editable: false,   cellsalign: 'center',align: 'center' },
        { text: 'id',editable: false,  datafield: 'supid',hidden:true },
       
        { text: 'เบอร์โทรศัพท์',datafield:'phone', editable: false,  filterable:false, cellsalign: 'center',align: 'center' },
       
        
      { text: 'แก้ไข',editable: false,filterable:false,columntype:'button', buttonclick: (row: number): void => {

        this.props.toggle(this.myGrid.current!.getrowdata(row));
        
    },
    cellsrenderer: (): string => {
        return 'แก้ไข';
    }, align: 'center', cellsalign: 'center',width: '15%' },
    
        // { text: 'วันที่',datafield:'upd', editable: false,   cellsalign: 'center',align: 'center' },
       
      
    ]
    const source: any = {
      datafields: [
          { name: 'listname', type: 'string' },
          { name: 'nickname', type: 'string' },
          { name: 'phone', type: 'string' },
          { name: 'supid', type: 'string' },
          { name: 'id', type: 'number' },
          { name: 'transportname', type: 'string' },
          // { name: 'upd', type: 'string' },
       
      ],
      datatype: 'array',
       localdata:this.props.driverdata,
      updaterow: (rowid: any, rowdata: any, commit: any): void => {
        // this.props.onUpdatedata(rowdata);
        commit(true);
    }
  };
      return (
        <JqxGrid
              ref={this.myGrid}
              theme="metrodark"
              width={'90%'} source={new jqx.dataAdapter(source)} columns={columns}
              filterable={true}   showstatusbar={true} showfilterrow={true}
              pageable={false} autoheight={false} sortable={false} altrows={false}
              editmode ={'click'}
              enabletooltips={true} editable={true} selectionmode={'singlerow'}
            
        />
      );
  }
}
export default Datagridcarboy;