import React, { Component } from "react";
import {
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBCardFooter
} from "mdbreact";
import LoadingOverlay from "react-loading-overlay";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { withSnackbar } from "notistack";
import Grid from "@material-ui/core/Grid";
import { MDBBtn, MDBIcon } from "mdbreact";
import LinearProgress from "@material-ui/core/LinearProgress";
import { MDBBtnGroup, MDBDropdown, MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem } from "mdbreact";
import {
  ExcelExport,
  ExcelExportColumn
} from "@progress/kendo-react-excel-export";

import Datagrid from "./datagridbyday.tsx";
import ApiService from "../actions/apidata";
import AuthService from "../authlogin/AuthService";
import jiblogo from "../img/logo-logistics3.png";
import Datetime1 from "./datetime1.tsx";
import Datetime2 from "./datetime2.tsx";

class Reportbyday extends Component {
  _exporter;
  constructor(props) {
    super(props);
    this.senddata = this.senddata.bind(this);
    this.searchdata = this.searchdata.bind(this);
    this.export = this.export.bind(this);
    this.Updatedata = this.Updatedata.bind(this);
    this.getDatenow = this.getDatenow.bind(this);
    this.setValueddl = this.setValueddl.bind(this);

    this.state = {
      completed: 0,
      buffer: 10,
    //   mdata: [],
      headdata: [],
    //   dataex: [],
      rowdata: [],
      dataprint: [],
      active:false,
       valueddl: [],
    //   carname: "",
    //   datedoc: "",
    //   carphone: "",
    //   dateprint: "",
    //   datacarname: "",
    //   datacarphone: "",
    //   datacarnumber: "",
    //   datatotalload: "",
    //   dataload: "",
    //   dataroute: "",
      datasupname: "-- เลือกบริษัทขนส่ง --",
    //   searchsup: "",
    //   searchdoc: "",
      dateselect1: this.getDatenow(),
      dateselect2: this.getDatenow()
    };
    this.Auth = new AuthService();
    this.ApiCall = new ApiService();
  }

  export() {
    this._exporter.save();
  }
  setValueddl(label){
    // console.log(label.target.value)
    this.setState({
        datasupname:label.target.value
    })
  }
  getDatenow() {
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + month + "-" + day;
    return today;
  }
  componentDidMount() {
    if (!this.Auth.loggedIn()) {
      window.location.href = "/Login";
    }
  
    this.ApiCall.gettransport()
      .then(res => {
        //   console.log(res.data)
        if (res.status === true) {
          this.setState({ valueddl: res.data });
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });

    this.senddata();
  }

  Updatedata(typedate, rows) {
    if (typedate === 0) {
      this.setState(
        {
          dateselect1: rows
        },
        () => {
        //   console.log(this.state.dateselect1);
          // this.Getreportnow();
        }
      );
    } else {
      this.setState(
        {
          dateselect2: rows
        },
        () => {
        //   console.log(this.state.dateselect2);
          // console.log(this.state.dateselect1)
          // this.Getreportnow();
        }
      );
    }
  }
  searchdata() {
     
     if(this.state.datasupname=='-- เลือกบริษัทขนส่ง --'){
        this.props.enqueueSnackbar(
            "เลือกบริษัทขนส่งแล้วหรือยัง..",
            {
              variant: "error"
            }
          );
     }else{
       this.setState({active:true},()=>{
        this.senddata();
       })
        
     } 
    
  }
  senddata() {
    var docvar = {
      searchsup: this.state.datasupname,
      dateselect1: this.state.dateselect1,
      dateselect2: this.state.dateselect2
    };
    this.ApiCall.GetdataLogisDoc_report1(docvar)
      .then(res => {
        if (res.status === true) {
            // console.log(res.data)
           this.setState({ mdata: res.data,active:false });
        } else {
          this.setState({ active:false });
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
 
  // load_print(data) {
  //   // this.getexcel(data);
  //   this.setState(
  //     {
  //       doccode: data.doc_code,
  //       carname: data.car_name,
  //       carphone: data.car_phone,
  //       datedoc: data.date_doc
  //     },
  //     () => {
  //       this.timeout = setTimeout(() => {
  //         var content = document.getElementById("printarea");
  //         var pri = document.getElementById("ifmcontentstoprint").contentWindow;
  //         pri.document.open();
  //         pri.document.write(content.innerHTML);
  //         pri.document.close();

  //         pri.print();
  //       }, 500);
  //     }
  //   );
  // }

//  
  render() {
    let test = Array();
    test = this.state.dataex;

    function checkvalue(rowdata, headdata) {
      for (var i = 0; i < test.length; i++) {
        if (test[i].branchname == rowdata && test[i].loadname == headdata) {
          return test[i].countbox;
        }
      }
    }
    const list = this.state.valueddl.map(valueddl => (
        <MDBDropdownItem onClick={this.setValueddl} value={valueddl.label}>
            {valueddl.label}
        </MDBDropdownItem>
    ));

    const listtitle = this.state.valueddl.map(valueddl => (
      <tr align="center">
        <td> {valueddl.loaddoc} </td>
        <td> {valueddl.total} </td>
      </tr>
    ));

    const { completed } = this.state;

    return (
      <div>
        <MDBRow>
          <MDBCol md="12">
            <MDBCard>
              <MDBCardBody style={{ fontFamily: "Prompt" }}>
                <Grid container spacing={24}>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 3 }}
                  >
                    <h4 style={{ color: "green" }}>
                      <MDBIcon icon="paste" className="pink-text pr-3" />
                      &nbsp;รายงานการขนส่งรายวัน (แยกตามบริษัทขนส่ง)
                    </h4>
                    <h5 style={{ color: "#8089a9", textAlign: "right" }}>
                      <MDBIcon icon="user-edit" className="cyan-text pr-3" />
                      &nbsp;
                    </h5>
                  </Grid>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0 }}
                  >
                    <LinearProgress
                      color="secondary"
                      variant="determinate"
                      value={completed}
                    />
                    <hr />
                  </Grid>

                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0 }}
                  >
                    <MDBCard
                      style={{ width: "100%", marginTop: "0" }}
                      className="text-center"
                    >
                      <MDBCardHeader
                        style={{
                          borderBottomWidth: "0px",
                          backgroundColor: "#272262"
                        }}
                      >
                        <Grid container spacing={24}>
                        <Grid
                            item
                            lg={2}
                            xl={2}
                            xs={2}
                            sm={2}
                            md={2}
                            style={{ padding: 10 }}
                          >
                            <MDBBtnGroup>
                              <MDBDropdown>
                                <MDBDropdownToggle
                                  caret
                                  color="danger"
                                  className="h-100"
                                >
                                  {this.state.datasupname}
                                </MDBDropdownToggle>
                                <MDBDropdownMenu basic color="danger">
                                 {list}
                                </MDBDropdownMenu>
                              </MDBDropdown>
                            </MDBBtnGroup>
                          </Grid>
                          <Grid
                            item
                            lg={1}
                            xl={1}
                            xs={1}
                            sm={1}
                            md={1}
                            style={{
                              padding: 5,
                              marginLeft: 0,
                              paddingTop: 15
                            }}
                          >
                            <h5 style={{ color: "white", fontSize: 16 }}>
                              วันที่เอกสาร
                            </h5>
                          </Grid>
                          <Grid
                            item
                            lg={1}
                            xl={1}
                            xs={1}
                            sm={1}
                            md={1}
                            style={{
                              padding: 0,
                              marginLeft: 5,
                              paddingTop: 15
                            }}
                          >
                            <Datetime1 Updatedata={this.Updatedata} />
                          </Grid>
                          <Grid
                            item
                            lg={1}
                            xl={1}
                            xs={1}
                            sm={1}
                            md={1}
                            style={{
                              padding: 0,
                              marginLeft: 5,
                              paddingTop: 15
                            }}
                          >
                            <Datetime2 Updatedata={this.Updatedata} />
                          </Grid>

                          <Grid
                            item
                            lg={4}
                            xl={4}
                            xs={4}
                            sm={4}
                            md={4}
                            style={{ padding: 10 }}
                          >
                            <MDBBtn
                              color="default"
                              onClick={() => {
                                this.searchdata();
                              }}
                            >
                              ค้นหา <MDBIcon icon="search" className="ml-1" />
                            </MDBBtn>
                           
                            <MDBBtn
                              color="secondary"
                              onClick={() => {
                                this.export();
                              }}
                            >
                              <MDBIcon
                                icon="calendar-alt"
                                className="mr-1"
                                size="sm"
                              />{" "}
                              Excel
                            </MDBBtn>
                          </Grid>
                        </Grid>
                      </MDBCardHeader>
                      <MDBCardBody
                        style={{ padding: 12, backgroundColor: "#272262" }}
                      >
                        <Grid container spacing={24}>
                          <Grid
                            item
                            lg={12}
                            xl={12}
                            xs={12}
                            sm={12}
                            md={12}
                            style={{ padding: 0 }}
                          >
                            <center>
                            <LoadingOverlay active={this.state.active} spinner text="loading...">
                              <Datagrid
                                datadoc={this.state.mdata}
                                loadprint={this.load_print}
                                CancelDocjob={this.CancelDocjob}
                              />
                              </LoadingOverlay>
                            </center>
                          </Grid>
                        </Grid>
                      </MDBCardBody>
                      <MDBCardFooter
                        style={{
                          borderTopWidth: "0px",
                          backgroundColor: "#272262",
                          color: "white"
                        }}
                      />
                    </MDBCard>
                  </Grid>
                </Grid>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>

        </MDBRow>

        <ExcelExport
          data={this.state.mdata}
          // group={group}
          fileName={
            "ข้อมูลขนส่ง " +
            this.state.dateselect1 +
            "_" +
            this.state.dateselect2 +
            ".xlsx"
          }
          ref={exporter => {
            this._exporter = exporter;
          }}
        >
          <ExcelExportColumn
            field="doc_code"
            title="เลขที่เอกสาร"
            locked={true}
            width={150}
          />
          <ExcelExportColumn field="type_logis" title="ขนส่ง" width={150} />
          <ExcelExportColumn field="date_doc" title="วันที่ขนส่ง" width={150} />
          <ExcelExportColumn field="sup_name" title="บริษัท" width={200} />
          <ExcelExportColumn field="car_number" title="ทะเบียนรถ" width={200} />
          <ExcelExportColumn field="car_rout" title="เส้นทาง" width={150} />
          <ExcelExportColumn field="sup_name" title="บริษัท" width={200} />
          <ExcelExportColumn field="car_number" title="ทะเบียนรถ" width={100} />
          <ExcelExportColumn field="car_name" title="ชื่อคนขับ" width={100} />
          <ExcelExportColumn field="car_phone" title="เบอร์โทร" width={100} />
          <ExcelExportColumn field="doc_status" title="สถานะ" width={100} />
          <ExcelExportColumn field="logis_price" title="ค่าขนส่ง" width={100} />
          <ExcelExportColumn field="palate" title="จำนวนพาเลท" width={100} />
          <ExcelExportColumn field="branch" title="จำนวนสาขา" width={100} />
          <ExcelExportColumn field="box" title="จำนวนกล่อง" width={100} />
        </ExcelExport>
      </div>
    );
  }
}

export default withSnackbar(Reportbyday);
