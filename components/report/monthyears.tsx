import * as React from 'react';
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";
import JqxDateTimeInput from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxdatetimeinput';
class Monthyears extends React.PureComponent<any> {
   
    constructor(props: {}) {
        
        super(props);
        this.onValueChanged = this.onValueChanged.bind(this);
    }
    public render() {
        return (
            <JqxDateTimeInput  onValueChanged={this.onValueChanged}  theme="metrodark"  width={'100%'} height={35}  animationType={'slide'} dropDownHorizontalAlignment={'right'} />
        );
    }
    private onValueChanged(event: any): void {
        const date = event.args.date;
        var value = date.getFullYear()+'-'+(date.getMonth()+1);
        this.props.Updatedata(0,value);
       
    }
}
export default Monthyears;