import React, { Component } from "react";
import {
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBCardFooter
} from "mdbreact";
import Grid from "@material-ui/core/Grid";
import { MDBIcon } from "mdbreact";
import LinearProgress from "@material-ui/core/LinearProgress";
import Barcode from "react-barcode";

import ApiService from "../actions/apidata";
import AuthService from "../authlogin/AuthService";
import Datagridpre from "./datagrid_pre.tsx";


export default class LogisDocPre extends Component {
  constructor(props) {
    super(props);

    this.state = {
      completed: 0,
      loaddata: [],
     
    };
    this.Auth = new AuthService();
    this.ApiCall = new ApiService();

  }


  loadGrid() {
      this.ApiCall.getlogregispre()
        .then(res => {
          if (res.status === true) {
            this.setState({
                loaddata:res.data
            });
          } else {
            console.log(res.message);
          }
        })
        .catch(error => {
          console.error(error.message);
        });
 
  }

  componentDidMount() {
    if (!this.Auth.loggedIn()) {
      window.location.href = "/Login";
    }else{
        this.loadGrid()
    }
  }
  render() {
    return (
      <div>
        <MDBRow>
          <MDBCol md="12">
            <MDBCard>
              <MDBCardBody style={{ fontFamily: "Prompt" }}>
                <Grid container spacing={24}>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 3 }}
                  >
                    <h4
                      style={{ color: "green",paddingBottom:10,paddingTop:10, marginLeft: 10 }}
                    >
                      <MDBIcon
                        icon="file-contract"
                        className="pink-text pr-3"
                      />
                      &nbsp;รายการขนส่งที่ยังไม่ดำเนินงาน(วันนี้)
                    </h4>
                  </Grid>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0 }}
                  >
                    <LinearProgress
                      color="secondary"
                      variant="determinate"
                      value={this.state.completed}
                    />
                    <hr />
                  </Grid>

                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0 }}
                  >
                   <Datagridpre loaddata={this.state.loaddata} />
                  </Grid>
                </Grid>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
        </MDBRow>
      
      </div>
    );
  }
}
