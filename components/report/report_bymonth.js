import React, { Component } from "react";
import {
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBCardFooter
} from "mdbreact";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { withSnackbar } from "notistack";
import Grid from "@material-ui/core/Grid";
import { MDBBtn, MDBIcon } from "mdbreact";
import LinearProgress from "@material-ui/core/LinearProgress";
import LoadingOverlay from "react-loading-overlay";
import {
  MDBBtnGroup,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem
} from "mdbreact";
import {
  ExcelExport,
  ExcelExportColumn
} from "@progress/kendo-react-excel-export";

import Datagrid from "./datagridbymonth.tsx";
import ApiService from "../actions/apidata";
import AuthService from "../authlogin/AuthService";
import jiblogo from "../img/logo-logistics3.png";
import Monthyears from "./monthyears.tsx";
// import Datetime2 from "./datetime2.tsx";
const monthNames = [{name:"มกราคม",value:1}, {name:"กุมภาพันธ์",value:2}, {name:"มีนาคม",value:3}, {name:"เมษายน",value:4}, {name:"พฤษภาคม",value:5}, {name:"มิถุนายน",value:6},
{name:"กรกฎาคม",value:7}, {name:"สิงหาคม",value:8}, {name:"กันยายน",value:9}, {name:"ตุลาคม",value:10}, {name:"พฤศจิกายน",value:11}, {name:"ธันวาคม",value:12}
];
class Reportbymonth extends Component {
  _exporter;
  constructor(props) {
    super(props);
    this.senddata = this.senddata.bind(this);
    this.searchdata = this.searchdata.bind(this);
    this.export = this.export.bind(this);
    this.setMonthddl = this.setMonthddl.bind(this);
    this.setYearddl = this.setYearddl.bind(this);
    this.getDatenow = this.getDatenow.bind(this);
    this.setValueddl = this.setValueddl.bind(this);

    this.state = {
      completed: 0,
      buffer: 10,
      headdata: [],
      rowdata: [],
      mdata:[],
      active:false,
      dataprint: [],
      valueddl: [],
      YearNames:[],
      monthvalue:this.getDatenow2(),
      supval:0,
      datasupname: "-- เลือกบริษัทขนส่ง --",
      monthnameselect: this.getDatenow(1),
      yearnameselect: this.getDatenow(2),
    
    };
    this.Auth = new AuthService();
    this.ApiCall = new ApiService();
  }

  export() {
    this._exporter.save();
  }
  setValueddl(label) {
    // console.log(label.target.name)
    this.setState({
      datasupname: label.target.name,
      supval:label.target.value
    });
  }
  setMonthddl(event){
    // console.log(event.target.name)
    this.setState({monthvalue:event.target.value,monthnameselect:event.target.name});
  }
  setYearddl(event){
    // console.log(event.target.name)
    this.setState({yearnameselect:event.target.value});
  }

  getDatenow2() {
    var now = new Date();
    
    return now.getMonth()+1
  
   }
  getDatenow(val) {
    var now = new Date();
    
   if(val===1){
    const monthtxt = now.getMonth();
    var yeartxt = now.getFullYear();
    return monthNames[monthtxt].name
   }else{
    
    return now.getFullYear()
    // console.log(yearArr)
   }

  }
  componentDidMount() {
    if (!this.Auth.loggedIn()) {
      window.location.href = "/Login";
    }
    var now = new Date();
    let yearArr=[];
    for(var i=0;i<5;i++){
      yearArr[i]=now.getFullYear()-i;
    }
    this.setState({YearNames:yearArr})
    this.ApiCall.gettransport()
      .then(res => {
        //   console.log(res.data)
        if (res.status === true) {
          this.setState({ valueddl: res.data });
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });

    // this.senddata();
  }

  Updatedata(typedate, rows) {
    if (typedate === 0) {
      this.setState(
        {
          dateselect1: rows
        },
        () => {
          //   console.log(this.state.dateselect1);
          // this.Getreportnow();
        }
      );
    } else {
      this.setState(
        {
          dateselect2: rows
        },
        () => {
          //   console.log(this.state.dateselect2);
          // console.log(this.state.dateselect1)
          // this.Getreportnow();
        }
      );
    }
  }
  searchdata() {
    if (this.state.datasupname == "-- เลือกบริษัทขนส่ง --") {
      this.props.enqueueSnackbar("เลือกบริษัทขนส่งแล้วหรือยัง..", {
        variant: "error"
      });
    } else {
      this.setState({active:true},()=>{
        this.senddata();
      })
      
    }
  }
  senddata() {
    var docvar = {
      supid: this.state.supval,
      years: this.state.yearnameselect,
      months: this.state.monthvalue
    };
    // console.log(docvar)
    this.ApiCall.showreportBymonths(docvar)
      .then(res => {
        if (res.status === true) {
          // console.log(res.data)
          this.setState({ mdata: res.data,active:false });
        } else {
          console.log(res.message);
          this.setState({ active:false });
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }

  render() {
    let test = Array();
    test = this.state.dataex;

    function checkvalue(rowdata, headdata) {
      for (var i = 0; i < test.length; i++) {
        if (test[i].branchname == rowdata && test[i].loadname == headdata) {
          return test[i].countbox;
        }
      }
    }
    const list = this.state.valueddl.map(valueddl => (
      <MDBDropdownItem onClick={this.setValueddl} value={valueddl.value} name={valueddl.label}>
        {valueddl.label}
      </MDBDropdownItem>
    ));
    const monthddl = monthNames.map(valueddl => (
      <MDBDropdownItem onClick={this.setMonthddl} value={valueddl.value} name={valueddl.name}>
        {valueddl.name}
      </MDBDropdownItem>
    ));
    const yearddl = this.state.YearNames.map(valueddl => (
      <MDBDropdownItem onClick={this.setYearddl} value={valueddl} >
        {valueddl}
      </MDBDropdownItem>
    ));
    


    const { completed } = this.state;

    return (
      <div>
        <MDBRow>
          <MDBCol md="12">
            <MDBCard>
              <MDBCardBody style={{ fontFamily: "Prompt" }}>
                <Grid container spacing={24}>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 3 }}
                  >
                    <h4 style={{ color: "green" }}>
                      <MDBIcon icon="paste" className="pink-text pr-3" />
                      &nbsp;รายงานการขนส่ง สะสมรายเดือน (แยกตามบริษัทขนส่ง)
                    </h4>
                    <h5 style={{ color: "#8089a9", textAlign: "right" }}>
                      <MDBIcon icon="user-edit" className="cyan-text pr-3" />
                      &nbsp;
                    </h5>
                  </Grid>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0 }}
                  >
                    <LinearProgress
                      color="secondary"
                      variant="determinate"
                      value={completed}
                    />
                    <hr />
                  </Grid>

                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0 }}
                  >
                    <MDBCard
                      style={{ width: "100%", marginTop: "0" }}
                      className="text-center"
                    >
                      <MDBCardHeader
                        style={{
                          borderBottomWidth: "0px",
                          backgroundColor: "#272262"
                        }}
                      >
                        <Grid container spacing={24}>
                          <Grid
                            item
                            lg={2}
                            xl={2}
                            xs={2}
                            sm={2}
                            md={2}
                            style={{ padding: 10,textAlign:'right' }}
                          >
                            <MDBBtnGroup>
                              <MDBDropdown>
                                <MDBDropdownToggle
                                  caret
                                  color="danger"
                                  className="h-100"
                                >
                                  {this.state.datasupname}
                                </MDBDropdownToggle>
                                <MDBDropdownMenu basic color="danger">
                                  {list}
                                </MDBDropdownMenu>
                              </MDBDropdown>
                            </MDBBtnGroup>
                          </Grid>
                          <Grid
                            item
                            lg={2}
                            xl={2}
                            xs={2}
                            sm={2}
                            md={2}
                            style={{
                              padding: 10,
                              textAlign:'right' 
                            }}
                          >
                           <MDBBtnGroup>
                              <MDBDropdown>
                                <MDBDropdownToggle
                                  caret
                                  color="danger"
                                  className="h-100"
                                >
                                  {this.state.monthnameselect}
                                </MDBDropdownToggle>
                                <MDBDropdownMenu basic color="danger">
                                {monthddl}
                                </MDBDropdownMenu>
                              </MDBDropdown>
                            </MDBBtnGroup>
                          </Grid>
                          <Grid
                            item
                            lg={1}
                            xl={1}
                            xs={1}
                            sm={1}
                            md={1}
                            style={{
                              padding: 10,
                              textAlign:'left' 
                             
                            }}
                          >
                           <MDBBtnGroup>
                              <MDBDropdown>
                                <MDBDropdownToggle
                                  caret
                                  color="danger"
                                  className="h-100"
                                >
                                  {this.state.yearnameselect}
                                </MDBDropdownToggle>
                                <MDBDropdownMenu basic color="danger">
                                {yearddl}
                                </MDBDropdownMenu>
                              </MDBDropdown>
                            </MDBBtnGroup>
                          </Grid>
                          <Grid
                            item
                            lg={1}
                            xl={1}
                            xs={1}
                            sm={1}
                            md={1}
                            style={{
                              padding: 0,
                              marginLeft: 5,
                              paddingTop: 15
                            }}
                          >
                            
                          </Grid>
                          {/* <Grid
                            item
                            lg={1}
                            xl={1}
                            xs={1}
                            sm={1}
                            md={1}
                            style={{
                              padding: 0,
                              marginLeft: 5,
                              paddingTop: 15
                            }}
                          >
                            <Datetime2 Updatedata={this.Updatedata} />
                          </Grid> */}

                          <Grid
                            item
                            lg={4}
                            xl={4}
                            xs={4}
                            sm={4}
                            md={4}
                            style={{ padding: 10 }}
                          >
                            <MDBBtn
                              color="default"
                              onClick={() => {
                                this.searchdata();
                              }}
                            >
                              ค้นหา <MDBIcon icon="search" className="ml-1" />
                            </MDBBtn>

                            <MDBBtn
                              color="secondary"
                              onClick={() => {
                                this.export();
                              }}
                            >
                              <MDBIcon
                                icon="calendar-alt"
                                className="mr-1"
                                size="sm"
                              />{" "}
                              Excel
                            </MDBBtn>
                          </Grid>
                        </Grid>
                      </MDBCardHeader>
                      <MDBCardBody
                        style={{ padding: 12, backgroundColor: "#272262" }}
                      >
                        <Grid container spacing={24}>
                          <Grid
                            item
                            lg={12}
                            xl={12}
                            xs={12}
                            sm={12}
                            md={12}
                            style={{ padding: 0 }}
                          >
                            <center>
                            <LoadingOverlay active={this.state.active} spinner text="loading...">
                              <Datagrid
                                datadoc={this.state.mdata}
                                loadprint={this.load_print}
                                CancelDocjob={this.CancelDocjob}
                              />
                              </LoadingOverlay>
                            </center>
                          </Grid>
                        </Grid>
                      </MDBCardBody>
                      <MDBCardFooter
                        style={{
                          borderTopWidth: "0px",
                          backgroundColor: "#272262",
                          color: "white"
                        }}
                      />
                    </MDBCard>
                  </Grid>
                </Grid>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
        </MDBRow>

        <ExcelExport
          data={this.state.mdata}
          // group={group}
          fileName={
            "ข้อมูลขนส่ง " +
            this.state.datasupname +
            "_" +
            this.state.monthnameselect +
            ".xlsx"
          }
          ref={exporter => {
            this._exporter = exporter;
          }}
        >
          <ExcelExportColumn
            field="doc_code"
            title="เลขที่เอกสาร"
            locked={true}
            width={150}
          />
          <ExcelExportColumn field="type_logis" title="ขนส่ง" width={150} />
          <ExcelExportColumn field="date_doc" title="วันที่ขนส่ง" width={150} />
          <ExcelExportColumn field="sup_name" title="บริษัท" width={200} />
          <ExcelExportColumn field="car_number" title="ทะเบียนรถ" width={200} />
          <ExcelExportColumn field="car_rout" title="เส้นทาง" width={150} />
          <ExcelExportColumn field="sup_name" title="บริษัท" width={200} />
          <ExcelExportColumn field="car_number" title="ทะเบียนรถ" width={100} />
          <ExcelExportColumn field="car_name" title="ชื่อคนขับ" width={100} />
          <ExcelExportColumn field="car_phone" title="เบอร์โทร" width={100} />
          <ExcelExportColumn field="doc_status" title="สถานะ" width={100} />
          <ExcelExportColumn field="logis_price" title="ค่าขนส่ง" width={100} />
          <ExcelExportColumn field="palate" title="จำนวนพาเลท" width={100} />
          <ExcelExportColumn field="branch" title="จำนวนสาขา" width={100} />
          <ExcelExportColumn field="box" title="จำนวนกล่อง" width={100} />
        </ExcelExport>
      </div>
    );
  }
}

export default withSnackbar(Reportbymonth);
