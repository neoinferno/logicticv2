import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Datagrid2 extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();

  constructor(props: any) {
      super(props);
     
     
  }
  
  public render() {
     const cellsrenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
      if (value > 0) {
        return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:red;"><strong>'+value+'</strong></div>';
    }
    else {
        return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:black;"><strong>'+value+'</strong></div>';
    }
    };
   
    const columns: any =[
        { text: 'วันที่',  editable: false, datafield: 'date_doc',  cellsalign: 'center',align: 'center', width: '10%',aggregates: ['count']},
        { text: 'บริษัทขนส่ง',editable: false,  datafield: 'sup_name',align: 'center', cellsalign: 'center',width: '20%' },
        { text: 'ใบงานทั้งหมด',editable: false, datafield: 'joball',cellsformat: 'n',  align: 'center', cellsalign: 'center',width: '10%',aggregates: ['sum'] },
        { text: 'ใบงานที่ปิดงาน',editable: false,  datafield: 'jobcomplate',cellsformat: 'n', align: 'center', cellsalign: 'center',width: '10%',aggregates: ['sum'] },
        { text: 'ใบงานที่ค้างปิด',editable: false, datafield: 'jobprocess',cellsformat: 'n',cellsrenderer,  align: 'center', cellsalign: 'center',width: '10%',aggregates: ['sum'] },
        { text: 'ใบงานยกเลิก',editable: false, datafield: 'jobcancel',cellsformat: 'n',cellsrenderer,  align: 'center', cellsalign: 'center',width: '10%',aggregates: ['sum'] },
        { text: 'มูลค่าขนส่ง(ปิดงาน)',editable: false, datafield: 'totalPrice',cellsformat: 'n',  align: 'center', cellsalign: 'center',width: '10%',aggregates: ['sum'] },
        { text: 'จำนวนกล่องที่(ปิดงาน)',editable: false,  datafield: 'totalbox',cellsformat: 'n', align: 'center', cellsalign: 'center',width: '10%',aggregates: ['sum'] },
        { text: 'จำนวนพาเลท(ปิดงาน)',editable: false,  datafield: 'totalpalate',cellsformat: 'n', align: 'center', cellsalign: 'center',width: '10%',aggregates: ['sum'] },  
    ]
    const source: any = {
      datafields: [
          { name: 'date_doc', type: 'string' },
          { name: 'sup_name', type: 'string' },
          { name: 'joball', type: 'number' },
          { name: 'jobcomplate', type: 'number' },
          { name: 'jobprocess', type: 'number' },
          { name: 'jobcancel', type: 'number' },
          { name: 'totalPrice', type: 'number' },
          { name: 'totalbox', type: 'number' },
          { name: 'totalpalate', type: 'number' },
      ],
      datatype: 'array',
      localdata:this.props.datadoc,
  };
      return (
        <JqxGrid
              ref={this.myGrid}
             
              theme="material-purple"
              showstatusbar={true}
              width={'99%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={true}
              height={450}
              enabletooltips={true} editable={true} selectionmode={'singlecell'}
              showaggregates={true} filterable={true} filtermode={'excel'}
        />
      );
  }





}
export default Datagrid2;