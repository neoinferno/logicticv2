import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Datagrid1 extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();

  constructor(props: any) {
      super(props);
     
     
  }
  
  public render() {
     const cellsrenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
        if (value === "ปิดงานแล้ว") {
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:orange;"><strong>'+value+'</strong></div>';
        }else if(value === "ยกเลิกเอกสาร"){
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:red;"><strong>'+value+'</strong></div>';
        }
        else {
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:green;"><strong>'+value+'</strong></div>';
        }
    };
    const columns: any =[
        { text: 'เลขที่เอกสาร',  editable: false, datafield: 'doc_code',  cellsalign: 'center',align: 'center', width: '10%',aggregates: ['count']},
        { text: 'ขนส่ง',editable: false,  datafield: 'type_logis',align: 'center', cellsalign: 'center',width: '10%' },
        { text: 'วันที่เอกสาร',editable: false, datafield: 'date_doc',  align: 'center', cellsalign: 'center',width: '10%' },
        { text: 'บริษัทขนส่ง',editable: false,  datafield: 'sup_name', align: 'center', cellsalign: 'center',width: '10%' },
        { text: 'ทะเบียนรถ',editable: false, datafield: 'car_number',  align: 'center', cellsalign: 'center',width: '5%' },
        { text: 'ชื่อผู้ขับ',editable: false, datafield: 'car_name',  align: 'center', cellsalign: 'center',width: '10%' },
        { text: 'สายรถ',editable: false, datafield: 'car_rout',  align: 'center', cellsalign: 'center',width: '10%' },
        { text: 'เบอร์โทร',editable: false,  datafield: 'car_phone', align: 'center', cellsalign: 'center',width: '8%' },
        { text: 'สถานะ',editable: false,  datafield: 'doc_status',cellsrenderer, align: 'center', cellsalign: 'center',width: '7%' },
        { text: 'ค่าขนส่ง',editable: false,  datafield: 'logis_price',cellsformat: 'n', align: 'center', cellsalign: 'center',width: '5%', aggregates: ["sum"]  },
        { text: 'จำนวนเลท',editable: false,  datafield: 'palate', align: 'center', cellsalign: 'center',width: '5%', aggregates: ["sum"]  },
        { text: 'จำนวนสาขา',editable: false,  datafield: 'branch', align: 'center', cellsalign: 'center',width: '5%', aggregates: ["sum"]  },
        { text: 'จำนวนกล่อง',editable: false,  datafield: 'box', align: 'center', cellsalign: 'center',width: '5%',  aggregates: ['sum'] },
       
    
    ]
    const source: any = {
      datafields: [
          { name: 'doc_code', type: 'string' },
          { name: 'type_logis', type: 'string' },
          { name: 'date_doc', type: 'string' },
          { name: 'sup_name', type: 'string' },
          { name: 'car_number', type: 'string' },
          { name: 'car_name', type: 'string' },
          { name: 'car_phone', type: 'string' },
          { name: 'doc_status', type: 'string' },
          { name: 'logis_price', type: 'number' },
          { name: 'branch', type: 'number' },
          { name: 'box', type: 'number' },
           { name: 'car_rout', type: 'string' },
         { name: 'palate', type: 'number' },
      ],
      datatype: 'array',
      localdata:this.props.datadoc,
  };
      return (
        <JqxGrid
              ref={this.myGrid}
             
              theme="material-purple"
              showstatusbar={true}
              width={'99%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={true}
              height={450}
              enabletooltips={true} editable={true} selectionmode={'singlecell'}
              showaggregates={true} filterable={true} filtermode={'excel'}
        />
      );
  }





}
export default Datagrid1;