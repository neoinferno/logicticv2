import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Datagridpre extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();

  constructor(props: any) {
      super(props);
     
     
  }
  
  public render() {
    const columns: any =[
 
        { text: 'วันที่โหลด',editable: false, datafield: 'loaddate',  align: 'center', cellsalign: 'center',width: '10%' },
        { text: 'ประมาณเวลาโหลด',editable: false,  datafield: 'loadtime', align: 'center', cellsalign: 'center',width: '10%' },
        { text: 'บริษัทขนส่ง',editable: false, datafield: 'transportname',  align: 'center', cellsalign: 'center',width: '20%' },
        { text: 'เส้นทาง-รอบ',editable: false, datafield: 'carnumber',  align: 'center', cellsalign: 'center',width: '10%' },
        { text: 'จำนวนพาเลท',editable: false, datafield: 'latenum',cellsformat: 'n',  align: 'center', cellsalign: 'center', aggregates: ["sum"] ,width: '10%' },
        { text: 'จำนวนกล่อง',editable: false,  datafield: 'boxnum',cellsformat: 'n', align: 'center', cellsalign: 'center',width: '10%', aggregates: ["sum"]  },
        { text: 'เส้นทาง',editable: false,  datafield: 'route', align: 'center', cellsalign: 'center',width: '10%' },

    ]
    const source: any = {
      datafields: [
          { name: 'loaddate', type: 'string' },
          { name: 'loadtime', type: 'string' },
          { name: 'transportname', type: 'string' },
          { name: 'carnumber', type: 'string' },
          { name: 'latenum', type: 'number' },
          { name: 'boxnum', type: 'number' },
          { name: 'route', type: 'string' },
         
      ],
      datatype: 'array',
      localdata:this.props.loaddata,
  };
      return (
        <JqxGrid
              ref={this.myGrid}
             
              theme="material-purple"
              showstatusbar={true}
              width={'99%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={true}
              editmode ={'click'} height={450}
              enabletooltips={true} editable={true} selectionmode={'singlecell'}
              showaggregates={true}
        />
      );
  }





}
export default Datagridpre;