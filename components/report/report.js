import React, { Component, Fragment } from "react";
import { MDBRow, MDBCol, MDBCard, MDBCardBody, MDBBtn } from "mdbreact";
import Grid from "@material-ui/core/Grid";
import { MDBIcon } from "mdbreact";
import LinearProgress from "@material-ui/core/LinearProgress";
import Barcode from "react-barcode";
import { NavLink } from "react-router-dom";
import ApiService from "../actions/apidata";
import AuthService from "../authlogin/AuthService";

export default class Report extends Component {
  constructor(props) {
    super(props);
    this.state = {
      completed: 0,
      loaddata: [],
      print_sup: "",
      loaddoc: "",
      palete: "",
      box_num: "",
      time: Date.now(),
      load: 0
    };
    this.Auth = new AuthService();
    this.ApiCall = new ApiService();
  }

  componentDidMount() {
    if (!this.Auth.loggedIn()) {
      window.location.href = "/Login";
    }
  }

  render() {
    return (
      <div>
        <MDBRow>
          <MDBCol md="12">
            <MDBCard>
              <MDBCardBody style={{ fontFamily: "Prompt" }}>
                <Grid container spacing={24}>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 3 }}
                  >
                    <h4
                      style={{
                        color: "green",
                        paddingBottom: 10,
                        paddingTop: 10,
                        marginLeft: 10
                      }}
                    >
                      <MDBIcon
                        icon="file-contract"
                        className="pink-text pr-3"
                      />
                      &nbsp;รายงานการขนส่งสินค้า
                    </h4>

                    {/* <h5 style={{ color: "#8089a9", textAlign: "right" }}>
                    <MDBIcon icon="user-edit" className="cyan-text pr-3" />
                    &nbsp;
                  </h5> */}
                  </Grid>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0 }}
                  >
                    <LinearProgress
                      color="secondary"
                      variant="determinate"
                      value={this.state.completed}
                    />
                    <hr />
                  </Grid>

                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0, color: "white" }}
                  >
                    <Fragment>
                      <NavLink to="/report1">
                        <MDBBtn gradient="peach"> <MDBIcon
                          icon="box"
                          className="black-text pr-3"
                          size="3x"
                        />
                          <k style={{ fontSize: 18 }}>
                            สรุปยอดส่ง รายวัน (By Sup)
                          </k>
                        </MDBBtn>
                      </NavLink>
                    </Fragment>
                  </Grid>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0, color: "white" }}
                  >
                    {" "}
                    <NavLink to="/report2" >
                      <MDBBtn gradient="purple">
                        <MDBIcon
                          icon="braille"
                          className="black-text pr-3"
                          size="3x"
                        />
                        <k style={{ fontSize: 18 }}>
                          สรุปยอดส่งสะสม รายเดือน (By Sup)
                        </k>
                      </MDBBtn>
                    </NavLink>

                  </Grid>

                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0, color: "white" }}
                  >
                    <Fragment>
                      <NavLink to="/Signature">
                        <MDBBtn gradient="blue"> <MDBIcon
                          icon="file-signature"
                          className="black-text pr-3"
                          size="3x"
                        />
                          <k style={{ fontSize: 18 }}>
                            ข้อมูลรับของ (Data Signa)
                          </k>
                        </MDBBtn>
                      </NavLink>
                    </Fragment>
                  </Grid>

                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0, color: "white" }}
                  >
                    {/* <NavLink to="report/report3" target="_blank">
                      <MDBBtn gradient="blue">
                        <k style={{ fontSize: 16 }}>ขนส่ง เจ.ไอ.บี </k>
                      </MDBBtn>
                    </NavLink> */}
                  </Grid>
                </Grid>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
        </MDBRow>
        <div style={{ margin: "0px" }} style={{ display: "none" }}>
          <iframe
            id={"ifmcontentstoprint"}
            style={{
              margin: "0px",
              height: "0px",
              width: "0px",
              position: "absolute"
            }}
          />
          <div id={"printarea"} style={{ margin: 0 }}>
            <div
              style={{
                fontSize: 16,
                textAlign: "left",
                padding: 0,
                margin: 0,
                marginLeft: 5
              }}
            >
              {this.state.print_sup} ( {this.state.box_num} ){" "}
            </div>
            <div style={{ textAlign: "center", padding: 0, margin: 0 }}>
              <Barcode
                value={this.state.loaddoc}
                fontSize={14}
                marginLeft={0}
                marginRight={10}
                format={"CODE128"}
                width={2}
                height={"60%"}
              />
            </div>
            <br />
            {/* <div style={{fontSize:16,textAlign:'right',margin:0,marginRight:10}}>{this.state.palete}</div> */}
          </div>
        </div>
      </div>
    );
  }
}
