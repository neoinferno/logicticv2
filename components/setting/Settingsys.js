import React, { Component } from "react";
import { MDBRow, MDBCol, MDBCard, MDBCardBody } from "mdbreact";
import Grid from "@material-ui/core/Grid";
import Select from "react-select";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import { SnackbarProvider } from "notistack";

// Page Ren
import Boxset from "./Boxset";
import Routeset from "./Routeset";

import ApiService from "../actions/apidata";
import AuthService from "../authlogin/AuthService";

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}
TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

export default class Settingsys extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0
    };
    this.Auth = new AuthService();
    this.ApiCall = new ApiService();
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };
  componentDidMount(){
    if (!this.Auth.loggedIn()) {
      window.location.href = "/Login";
    }
  }
  

  render() {
    const { classes } = this.props;
    const { value } = this.state;
    return (
      <MDBRow>
        <MDBCol md="12">
          <MDBCard>
            <MDBCardBody style={{ fontFamily: "Prompt" }}>
              <AppBar position="static">
                <Tabs value={value} onChange={this.handleChange}>
                  <Tab
                    style={{
                      fontSize: 14,
                      fontFamily: "Prompt",
                      background: "#040040"
                    }}
                    label="ตั้งค่าประเภทกล่องและขนาด (Box And Size)"
                  />
                  <Tab
                    style={{
                      fontSize: 14,
                      fontFamily: "Prompt",
                      background: "#040040"
                    }}
                    label="ตั้งค่าสาขาและเส้นทางขนส่ง (Router)"
                  />
                </Tabs>
              </AppBar>
              {value === 0 && (
                <TabContainer>
                  <SnackbarProvider
                    maxSnack={3}
                    anchorOrigin={{
                      vertical: "top",
                      horizontal: "right"
                    }}
                  >
                    <Boxset />
                  </SnackbarProvider>
                </TabContainer>
              )}
              {value === 1 && (
                <TabContainer>
                  <SnackbarProvider
                    maxSnack={3}
                    anchorOrigin={{
                      vertical: "top",
                      horizontal: "right"
                    }}
                  >
                    <Routeset />
                  </SnackbarProvider>
                </TabContainer>
              )}
              {/* </Grid> */}
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    );
  }
}
