import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import LinearProgress from "@material-ui/core/LinearProgress";
import { TextInput } from "grommet";
import { withSnackbar } from "notistack";
import { MDBBtn, MDBIcon } from "mdbreact";
import Boxgrid from "./boxgrid.tsx";
//API connect
import ApiService from "../actions/apidata";
class Boxset extends Component {
  constructor(props) {
    super(props);
    this.state = {
      completed: 0,
      buffer: 10,
      description: "",
      wide: "",
      long: "",
      high: "",
      volume: "",
      boxdetail:[]
    };
    this.setBoxnow = this.setBoxnow.bind(this);
    this.onUpdatedata = this.onUpdatedata.bind(this);
    this.ApiCall = new ApiService();
  }
  processvolume() {
    this.setState(
      {
        volume: this.state.wide * this.state.long * this.state.high
      },
      () => {}
    );
  }

  clearvolume() {
    this.setState(
      {
        volume: 0,
        wide: "",
        long: "",
        high: "",
        description: ""
      },
      () => {
      }
    );
  }
  componentWillMount(){
    this.getBoxdetail()
  }
  getBoxdetail(){
    this.ApiCall.GetDetailBox()
    .then(res => {
      if (res.status === true) {
        this.setState({
          boxdetail:res.data
        })
      } 
    })
    .catch(error => {
      console.error(error.message);
    });
  }
  onUpdatedata(rowdata){
    
      let boxarray = Array();
      boxarray = {
        type: 2,
        description: rowdata.box_detail,
        wide: rowdata.wide,
        long: rowdata.long,
        high: rowdata.high,
        volume: rowdata.wide*rowdata.long*rowdata.high
      };

      this.ApiCall.SetBox(boxarray)
        .then(res => {
          console.log(res);
          if (res.status === true) {
            this.props.enqueueSnackbar("อัพเดทข้อมูล สำเร็จแล้ว", {
              variant: "success"
            });
            this.getBoxdetail();
          } else if (res.status === false) {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการให้สำเร็จได้ หรือ มีรายการนี้อยู่แล้ว ลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          } else {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          }
        })
        .catch(error => {
          console.error(error.message);
        });
  }
  setBoxnow() {
    if (
      this.state.description === "" ||
      this.state.wide === "" ||
      this.state.long === "" ||
      this.state.high === ""
    ) {
      this.props.enqueueSnackbar(
        "Warning กรุณา ระบุใส่ข้อมูลให้ถูกต้อง ครบถ้วน??",
        {
          variant: "warning"
        }
      );
    } else {
      let boxarray = Array();
      boxarray = {
        type: 1,
        description: this.state.description,
        wide: this.state.wide,
        long: this.state.long,
        high: this.state.high,
        volume: this.state.volume
      };

      this.ApiCall.SetBox(boxarray)
        .then(res => {
          console.log(res);
          if (res.status === true) {
            this.props.enqueueSnackbar("บันทึกข้อมูล สำเร็จแล้ว", {
              variant: "success"
            });
            this.getBoxdetail();
            this.clearvolume();
          } else if (res.status === false) {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการให้สำเร็จได้ หรือ มีรายการนี้อยู่แล้ว ลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          } else {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          }
        })
        .catch(error => {
          console.error(error.message);
        });
    }
  }

  render() {
    const { completed } = this.state;
    return (
      <div style={{ fontFamily: "Prompt" }}>
        <Grid container spacing={24}>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 0 }}
          />
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <h4 style={{ color: "green" }}>
              <br />
              <MDBIcon icon="cogs" className="cyan-text pr-3" />&nbsp;กำหนดประเภทกล่อง (ขนาดกล่อง)
            </h4>
          </Grid>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <LinearProgress
              color="secondary"
              variant="determinate"
              value={completed}
            />
            <hr />
          </Grid>
          <Grid item lg={6} xl={6} xs={6} sm={6} md={6} style={{ padding: 3 }}>
            <Grid container spacing={24}>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >
                <h5
                  style={{
                    color: "#8089a9",
                    textAlign: "left",
                    marginLeft: 10
                  }}
                >
                  เพิ่มประเภทกล่องใหม่{" "}
                  {/* <i style={{ color: "#596eb9" }}>{this.state.code_route}</i> */}
                </h5>
              </Grid>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              />
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}> ประเภทกล่อง</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              >
                <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.description}
                  // placeholder="exp A1"
                  ref={input => {
                    this.description = input;
                  }}
                  onChange={event =>
                    this.setState({ description: event.target.value }, () => {})
                  }
                />
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              />
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}> ขนาด</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={1}
                xl={1}
                xs={1}
                sm={1}
                md={1}
                style={{ padding: 3 }}
              >
                <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.wide}
                  placeholder="กว้าง"
                  ref={input => {
                    this.wide = input;
                  }}
                  onChange={event =>
                    this.setState({ wide: event.target.value }, () => {
                      this.processvolume();
                    })
                  }
                />
              </Grid>
              <Grid
                item
                lg={1}
                xl={1}
                xs={1}
                sm={1}
                md={1}
                style={{ padding: 3 }}
              >
                <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.long}
                  placeholder="ยาว"
                  ref={input => {
                    this.long = input;
                  }}
                  onChange={event =>
                    this.setState({ long: event.target.value }, () => {
                      this.processvolume();
                    })
                  }
                />
              </Grid>
              <Grid
                item
                lg={1}
                xl={1}
                xs={1}
                sm={1}
                md={1}
                style={{ padding: 3 }}
              >
                <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.high}
                  placeholder="สูง"
                  ref={input => {
                    this.high = input;
                  }}
                  onChange={event =>
                    this.setState({ high: event.target.value }, () => {
                      this.processvolume();
                    })
                  }
                />
              </Grid>
              <Grid
                item
                lg={5}
                xl={5}
                xs={5}
                sm={5}
                md={5}
                style={{ padding: 3 }}
              />
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}> ปริมาตรกล่อง</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={3}
                xl={3}
                xs={3}
                sm={3}
                md={3}
                style={{ padding: 3 }}
              >
                <TextInput
                  disabled={true}
                  style={{
                    width: "100%",
                    color: "#3f51b5",
                    fontSize: 14,
                    opacity: 10
                  }}
                  size="xsmall"
                  value={this.state.volume}
                  // placeholder="exp A1"
                  ref={input => {
                    this.description = input;
                  }}
                />
              </Grid>
              <Grid
                item
                lg={1}
                xl={1}
                xs={1}
                sm={1}
                md={1}
                style={{ padding: 0 }}
              >
                <h6 style={{ textAlign: "left", marginTop: 10 }}>
                  <strong style={{ fontSize: 16, color: "#3f51b5" }}>
                    {" "}
                    cm<sup>3</sup>
                  </strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={1}
                xl={1}
                xs={1}
                sm={1}
                md={1}
                style={{ padding: 0 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 0 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 10 }}
              >
                <MDBBtn
                  color="primary"
                  onClick={() => {
                    this.clearvolume();
                  }}
                >
                  <MDBIcon icon="undo" className="mr-1" /> ล้างข้อมูล
                </MDBBtn>
                <MDBBtn
                  color="default"
                  onClick={() => {
                    this.setBoxnow();
                  }}
                >
                  บันทึกข้อมูลกล่อง <MDBIcon icon="save" className="ml-1" />
                </MDBBtn>
              </Grid>
            </Grid>
          </Grid>
          <Grid item lg={6} xl={6} xs={6} sm={6} md={6} style={{ padding: 3 }}>
            <Grid container spacing={24}>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >
                <Grid
                  item
                  lg={12}
                  xl={12}
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 3 }}
                >
                  <h5
                    style={{
                      color: "#8089a9",
                      textAlign: "left",
                      marginLeft: 10
                    }}
                  >
                    อัพเดทข้อมูลกล่อง{" "}
                    <i style={{ color: "#596eb9" }}>{this.state.code_route}</i>
                  </h5>
                </Grid>
                <Grid item lg={12} xl={12} xs={12} sm={12} md={12}>
                  <Boxgrid onUpdatedata={this.onUpdatedata} boxdetail={this.state.boxdetail}/>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withSnackbar(Boxset);
