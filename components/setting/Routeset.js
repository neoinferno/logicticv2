import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import LinearProgress from "@material-ui/core/LinearProgress";
import { TextInput, Button } from "grommet";
import { withSnackbar } from "notistack";
import { MDBIcon } from "mdbreact";
import Routegrid from "./routegrid.tsx";
import Routegridhave from "./routegridhave.tsx";
//API connect
import ApiService from "../actions/apidata";
class Routeset extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code_route: "",
      completed: 0,
      buffer: 10,
      mdatahave: [],
      mbranch: "",
      mdatanon: []
    };
    this.getbranch = this.getbranch.bind(this);
    this.setRoute = this.setRoute.bind(this);
    this.ApiCall = new ApiService();
  }

  progress = () => {
    const { completed } = this.state;
    if (completed === 100) {
      this.setState({ completed: 0 });
    } else {
      const diff = Math.random() * 10;
      this.setState({ completed: Math.min(completed + diff, 100) });
    }
  };
  setRoute(value) {
    this.setState({ mbranch: value.branch });
  }
  upsend(value) {
    if (value == 1) {
      var dataupdate = {
        branch: this.state.mbranch,
        route: ""
      };
    } else {
      var dataupdate = {
        branch: this.state.mbranch,
        route: this.state.code_route
      };
    }

    if (this.state.mbranch === "" || this.state.code_route === "") {
      this.props.enqueueSnackbar(
        "ไม่สามารถทำรายการให้สำเร็จได้ โปรดเลือกรายการ..",
        {
          variant: "error"
        }
      );
    } else {
      this.ApiCall.UpdateRoute(dataupdate)
        .then(res => {
          if (res.status === true) {
            this.props.enqueueSnackbar("อัพเดทข้อมูล สำเร็จแล้ว", {
              variant: "success"
            });
            this.getbranch();
          } else if (res.status === false) {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการให้สำเร็จได้ หรือ มีรายการนี้อยู่แล้ว ลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          } else {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          }
        })
        .catch(error => {
          console.error(error.message);
        });
    }
  }
  
  getbranch() {
    var route = this.state.code_route;
    this.ApiCall.TTB_GetBranch(route)
      .then(res => {
        this.setState({ mdatahave: res.Have });
        this.setState({ mdatanon: res.Non });
      })
      .catch(error => {
        console.error(error.message);
      });
  }

  render() {
    const { completed } = this.state;
    return (
      <div style={{ fontFamily: "Prompt" }}>
        <Grid container spacing={24}>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 0 }}
          />
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <h4 style={{ color: "green" }}>
              <br />
              <MDBIcon icon="cogs" className="cyan-text pr-3" />
              &nbsp;ตั้งค่าสาขาและเส้นทางขนส่ง
            </h4>
          </Grid>
          <Grid
            item
            lg={10}
            xl={10}
            xs={10}
            sm={10}
            md={10}
            style={{ padding: 3 }}
          >
            <br />
            <h5 style={{ color: "#8089a9", textAlign: "left", marginLeft: 10 }}>
              เส้นทางที่เลือก{" "}
              <i style={{ color: "#596eb9" }}>{this.state.code_route}</i>
            </h5>
          </Grid>
          <Grid item lg={2} xl={2} xs={2} sm={2} md={2} style={{ padding: 3 }}>
            <h6 style={{ textAlign: "right" }}>
              <strong style={{ fontSize: 16 }}>ค้นหา ด้วยรหัสเส้นทาง</strong>
            </h6>
            <TextInput
              style={{ width: "100%", fontSize: 14 }}
              size="xsmall"
              value={this.state.code_route}
              placeholder="exp 031"
              ref={input => {
                this.code_route = input;
              }}
              onChange={event =>
                this.setState({ code_route: event.target.value }, () => {})
              }
              onKeyPress={event => {
                if (event.key === "Enter") {
                  this.getbranch();
                }
              }}
            />
          </Grid>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <LinearProgress
              color="secondary"
              variant="determinate"
              value={completed}
            />
            <hr />
          </Grid>

          <Grid item lg={6} xl={6} xs={6} sm={6} md={6} style={{ padding: 3 }}>
            <Grid container spacing={24}>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >
                <Grid
                  item
                  lg={11}
                  xl={11}
                  xs={11}
                  sm={11}
                  md={11}
                  style={{ padding: 3 }}
                >
                  <h5
                    style={{
                      color: "#8089a9",
                      textAlign: "left",
                      marginLeft: 10,
                      color: "blue"
                    }}
                  >
                    สาขาที่ไม่อยู่{" "}
                  </h5>
                  <div align="right">
                    <Button
                      label={<MDBIcon icon="arrow-circle-right" />}
                      onClick={() => {
                        this.upsend(0);
                      }}
                    />
                  </div>
                </Grid>

                <Grid item lg={12} xl={12} xs={12} sm={12} md={12}>
                  <Routegrid
                    setRoute={this.setRoute}
                    getbranch={this.getbranch}
                    mdatanon={this.state.mdatanon}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>

          <Grid item lg={6} xl={6} xs={6} sm={6} md={6} style={{ padding: 3 }}>
            <Grid container spacing={24}>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >
                <Grid
                  item
                  lg={11}
                  xl={11}
                  xs={11}
                  sm={11}
                  md={11}
                  style={{ padding: 3 }}
                >
                  <h5
                    style={{
                      color: "#8089a9",
                      textAlign: "right",
                      marginLeft: 10,
                      color: "blue"
                    }}
                  >
                    สาขาที่มีอยู่{" "}
                    <i style={{ color: "#596eb9" }}>{this.state.code_route}</i>
                  </h5>
                  <div align="left">
                    <Button
                      label={<MDBIcon icon="arrow-circle-left" />}
                      onClick={() => {
                        this.setState(
                          {
                            route: ""
                          },
                          () => {
                            this.upsend(1);
                          }
                        );
                      }}
                    />
                  </div>
                </Grid>
                <Grid item lg={12} xl={12} xs={12} sm={12} md={12}>
                  <Routegridhave
                    getbranch={this.getbranch}
                    mdatahave={this.state.mdatahave}
                    setRoute={this.setRoute}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withSnackbar(Routeset);
