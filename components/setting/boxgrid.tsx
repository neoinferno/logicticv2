import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Boxgrid extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();
  constructor(props: any) {
      super(props);
  }
 
  public render() {
    const cellsrenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
        if (value === 0) {
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:red;"><strong>'+value+'</strong></div>';
        }
        else {
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:green;"><strong>'+value+'</strong></div>';
        }
    };
    const columngroups: any =[
        { text: 'ขนาดกล่อง', align: 'center', name: 'boxsize' }
    ]
    const columns: any =[
        { text: 'ประเภท(กล่อง)', pinned: true, editable: false,  datafield: 'box_detail', cellsalign: 'center',align: 'center', width: '30%' },
        { text: 'ความกว้าง ( W )', columngroup: 'boxsize', cellsrenderer, datafield: 'wide', align: 'center', cellsalign: 'center',width: '15%' },
        { text: 'ความยาว ( L )', columngroup: 'boxsize',cellsrenderer, datafield: 'long', align: 'center', cellsalign: 'center',width: '15%' },
        { text: 'ความสูง ( H )',columngroup: 'boxsize', cellsrenderer, datafield: 'high', align: 'center', cellsalign: 'center',width: '15%' },
        { text: 'ปริมาตร ( W x L x H )',pinned: true,editable: false, datafield: 'volume', align: 'center', cellsalign: 'center',width: '25%' },
    ]
    const source: any = {
      datafields: [
          { name: 'box_detail', type: 'string' },
          { name: 'wide', type: 'number' },
          { name: 'long', type: 'number' },
          { name: 'high', type: 'number' },
          { name: 'volume', type: 'number' },
      ],
      datatype: 'array',
       localdata:this.props.boxdetail,
      updaterow: (rowid: any, rowdata: any, commit: any): void => {
        this.props.onUpdatedata(rowdata);
        commit(true);
    }
  };
      return (
        <JqxGrid
              ref={this.myGrid}
              theme="metrodark"
              width={'90%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={false}
              editmode ={'click'}
              enabletooltips={true} editable={true} selectionmode={'multiplecellsadvanced'}
              columngroups={columngroups}
        />
      );
  }
}
export default Boxgrid;