import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Routegrid extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();
  
  constructor(props: any) {
      super(props);
      this.myGridOnRowSelect = this.myGridOnRowSelect.bind(this);
  }
 
  public render() {
   
    
    const columns: any =[
        { text: 'ID', pinned: true,datafield: 'branch', editable: false,  cellsalign: 'center',align: 'center', width: '10%' },
        { text: 'สาขา', pinned: true,datafield: 'branchname', editable: false,  cellsalign: 'center',align: 'center', width: '40%' },
        { text: 'เบอร์โทรศัพท์' , align: 'center',datafield: 'tel', cellsalign: 'center',width: '30%' },
        { text: 'เส้นทาง' , align: 'center', datafield: 'route',cellsalign: 'center',width: '20%' }
    ]
    const source: any = {
      datafields: [
        { name: 'branch', type: 'string' },
        { name: 'branchname', type: 'string' },
        { name: 'tel', type: 'string' },
        { name: 'route', type: 'string' },
      ],
      datatype: 'array',
       localdata:this.props.mdatanon,
      updaterow: (rowid: any, rowdata: any, commit: any): void => {
        // this.props.onUpdatedata(rowdata);
        commit(true);
    }
  };
      return (
        <JqxGrid
              ref={this.myGrid}
              onRowselect={this.myGridOnRowSelect}
              theme="metrodark"
              width={'90%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={false}
             
              enabletooltips={true} editable={false} selectionmode={'singlerow'}
             
        />
      );
      
  }
  private myGridOnRowSelect(event: any): void {
    let value = this.myGrid.current!.getrowdata(event.args.rowindex);
    // console.log(value.branch)
    this.props.setRoute(value);
 
  };
}
export default Routegrid;