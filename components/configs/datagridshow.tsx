import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
let databranch =Array();
class Datagrid extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();
  constructor(props: any) {
      super(props);
  }
 
  public render() {
    const columns: any =[
        { text: 'รหัสเส้นทาง', pinned: true, editable: false,  datafield: 'route', cellsalign: 'center',align: 'center', width: '45%' },
        { text: 'สาขา', datafield: 'branchname', align: 'center', cellsalign: 'center',width: '54.5%' }
    ]
    const source: any = {
      datafields: [
          { name: 'rank', type: 'string' },
          { name: 'branch', type: 'string' },
          { name: 'route', type: 'string' },
          { name: 'branchname', type: 'string' },
      ],
      datatype: 'array',
      localdata:this.props.databranch,
  };
      return (
          <JqxGrid
              ref={this.myGrid}
              theme="metrodark"
              width={'90%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={false}
              enabletooltips={true} editable={false} selectionmode={'multiplecellsadvanced'}
               />
      );
  }
}
export default Datagrid;