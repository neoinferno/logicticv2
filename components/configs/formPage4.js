import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import { TextInput } from "grommet";
import { withSnackbar } from "notistack";
import "./form.css";
import { MDBIcon } from "mdbreact";
import LinearProgress from "@material-ui/core/LinearProgress";
import DataGridSet from "./jibdatagrid.tsx";
import DataGridShow from "./datagridshow.tsx";
import DataGridTitle from "./datagridtitle.tsx";
//API connect
import ApiService from "../actions/apidata";
class formPage4 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code_route: "",
      Routetitle:[],
      Routemon:[],
      completed: 0,
      buffer: 10,
      databranch: [],
      carname: []
    };
    this.ApiCall = new ApiService();
    this.SearchData = this.SearchData.bind(this);
    this.SearchDataMon = this.SearchDataMon.bind(this);
    this.onUpdatedata = this.onUpdatedata.bind(this);
    this.setCode_Route = this.setCode_Route.bind(this);
  }

  componentDidMount(){
    this.SearchData()
  }
  progress = () => {
    const { completed } = this.state;
    if (completed === 100) {
      this.setState({ completed: 0 });
    } else {
      const diff = Math.random() * 10;
      this.setState({ completed: Math.min(completed + diff, 100) });
    }
  };
  SearchData = () => {
    let varSearch = Array();
    if (typeof this.props.supcar.value === "undefined") {
      this.props.enqueueSnackbar(
        "Warning กรุณา ระบุบริษัทขนส่งก่อนเลือกเส้นทาง",
        {
          variant: "warning"
        }
      );
    } else {
        // console.log(this.props.supcar)
            this.ApiCall.getrouteJIB(this.props.supcar.value)
              .then(res => {
                if (res.status === true) {
                  this.setState({ Routetitle: res.data }, () => {});
                } else if (res.status === false) {
                  this.setState({ Routetitle: [] }, () => {});
                  console.log(res.message);
                } else {
                  console.log(res.message);
                }
              })
              .catch(error => {
                console.error(error.message);
              });   
    }
  };
  SearchDataMon = () => {
    let varSearch = Array();
    if (typeof this.props.supcar.value === "undefined") {
      this.props.enqueueSnackbar(
        "Warning กรุณา ระบุบริษัทขนส่งก่อนเลือกเส้นทาง",
        {
          variant: "warning"
        }
      );
    } else {
        varSearch={
            supid:this.props.supcar.value,
            route:this.state.code_route
        }
        // console.log(varSearch)
        // console.log(this.props.supcar)
            this.ApiCall.getrouteMonjib(varSearch)
              .then(res => {
                 
                if (res.status === true) {
                  this.setState({ Routemon: res.data }, () => {});
                } else if (res.status === false) {
                  this.setState({ Routemon: [] }, () => {});
                  console.log(res.message);
                } else {
                  console.log(res.message);
                }
              })
              .catch(error => {
                console.error(error.message);
              });   
    }
  };
  

  onUpdatedata(rowsdata) {
    let dataupdate = Array();
   
    dataupdate = {
      supid: this.props.supcar.value,
      route: this.state.code_route,
      m_money: rowsdata.m_money,
      oil: rowsdata.oil,
      r_money: rowsdata.r_money,
      money_child: rowsdata.money_child,
      expressway: rowsdata.expressway,
    };
    // console.log(dataupdate)
    this.ApiCall.UpdateJibPrice(dataupdate)
      .then(res => {
        if (res.status === true) {
          this.props.enqueueSnackbar("บันทึกข้อมูล สำเร็จแล้ว", {
            variant: "success"
          });
        //   this.props.getRoutetitle();
        } else if (res.status === false) {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
        } else {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  setCode_Route(code_id) {
    this.setState(
      {
        code_route: code_id
      },
      () => {
        this.SearchDataMon();
      }
    );
  }
  render() {
    const { completed } = this.state;
    return (
      <div style={{ fontFamily: "Prompt" }}>
        <Grid container spacing={24}>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 0 }}
          />
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <h4 style={{ color: "green" }}>
              <br />
              <MDBIcon icon="baby-carriage" size="2x" className="red-text pr-3"/>&nbsp;บันทึกข้อมูลราคาขนส่ง (JIB){" "}
              <i style={{ color: "#1200ff" }}>{this.props.supcar.label}</i>
            </h4>
          </Grid>
          <Grid
            item
            lg={10}
            xl={10}
            xs={10}
            sm={10}
            md={10}
            style={{ padding: 3 }}
          >
            <br />
            <h5 style={{ color: "#8089a9", textAlign: "left", marginLeft: 10 }}>
              เส้นทางที่เลือก{" "}
              <i style={{ color: "#596eb9" }}>{this.state.code_route}</i>
            </h5>
          </Grid>
          <Grid item lg={2} xl={2} xs={2} sm={2} md={2} style={{ padding: 3 }}>
            <h6 style={{ textAlign: "right" }}>
              <strong style={{ fontSize: 16 }}>ค้นหา ด้วยรหัสเส้นทาง</strong>
            </h6>
            <TextInput
              style={{ width: "100%", fontSize: 14 }}
              size="xsmall"
              value={this.state.code_route}
              placeholder="exp 031"
              ref={input => {
                this.code_route = input;
              }}
              onChange={event =>
                this.setState({ code_route: event.target.value }, () => {})
              }
              onKeyPress={event => {
                if (event.key === "Enter") {
                  this.SearchDataMon();
                }
              }}
            />
          </Grid>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <LinearProgress
              color="secondary"
              variant="determinate"
              value={completed}
            />
            <hr />
          </Grid>
          <Grid item lg={3} xl={3} xs={3} sm={3} md={3} style={{ padding: 0 }}>
            <center>
              <DataGridTitle
                databranch={this.state.Routetitle}
                setCode_Route={this.setCode_Route}
              />
            </center>
          </Grid>
          <Grid item lg={9} xl={9} xs={9} sm={9} md={9} style={{ padding: 0 }}>
            <center>
              <DataGridSet
                databranch={this.state.Routemon}
                onUpdatedata={this.onUpdatedata}
              />
            </center>
          </Grid>
        </Grid>
      </div>
    );
  }
}
export default withSnackbar(formPage4);
