import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
let databranch =Array();
class Datagrid extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();
  private selectedRowIndex = React.createRef<HTMLSpanElement>();
  constructor(props: any) {
      super(props);
      this.myGridOnRowSelect = this.myGridOnRowSelect.bind(this);
  }
  public render() {
    const cellsrenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
        if (rowdata.set_num === 0) {
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:white;"><strong>'+value+'</strong></div>';
        }
        else {
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:#ff5400;"><strong>'+value+'</strong></div>';
        }
    };
    const columns: any =[
        { text: 'รหัสเส้นทาง',cellsrenderer, pinned: true, editable: false,  datafield: 'route', cellsalign: 'center',align: 'center', width: '45%' },
        { text: 'จำนวนสาขา',cellsrenderer, datafield: 'branch_num', align: 'center', cellsalign: 'center',width: '54.5%' }
    ]
    const source: any = {
      datafields: [
          { name: 'route', type: 'string' },
          { name: 'branch_num', type: 'number' },
          { name: 'set_num', type: 'number' },
      ],
      datatype: 'array',
      localdata:this.props.databranch,

  };
      return (
          <JqxGrid
              onRowselect={this.myGridOnRowSelect}
              ref={this.myGrid}
              theme="metrodark"
              width={'90%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={false}
              enabletooltips={true} editable={false} selectionmode={'singlerow'}
               />
      );
  }
  private myGridOnRowSelect(event: any): void {
    let value = this.myGrid.current!.getrowdata(event.args.rowindex);
    this.props.setCode_Route(value.route);
  };
}

export default Datagrid;