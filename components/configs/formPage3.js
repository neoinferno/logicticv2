import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import { TextInput } from "grommet";
import { withSnackbar } from "notistack";
import "./form.css";
import LinearProgress from "@material-ui/core/LinearProgress";

import { MDBBtn, MDBIcon } from "mdbreact";
//API connect
import ApiService from "../actions/apidata";
import Datagridweight from "./datagridweight.tsx";

class formPage3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      completed: 0,
      buffer: 10,
      weight:'',
      price: "",
    };
    this.clearvolume = this.clearvolume.bind(this);
    this.onUpdatedata = this.onUpdatedata.bind(this);
    this.ApiCall = new ApiService();
  }
 

 
  clearvolume() {
    this.setState({
      weight: "",
      price: ""
    });
  }

  onUpdatedata(rowdata){
    let dataupdate = Array();
        dataupdate = {
          sup_id: rowdata.sup_id,
          weight: rowdata.weight,
          type: 0,
          price: rowdata.price
        };
        this.ApiCall.UpdatePriceWeight(dataupdate)
          .then(res => {
            if (res.status === true) {
              this.props.enqueueSnackbar("อัพเดทข้อมูล สำเร็จแล้ว", {
                variant: "success"
              });
              this.props.getGridbox();
              this.clearvolume();
            } else if (res.status === false) {
              this.props.enqueueSnackbar(
                "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
                {
                  variant: "error"
                }
              );
            } else {
              this.props.enqueueSnackbar(
                "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
                {
                  variant: "error"
                }
              );
            }
          })
          .catch(error => {
            console.error(error.message);
          });
  }
  setPricenow() {
    if (typeof this.props.supcar.value === "undefined") {
      this.props.enqueueSnackbar("Warning กรุณา เลือกบริษัทขนส่ง..", {
        variant: "warning"
      });
    } else {
      if (
        this.state.weight === "" ||
        this.state.price === ""
      ) {
          this.props.enqueueSnackbar(
            "Warning คุณใส่ข้อมูลไม่ถูกต้อง ลองใหม่อีกครั้ง",
            {
              variant: "warning"
            }
          );
       
      } else {
        let dataupdate = Array();
        dataupdate = {
          sup_id: this.props.supcar.value,
          weight:this.state.weight,
          type: 0,
          price: this.state.price
        };
        this.ApiCall.UpdatePriceWeight(dataupdate)
          .then(res => {
            if (res.status === true) {
              this.props.enqueueSnackbar("บันทึกข้อมูล สำเร็จแล้ว", {
                variant: "success"
              });
              this.props.getGridWeight();
              this.clearvolume();
            } else if (res.status === false) {
              this.props.enqueueSnackbar(
                "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
                {
                  variant: "error"
                }
              );
            } else {
              this.props.enqueueSnackbar(
                "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
                {
                  variant: "error"
                }
              );
            }
          })
          .catch(error => {
            console.error(error.message);
          });
      }
    }
  }

  render() {
    const { completed } = this.state;
    return (
      <div style={{ fontFamily: "Prompt" }}>
        <Grid container spacing={24}>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 0 }}
          />
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <h4 style={{ color: "green" }}>
              <br />
              <MDBIcon icon="balance-scale" size="2x" className="red-text pr-3"/>&nbsp;บันทึกข้อมูลราคาขนส่ง (แบบชั่งน้ำหนัก){" "}
              <i style={{ color: "#1200ff" }}>{this.props.supcar.label}</i>
            </h4>
          </Grid>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <LinearProgress
              color="secondary"
              variant="determinate"
              value={completed}
            />
            <hr />
          </Grid>
          <Grid item lg={6} xl={6} xs={6} sm={6} md={6} style={{ padding: 3 }}>
            <Grid container spacing={24}>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >
                <h5
                  style={{
                    color: "#8089a9",
                    textAlign: "left",
                    marginLeft: 10
                  }}
                >
                  เพิ่มช่วงน้ำหนักและราคา{" "}
                  <i style={{ color: "#596eb9" }}>{this.state.code_route}</i>
                </h5>
              </Grid>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              />
              
             
              <Grid
                item
                lg={3}
                xl={3}
                xs={3}
                sm={3}
                md={3}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}> น้ำหนักไม่เกิน</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={3}
                xl={3}
                xs={3}
                sm={3}
                md={3}
                style={{ padding: 3 }}
              >
                <TextInput
                  disabled={false}
                  style={{
                    width: "100%",
                    color: "#3f51b5",
                    fontSize: 14,
                    opacity: 10
                  }}
                  size="xsmall"
                  value={this.state.weight}
                  // placeholder="exp A1"
                  ref={input => {
                    this.weight = input;
                  }}
                  onChange={event =>
                    this.setState({ weight: event.target.value }, () => {})
                  }
                />
              </Grid>
              <Grid
                item
                lg={1}
                xl={1}
                xs={1}
                sm={1}
                md={1}
                style={{ padding: 0 }}
              >
                <h6 style={{ textAlign: "left", marginTop: 10 }}>
                  <strong style={{ fontSize: 16, color: "#3f51b5" }}>
                    {" "}
                    kilogram
                  </strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={3}
                xl={3}
                xs={3}
                sm={3}
                md={3}
                style={{ padding: 0 }}
              />
              <Grid
                item
                lg={3}
                xl={3}
                xs={3}
                sm={3}
                md={3}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}> ราคาค่าขนส่ง ( บาท ) </strong>
                </h6>

              </Grid>
              <Grid
                item
                lg={5}
                xl={5}
                xs={5}
                sm={5}
                md={5}
                style={{ padding: 3 }}
              >
                <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.price}
                  // placeholder="exp A1"
                  ref={input => {
                    this.price = input;
                  }}
                  onChange={event =>
                    this.setState({ price: event.target.value }, () => {})
                  }
                />
              </Grid>
              <Grid
                item
                lg={5}
                xl={5}
                xs={5}
                sm={5}
                md={5}
                style={{ padding: 3 }}
              />
              <Grid
                item
                lg={1}
                xl={1}
                xs={1}
                sm={1}
                md={1}
                style={{ padding: 0 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 0 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 10 }}
              >
                <MDBBtn
                  color="primary"
                  onClick={() => {
                    this.clearvolume();
                  }}
                >
                  <MDBIcon icon="undo" className="mr-1" /> ล้างข้อมูล
                </MDBBtn>
                <MDBBtn
                  color="default"
                  onClick={() => {
                    this.setPricenow();
                  }}
                >
                  บันทึกราคากล่อง <MDBIcon icon="save" className="ml-1" />
                </MDBBtn>
              </Grid>
            </Grid>
          </Grid>
          <Grid item lg={6} xl={6} xs={6} sm={6} md={6} style={{ padding: 3 }}>
            <Grid container spacing={24}>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >
                <Grid
                  item
                  lg={12}
                  xl={12}
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 3 }}
                >
                  <h5
                    style={{
                      color: "#8089a9",
                      textAlign: "left",
                      marginLeft: 10
                    }}
                  >
                    อัพเดทราคาตามน้ำหนัก{" "}
                    <i style={{ color: "#596eb9" }}>{this.state.code_route}</i>
                  </h5>
                </Grid>
                <Grid item lg={12} xl={12} xs={12} sm={12} md={12}>
                  <Datagridweight Gridweight={this.props.Gridweight} onUpdatedata={this.onUpdatedata}/>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withSnackbar(formPage3);
