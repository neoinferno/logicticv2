import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Datagridbox extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();
  constructor(props: any) {
      super(props);
  }
 
  public render() {
    const cellsrenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
        if (value === 0) {
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:red;"><strong>'+value+'</strong></div>';
        }
        else {
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:green;"><strong>'+value+'</strong></div>';
        }
    };
    const columns: any =[
        { text: 'บริษัทขนส่ง', pinned: true, editable: false,  datafield: 'transportname', cellsalign: 'center',align: 'center', width: '35%' },
        { text: 'ประเภทกล่อง',pinned: true, editable: false, datafield: 'label', align: 'center', cellsalign: 'center',width: '30%' },
        { text: 'ปริมาตรกล่อง',pinned: true, editable: false, datafield: 'volume', align: 'center', cellsalign: 'center',width: '15%' },
        { text: 'ราคาขนส่ง', cellsrenderer, datafield: 'price', align: 'center', cellsalign: 'center',width: '20%' },
        { text: 'sup_id',hidden:true, datafield: 'sup_id', align: 'center', cellsalign: 'center',width: '5%' },
        { text: 'size_id',hidden:true, datafield: 'size_id', align: 'center', cellsalign: 'center',width: '5%' },
      
    ]
    const source: any = {
      datafields: [
          { name: 'transportname', type: 'string' },
          { name: 'label', type: 'string' },
          { name: 'volume', type: 'number' },
          { name: 'price', type: 'number' },
          { name: 'sup_id', type: 'number' },
          { name: 'size_id', type: 'number' },
      ],
      datatype: 'array',
      localdata:this.props.Gridbox,
      updaterow: (rowid: any, rowdata: any, commit: any): void => {
        this.props.onUpdatedata(rowdata);
        commit(true);
    }
  };
      return (
        <JqxGrid
              ref={this.myGrid}
              theme="metrodark"
              width={'90%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={false}
              editmode ={'click'}
              enabletooltips={true} editable={true} selectionmode={'multiplecellsadvanced'}
        />
      );
  }
}
export default Datagridbox;