import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import { TextInput, Button, Icons } from "grommet";
import Select from "react-select";
import { withSnackbar } from "notistack";
import "./form.css";
import LinearProgress from "@material-ui/core/LinearProgress";

import {
  MDBBtn,
  MDBIcon,
  MDBModal,
  MDBModalBody,
  MDBModalHeader,
  MDBModalFooter
} from "mdbreact";
//API connect
import ApiService from "../actions/apidata";
import Datagridbox from "./datagridbox.tsx";
import { array } from "prop-types";

class formPage2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      completed: 0,
      buffer: 10,
      boxNow: [],
      boxNowval: [],
      boxarray: "a1",
      weight: "",
      volume: "",
      volumepop: "",
      price: "",
      selectedValue: "",
      wide: "",
      long: "",
      high: "",
      modal: false,
      description: "",
      Gridbox: []
    };
    this.clearvolume = this.clearvolume.bind(this);
    this.onUpdatedata = this.onUpdatedata.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.setBoxnow = this.setBoxnow.bind(this);
    this.ApiCall = new ApiService();
  }
  componentDidMount() {
    this.ApiCall.DDl_GetDetailBox()
      .then(res => {
        if (res.status === true) {
          this.setState({ boxNow: res.data });
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  prox() {
    this.ApiCall.DDl_GetDetailBox()
      .then(res => {
        if (res.status === true) {
          this.setState({ boxNow: res.data }, () => {});
          this.setState(
            { boxNowval: this.state.boxNow[this.state.boxNow.length - 1] },
            () => {}
          );
          console.log(this.state.boxNowval);
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
    this.clearvolume();
  };
  cleardata() {
    this.setState({
      weight: "",
      volume: "",
      boxNowval: ""
    });
  }
  clearvolume() {
    this.setState({
      description: "",
      volumepop: "",
      long: "",
      high: "",
      wide: ""
    });
  }
  clearvolumepop() {
    this.setState({
      boxNow: [],
      volumepop: "",
      price: ""
    });
  }
  processvolume() {
    this.setState(
      {
        volumepop: this.state.wide * this.state.long * this.state.high
      },
      () => {}
    );
  }
  processweight() {
    this.setState(
      {
        weight: this.state.volume / 5000
      },
      () => {}
    );
  }
  handleChange() {
    this.prox();

    console.log(this.state.boxNowval);
  }
  setBoxnow() {
    if (
      this.state.description === "" ||
      this.state.wide === "" ||
      this.state.long === "" ||
      this.state.high === ""
    ) {
      this.props.enqueueSnackbar(
        "Warning กรุณา ระบุใส่ข้อมูลให้ถูกต้อง ครบถ้วน??",
        {
          variant: "warning"
        }
      );
    } else {
      let boxarray = Array();
      boxarray = {
        type: 1,
        description: this.state.description,
        wide: this.state.wide,
        long: this.state.long,
        high: this.state.high,
        volume: this.state.volumepop
      };

      this.ApiCall.SetBox(boxarray)
        .then(res => {
          if (res.status === true) {
            this.props.enqueueSnackbar("บันทึกข้อมูล สำเร็จแล้ว", {
              variant: "success"
            });
            this.toggle();

            this.handleChange();
            this.clearvolume();

            this.processweight();
          } else if (res.status === false) {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการให้สำเร็จได้ หรือ มีรายการนี้อยู่แล้ว ลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          } else {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          }
        })

        .catch(error => {
          console.error(error.message);
        });
    }
    this.setState({ volume: this.state.volumepop }, () => {});
  }

  onUpdatedata(rowdata) {
    let dataupdate = Array();
    dataupdate = {
      sup_id: rowdata.sup_id,
      size_id: rowdata.size_id,
      type: 1,
      price: rowdata.price
    };
    this.ApiCall.UpdatePriceBoxsize(dataupdate)
      .then(res => {
        if (res.status === true) {
          this.props.enqueueSnackbar("อัพเดทข้อมูล สำเร็จแล้ว", {
            variant: "success"
          });
          this.props.getGridbox();
          this.clearvolume();
        } else if (res.status === false) {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
        } else {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  setPricenow() {
    if (typeof this.props.supcar.value === "undefined") {
      this.props.enqueueSnackbar("Warning กรุณา เลือกบริษัทขนส่ง..", {
        variant: "warning"
      });
    } else {
      if (
        this.state.boxNowval.length === 0 ||
        this.state.volume === "" ||
        this.state.price === ""
      ) {
        this.setState({ databranch: [] }, () => {
          this.props.enqueueSnackbar(
            "Warning คุณใส่ข้อมูลไม่ถูกต้อง ลองใหม่อีกครั้ง",
            {
              variant: "warning"
            }
          );
        });
      } else {
        let dataupdate = Array();
        dataupdate = {
          sup_id: this.props.supcar.value,
          size_id: this.state.boxNowval.value,
          type: 1,
          price: this.state.price
        };
        this.ApiCall.UpdatePriceBoxsize(dataupdate)
          .then(res => {
            if (res.status === true) {
              this.props.enqueueSnackbar("บันทึกข้อมูล สำเร็จแล้ว", {
                variant: "success"
              });
              this.props.getGridbox();
              this.clearvolume();
            } else if (res.status === false) {
              this.props.enqueueSnackbar(
                "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
                {
                  variant: "error"
                }
              );
            } else {
              this.props.enqueueSnackbar(
                "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
                {
                  variant: "error"
                }
              );
            }
          })
          .catch(error => {
            console.error(error.message);
          });
      }
    }
  }

  render() {
    const { completed } = this.state;
    return (
      <div style={{ fontFamily: "Prompt" }}>
        <Grid container spacing={24}>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 0 }}
          />
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <h4 style={{ color: "green" }}>
              <br />
              <MDBIcon icon="box-open" size="2x" className="red-text pr-3" />
              &nbsp;บันทึกข้อมูลราคาขนส่ง (แบบวัดขนาดกล่อง){" "}
              <i style={{ color: "#1200ff" }}>{this.props.supcar.label}</i>
            </h4>
          </Grid>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <LinearProgress
              color="secondary"
              variant="determinate"
              value={completed}
            />
            <hr />
          </Grid>
          <Grid item lg={6} xl={6} xs={6} sm={6} md={6} style={{ padding: 3 }}>
            <Grid container spacing={24}>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >
                <h5
                  style={{
                    color: "#8089a9",
                    textAlign: "left",
                    marginLeft: 10
                  }}
                >
                  เพิ่มราคากล่อง{" "}
                  <i style={{ color: "#596eb9" }}>{this.state.code_route}</i>
                </h5>
              </Grid>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3, marginTop: 10 }}
              />
              <Grid
                item
                lg={3}
                xl={3}
                xs={3}
                sm={3}
                md={3}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}> ประเภทกล่อง</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              >
                <div className="select-up" style={{ textAlign: "left" }}>
                  <Select
                    value={this.state.boxNowval}
                    ref="selectValue"
                    placeholder="--เลือกกล่อง--"
                    size="xsmall"
                    style={{ width: 300, padding: 5 }}
                    options={this.state.boxNow}
                    onChange={event => {
                      this.setState(
                        { boxNowval: event, volume: event.volume },
                        () => {
                          this.price.focus();
                          this.processweight();
                        }
                      );
                    }}
                  />
                </div>
              </Grid>
              <Grid
                item
                lg={1}
                xl={1}
                xs={1}
                sm={1}
                md={1}
                style={{ padding: 3 }}
              >
                {" "}
                <Button
                  label="เพิ่ม"
                  onClick={() => {
                    this.toggle();
                  }}
                />
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              />

              <Grid
                item
                lg={3}
                xl={3}
                xs={3}
                sm={3}
                md={3}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}> น้ำหนักกล่อง</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={3}
                xl={3}
                xs={3}
                sm={3}
                md={3}
                style={{ padding: 3 }}
              >
                <TextInput
                  disabled={true}
                  style={{
                    width: "100%",
                    color: "#3f51b5",
                    fontSize: 14,
                    opacity: 10
                  }}
                  size="xsmall"
                  value={this.state.weight}
                  // placeholder="exp A1"
                  ref={input => {
                    this.description = input;
                  }}
                />
              </Grid>
              <Grid
                item
                lg={1}
                xl={1}
                xs={1}
                sm={1}
                md={1}
                style={{ padding: 0 }}
              >
                <h6 style={{ textAlign: "left", marginTop: 10 }}>
                  <strong style={{ fontSize: 16, color: "#3f51b5" }}>
                    {" "}
                    กิโล
                  </strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              />

              <Grid
                item
                lg={3}
                xl={3}
                xs={3}
                sm={3}
                md={3}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}> ปริมาตรกล่อง</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={3}
                xl={3}
                xs={3}
                sm={3}
                md={3}
                style={{ padding: 3 }}
              >
                <TextInput
                  disabled={true}
                  style={{
                    width: "100%",
                    color: "#3f51b5",
                    fontSize: 14,
                    opacity: 10
                  }}
                  size="xsmall"
                  value={this.state.volume}
                  // placeholder="exp A1"
                  ref={input => {
                    this.description = input;
                  }}
                />
              </Grid>
              <Grid
                item
                lg={1}
                xl={1}
                xs={1}
                sm={1}
                md={1}
                style={{ padding: 0 }}
              >
                <h6 style={{ textAlign: "left", marginTop: 10 }}>
                  <strong style={{ fontSize: 16, color: "#3f51b5" }}>
                    {" "}
                    cm<sup>3</sup>
                  </strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={3}
                xl={3}
                xs={3}
                sm={3}
                md={3}
                style={{ padding: 0 }}
              />
              <Grid
                item
                lg={3}
                xl={3}
                xs={3}
                sm={3}
                md={3}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}>
                    {" "}
                    ราคาค่าขนส่ง ( บาท ){" "}
                  </strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={5}
                xl={5}
                xs={5}
                sm={5}
                md={5}
                style={{ padding: 3 }}
              >
                <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.price}
                  // placeholder="exp A1"
                  ref={input => {
                    this.price = input;
                  }}
                  onChange={event =>
                    this.setState({ price: event.target.value }, () => {})
                  }
                />
              </Grid>
              <Grid
                item
                lg={5}
                xl={5}
                xs={5}
                sm={5}
                md={5}
                style={{ padding: 3 }}
              />
              <Grid
                item
                lg={1}
                xl={1}
                xs={1}
                sm={1}
                md={1}
                style={{ padding: 0 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 0 }}
              />
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 10 }}
              >
                <MDBBtn
                  color="primary"
                  onClick={() => {
                    this.cleardata();
                  }}
                >
                  <MDBIcon icon="undo" className="mr-1" /> ล้างข้อมูล
                </MDBBtn>
                <MDBBtn
                  color="default"
                  onClick={() => {
                    this.setPricenow();
                  }}
                >
                  บันทึกราคากล่อง <MDBIcon icon="save" className="ml-1" />
                </MDBBtn>
              </Grid>
            </Grid>
          </Grid>
          <Grid item lg={6} xl={6} xs={6} sm={6} md={6} style={{ padding: 3 }}>
            <Grid container spacing={24}>
              <Grid
                item
                lg={12}
                xl={12}
                xs={12}
                sm={12}
                md={12}
                style={{ padding: 3 }}
              >
                <Grid
                  item
                  lg={12}
                  xl={12}
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 3 }}
                >
                  <h5
                    style={{
                      color: "#8089a9",
                      textAlign: "left",
                      marginLeft: 10
                    }}
                  >
                    อัพเดทราคากล่อง{" "}
                    <i style={{ color: "#596eb9" }}>{this.state.code_route}</i>
                  </h5>
                </Grid>
                <Grid item lg={12} xl={12} xs={12} sm={12} md={12}>
                  <Datagridbox
                    Gridbox={this.props.Gridbox}
                    onUpdatedata={this.onUpdatedata}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <MDBModal isOpen={this.state.modal} toggle={this.toggle}>
          <MDBModalHeader toggle={this.toggle}>
            <strong style={{ fontSize: 30, color: "#3f51b5" }}>
              {" "}
              เพิ่มประเภทกล่องใหม่
            </strong>
          </MDBModalHeader>
          <MDBModalBody>
            <Grid container spacing={24}>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "center", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}> ประเภทกล่อง</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              >
                <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.description}
                  // placeholder="exp A1"
                  ref={input => {
                    this.description = input;
                  }}
                  onChange={event =>
                    this.setState({ description: event.target.value }, () => {})
                  }
                />
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              />
              <Grid
                item
                lg={3}
                xl={3}
                xs={3}
                sm={3}
                md={3}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "center", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}> ขนาด</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={2}
                xl={2}
                xs={2}
                sm={2}
                md={2}
                style={{ padding: 3 }}
              >
                <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.wide}
                  placeholder="กว้าง"
                  ref={input => {
                    this.wide = input;
                  }}
                  onChange={event =>
                    this.setState({ wide: event.target.value }, () => {
                      this.processvolume();
                    })
                  }
                />
              </Grid>
              <Grid
                item
                lg={2}
                xl={2}
                xs={2}
                sm={2}
                md={2}
                style={{ padding: 3 }}
              >
                <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.long}
                  placeholder="ยาว"
                  ref={input => {
                    this.long = input;
                  }}
                  onChange={event =>
                    this.setState({ long: event.target.value }, () => {
                      this.processvolume();
                    })
                  }
                />
              </Grid>
              <Grid
                item
                lg={2}
                xl={2}
                xs={2}
                sm={2}
                md={2}
                style={{ padding: 3 }}
              >
                <TextInput
                  style={{ width: "100%", fontSize: 14 }}
                  size="xsmall"
                  value={this.state.high}
                  placeholder="สูง"
                  ref={input => {
                    this.high = input;
                  }}
                  onChange={event =>
                    this.setState({ high: event.target.value }, () => {
                      this.processvolume();
                    })
                  }
                />
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3, marginTop: 10 }}
              >
                <h6 style={{ textAlign: "center", marginLeft: 0 }}>
                  <strong style={{ fontSize: 16 }}> ปริมาตรกล่อง</strong>
                </h6>
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              >
                <TextInput
                  disabled={true}
                  style={{
                    width: "100%",
                    color: "#3f51b5",
                    fontSize: 14,
                    opacity: 10
                  }}
                  size="xsmall"
                  value={this.state.volumepop}
                  // placeholder="exp A1"
                  ref={input => {
                    this.description = input;
                  }}
                />
              </Grid>
              <Grid
                item
                lg={4}
                xl={4}
                xs={4}
                sm={4}
                md={4}
                style={{ padding: 3 }}
              />
            </Grid>
          </MDBModalBody>
          <MDBModalFooter>
            <Button
              label="ปิด"
              onClick={() => {
                this.toggle();
              }}
            />
            <Button
              label="บันทึก"
              onClick={() => {
                this.setBoxnow();
              }}
            />
          </MDBModalFooter>
        </MDBModal>
      </div>
    );
  }
}

export default withSnackbar(formPage2);
