import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class Datagrid extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();
  constructor(props: any) {
      super(props);
  }
 
  
  public render() {
    const cellsrenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
        if (value === 0) {
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:red;"><strong>'+value+'</strong></div>';
        }
        else {
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:green;"><strong>'+value+'</strong></div>';
        }
    };
    const columns: any =[
        { text: 'จำนวนส่ง(สาขา)', pinned: true, editable: false,  datafield: 'rank', cellsalign: 'center',align: 'center', width: '40%' },
        { text: 'SET ค่าขนส่ง (เหมาคัน฿)', cellsrenderer, datafield: 'setPriceCar', align: 'center', cellsalign: 'center',width: '30%' },
        { text: 'SET ค่าขนส่ง (เหมากล่อง฿)', cellsrenderer, datafield: 'setPriceBox', align: 'center', cellsalign: 'center',width: '30%' }
    ]
    const source: any = {
      datafields: [
          { name: 'rank', type: 'int' },
          { name: 'setPriceCar', type: 'number' },
          { name: 'setPriceBox', type: 'number' },
      ],
      datatype: 'array',
      localdata:this.props.databranch,
      updaterow: (rowid: any, rowdata: any, commit: any): void => {
        this.props.onUpdatedata(rowdata);
        commit(true);
    }
  };
      return (
        <JqxGrid
              ref={this.myGrid}
              theme="metrodark"
              width={'90%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={false}
              editmode ={'click'}
              enabletooltips={true} editable={true} selectionmode={'multiplecellsadvanced'}
        />
      );
  }
}
export default Datagrid;