import React, { Component } from "react";
import { MDBRow, MDBCol, MDBCard, MDBCardBody } from "mdbreact";
import Grid from "@material-ui/core/Grid";
import Select from "react-select";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import { SnackbarProvider } from "notistack";
import Form1 from "./formPage1";
import Form2 from "./formPage2";
import Form3 from "./formPage3";
import ApiService from "../actions/apidata";
import AuthService from "../authlogin/AuthService";
function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}
TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

export default class Priceconfig extends Component {
  constructor(props) {
    super(props);
    this.state = {
      jobdoc: "",
      pricetype: { value: "0", label: "แบบเหมารถ" },
      value: 1,
      supcar: [],
      transportdata: [],
      Routetitle: [],
      Gridbox:[],
      Gridweight:[]
    };
    this.getRoutetitle = this.getRoutetitle.bind(this);
    this.getGridbox = this.getGridbox.bind(this);
    this.getGridWeight = this.getGridWeight.bind(this);
    this.Auth = new AuthService();
    this.ApiCall = new ApiService();
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  getGridbox() {
    if (typeof this.state.supcar.value === "undefined") {
      this.props.enqueueSnackbar("Warning คุณยังไม่ได้เลือกบริษัทขนส่ง..", {
        variant: "warning"
      });
    } else {
      this.ApiCall.ReciveBoxDetail(this.state.supcar.value)
        .then(res => {
          if (res.status === true) {
            this.setState({ Gridbox: res.data });
          } else {
            console.log(res.message);
          }
        })
        .catch(error => {
          console.error(error.message);
        });
    }
  }

  getGridWeight() {
    if (typeof this.state.supcar.value === "undefined") {
      this.props.enqueueSnackbar("Warning คุณยังไม่ได้เลือกบริษัทขนส่ง..", {
        variant: "warning"
      });
    } else {
      this.ApiCall.RecivegetGridWeight(this.state.supcar.value)
        .then(res => {
          if (res.status === true) {
            this.setState({ Gridweight: res.data });
          } else {
            console.log(res.message);
          }
        })
        .catch(error => {
          console.error(error.message);
        });
    }
  }


  componentDidMount() {
    if (!this.Auth.loggedIn()) {
      window.location.href = "/Login";
    }
    this.ApiCall.gettransport()
      .then(res => {
        if (res.status === true) {
          this.setState({ transportdata: res.data });
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  getRoutetitle() {
    this.ApiCall.getroute(this.state.supcar.value)
      .then(res => {
        if (res.status === true) {
          this.setState({ Routetitle: res.data }, () => {});
        } else if (res.status === false) {
          this.setState({ Routetitle: [] }, () => {});
          console.log(res.message);
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  render() {
    const { classes } = this.props;
    const { value } = this.state;
    return (
      <MDBRow>
        <MDBCol md="12">
          <MDBCard>
            <MDBCardBody style={{ fontFamily: "Prompt" }}>
              <Grid container spacing={24}>
                <Grid
                  item
                  lg={2}
                  xl={2}
                  xs={2}
                  sm={2}
                  md={2}
                  style={{ marginTop: 15, marginBottom: 30 }}
                >
                  <h4 style={{ textAlign: "right" }}>
                    <strong style={{ fontSize: 24 }}>ระบุบริษัทขนส่ง</strong>
                  </h4>
                </Grid>
                <Grid
                  item
                  lg={2}
                  xl={2}
                  xs={2}
                  sm={2}
                  md={2}
                  style={{ marginTop: 10, marginBottom: 20 }}
                >
                  <div className="select-up" style={{ textAlign: "left" }}>
                    <Select
                      value={this.state.supcar}
                      placeholder="-- เลือกบริษัทขนส่ง --"
                      size="xsmall"
                      style={{ width: 300, padding: 5 }}
                      options={this.state.transportdata}
                      onChange={event => {
                        this.setState({ supcar: event,chk:1 }, () => {
                          this.getRoutetitle();
                          this.getGridbox();
                          this.getGridWeight();
                        });
                      }}
                    />
                  </div>
                </Grid>
                <Grid
                  item
                  lg={8}
                  xl={8}
                  xs={8}
                  sm={8}
                  md={8}
                  style={{ padding: 3 }}
                />
              </Grid>
              <AppBar position="static">
                <Tabs value={value} onChange={this.handleChange}>
                  <Tab
                    style={{
                      fontSize: 16,
                      fontFamily: "Prompt",
                      background: "#040040"
                    }}
                    label="แบบเหมา คันและกล่อง"
                  />
                  <Tab
                    style={{
                      fontSize: 16,
                      fontFamily: "Prompt",
                      background: "#040040"
                    }}
                    label="ตั้งค่าขนาดกล่อง"
                  />
                  <Tab
                    style={{
                      fontSize: 16,
                      fontFamily: "Prompt",
                      background: "#040040"
                    }}
                    label="ตั้งค่าน้ำหนัก"
                  />

                  <Tab
                    style={{
                      fontSize: 16,
                      fontFamily: "Prompt",
                      background: "#040040"
                    }}
                    label="แบบ เจ.ไอ.บี"
                  />
                </Tabs>
              </AppBar>
              {value === 0 && (
                <TabContainer>
                  <SnackbarProvider
                    maxSnack={3}
                    anchorOrigin={{
                      vertical: "top",
                      horizontal: "right"
                    }}
                  >
                    <Form1
                      supcar={this.state.supcar}
                      getRoutetitle={this.getRoutetitle}
                      Routetitle={this.state.Routetitle}
                    />
                  </SnackbarProvider>
                </TabContainer>
              )}
              {value === 1 && (
                <TabContainer>
                  <SnackbarProvider
                    maxSnack={3}
                    anchorOrigin={{
                      vertical: "top",
                      horizontal: "right"
                    }}
                  >
                    <Form2 supcar={this.state.supcar} Gridbox={this.state.Gridbox } getGridbox={this.getGridbox} />
                  </SnackbarProvider>
                </TabContainer>
              )}
              {value === 2 && (
                <TabContainer>
                  <SnackbarProvider
                    maxSnack={3}
                    anchorOrigin={{
                      vertical: "top",
                      horizontal: "right"
                    }}
                  >
                    <Form3 supcar={this.state.supcar} Gridweight={this.state.Gridweight } getGridWeight={this.getGridWeight} />
                  </SnackbarProvider>
                </TabContainer>
              )}
              {/* </Grid> */}
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    );
  }
}
