import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import { TextInput } from "grommet";
import { withSnackbar } from "notistack";
import "./form.css";
import { MDBIcon } from "mdbreact";
import LinearProgress from "@material-ui/core/LinearProgress";
import DataGridSet from "./datagrid.tsx";
import DataGridShow from "./datagridshow.tsx";
import DataGridTitle from "./datagridtitle.tsx";
//API connect
import ApiService from "../actions/apidata";
class formPage1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code_route: "",
      completed: 0,
      buffer: 10,
      databranch: [],
      carname: []
    };
    this.ApiCall = new ApiService();
    this.SearchData = this.SearchData.bind(this);
    this.onUpdatedata = this.onUpdatedata.bind(this);
    this.setCode_Route = this.setCode_Route.bind(this);
  }
  progress = () => {
    const { completed } = this.state;
    if (completed === 100) {
      this.setState({ completed: 0 });
    } else {
      const diff = Math.random() * 10;
      this.setState({ completed: Math.min(completed + diff, 100) });
    }
  };
  SearchData = () => {
    let varSearch = Array();
    if (typeof this.props.supcar.value === "undefined") {
      this.props.enqueueSnackbar(
        "Warning กรุณา ระบุบริษัทขนส่งก่อนเลือกเส้นทาง",
        {
          variant: "warning"
        }
      );
    } else {
      varSearch = {
        carname: this.props.supcar.value,
        code_route: this.state.code_route
      };
      this.ApiCall.getbranch(varSearch)
        .then(res => {
          if (res.status === true) {
            this.setState({ databranch: res.data }, () => {});
          } else if (res.status === false) {
            this.setState({ databranch: [] }, () => {
              this.props.enqueueSnackbar("Warning ไม่พบข้อมูลเส้นทางนี้", {
                variant: "warning"
              });
            });
          } else {
            this.props.enqueueSnackbar(
              "มีปัญหาเกิดขึ้น ระหว่างค้นหาข้อมูล ลองใหม่อีกครั้ง..",
              {
                variant: "warning"
              }
            );
          }
        })
        .catch(error => {
          console.error(error.message);
        });
    }
  };

  onUpdatedata(rowsdata) {
    let dataupdate = Array();
    dataupdate = {
      sup_id: this.props.supcar.value,
      code_route: this.state.code_route,
      rank: rowsdata.rank,
      setPriceBox: rowsdata.setPriceBox,
      setPriceCar: rowsdata.setPriceCar
    };
    this.ApiCall.UpdatePrice(dataupdate)
      .then(res => {
        if (res.status === true) {
          this.props.enqueueSnackbar("บันทึกข้อมูล สำเร็จแล้ว", {
            variant: "success"
          });
          this.props.getRoutetitle();
        } else if (res.status === false) {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
        } else {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการให้สำเร็จได้ ลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  setCode_Route(code_id) {
    this.setState(
      {
        code_route: code_id
      },
      () => {
        this.SearchData();
      }
    );
  }
  render() {
    const { completed } = this.state;
    return (
      <div style={{ fontFamily: "Prompt" }}>
        <Grid container spacing={24}>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 0 }}
          />
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <h4 style={{ color: "green" }}>
              <br />
              <MDBIcon icon="baby-carriage" size="2x" className="red-text pr-3"/>&nbsp;บันทึกข้อมูลราคาขนส่ง (แบบเหมา){" "}
              <i style={{ color: "#1200ff" }}>{this.props.supcar.label}</i>
            </h4>
          </Grid>
          <Grid
            item
            lg={10}
            xl={10}
            xs={10}
            sm={10}
            md={10}
            style={{ padding: 3 }}
          >
            <br />
            <h5 style={{ color: "#8089a9", textAlign: "left", marginLeft: 10 }}>
              เส้นทางที่เลือก{" "}
              <i style={{ color: "#596eb9" }}>{this.state.code_route}</i>
            </h5>
          </Grid>
          <Grid item lg={2} xl={2} xs={2} sm={2} md={2} style={{ padding: 3 }}>
            <h6 style={{ textAlign: "right" }}>
              <strong style={{ fontSize: 16 }}>ค้นหา ด้วยรหัสเส้นทาง</strong>
            </h6>
            <TextInput
              style={{ width: "100%", fontSize: 14 }}
              size="xsmall"
              value={this.state.code_route}
              placeholder="exp 031"
              ref={input => {
                this.code_route = input;
              }}
              onChange={event =>
                this.setState({ code_route: event.target.value }, () => {})
              }
              onKeyPress={event => {
                if (event.key === "Enter") {
                  this.SearchData();
                }
              }}
            />
          </Grid>
          <Grid
            item
            lg={12}
            xl={12}
            xs={12}
            sm={12}
            md={12}
            style={{ padding: 3 }}
          >
            <LinearProgress
              color="secondary"
              variant="determinate"
              value={completed}
            />
            <hr />
          </Grid>
          <Grid item lg={3} xl={3} xs={3} sm={3} md={3} style={{ padding: 0 }}>
            <center>
              <DataGridTitle
                databranch={this.props.Routetitle}
                setCode_Route={this.setCode_Route}
              />
            </center>
          </Grid>
          <Grid item lg={5} xl={5} xs={5} sm={5} md={5} style={{ padding: 0 }}>
            <center>
              <DataGridSet
                databranch={this.state.databranch}
                onUpdatedata={this.onUpdatedata}
              />
            </center>
          </Grid>
          <Grid item lg={4} xl={4} xs={4} sm={4} md={4} style={{ padding: 0 }}>
            <center>
              <DataGridShow databranch={this.state.databranch} />
            </center>
          </Grid>
        </Grid>
      </div>
    );
  }
}
export default withSnackbar(formPage1);
