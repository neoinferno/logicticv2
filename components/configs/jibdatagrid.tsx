import React, { Component } from "react";
import "jqwidgets-scripts/jqwidgets/styles/jqx.base.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.material-purple.css";
import "jqwidgets-scripts/jqwidgets/styles/jqx.metrodark.css";

import JqxGrid, {
  IGridProps,
  jqx
} from "jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid";
class JibDatagrid extends React.PureComponent<any, IGridProps> {
  private myGrid = React.createRef<JqxGrid>();
  constructor(props: any) {
      super(props);
  }
 
  public render() {
    const cellsrenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
        if (value === 0) {
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:red;"><strong>'+value+'</strong></div>';
        }
        else {
            return '<div style="overflow:hidden;text-overflow:ellipsis;padding-bottom:2px;text-align:center;margin-right:2px;margin-left:4px;margin-top:4px;color:green;"><strong>'+value+'</strong></div>';
        }
    };
    const columns: any =[
        { text: 'รหัสเส้นทาง', pinned: true, editable: false,  datafield: 'route', cellsalign: 'center',align: 'center', width: '10%' },
        { text: 'เงินเดือนคนขับ', cellsrenderer, datafield: 'm_money', align: 'center', cellsalign: 'center',width: '20%' },
        { text: 'ค่าน้ำมัน', cellsrenderer, datafield: 'oil', align: 'center', cellsalign: 'center',width: '20%' },
        { text: 'ค่าเทียว', cellsrenderer, datafield: 'r_money', align: 'center', cellsalign: 'center',width: '20%' },
        { text: 'เด็กติดรถ/คน', cellsrenderer, datafield: 'money_child', align: 'center', cellsalign: 'center',width: '20%' },
        { text: 'ค่าทางด่วน', cellsrenderer, datafield: 'expressway', align: 'center', cellsalign: 'center',width: '10%' },
    ]
    const source: any = {
      datafields: [
          { name: 'route', type: 'string' },
          { name: 'm_money', type: 'number' },
          { name: 'oil', type: 'number' },
          { name: 'r_money', type: 'number' },
          { name: 'money_child', type: 'number' },
          { name: 'expressway', type: 'number' },
      ],
      datatype: 'array',
      localdata:this.props.databranch,
      updaterow: (rowid: any, rowdata: any, commit: any): void => {
        this.props.onUpdatedata(rowdata);
        commit(true);
    }
  };
      return (
        <JqxGrid
              ref={this.myGrid}
              theme="metrodark"
              width={'90%'} source={new jqx.dataAdapter(source)} columns={columns}
              pageable={false} autoheight={false} sortable={false} altrows={false}
              editmode ={'click'}
              enabletooltips={true} editable={true} selectionmode={'multiplecellsadvanced'}
        />
      );
  }
}
export default JibDatagrid;