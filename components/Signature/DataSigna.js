import React, { Component } from "react";
import {
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBCardFooter,
  MDBContainer,
  MDBView
} from "mdbreact";
import { withSnackbar } from "notistack";
import { Box, RadioButton } from "grommet";
import Grid from "@material-ui/core/Grid";
import { MDBBtn, MDBIcon, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBBtnGroup, MDBDropdown, MDBDropdownToggle, MDBDropdownMenu, MDBDropdownItem } from "mdbreact";
import LinearProgress from "@material-ui/core/LinearProgress";
import LoadingOverlay from "react-loading-overlay";
import { TextInput } from "grommet";
// import Datagridhead from "./datagridhead.tsx";
import Select from "react-select";
// import Datagrid from "./datagrid.tsx";
import ApiService from "../actions/apidata";
import AuthService from "../authlogin/AuthService";
import noimg from "./noimg.jpg"
import jiblogo from "../img/logo-logistics3.png";
import Datagrid from "./datagrid.tsx";
import Datetime1 from "./datetime1.tsx";
import Datetime2 from "./datetime2.tsx";


let DecodeImg = 'data:image/png;base64,'
let Img = DecodeImg + 'iVBORw0KGgoAAAANSUhEUgAAAfQAAADOCAIAAABHImeiAAAAA3NCSVQFBgUzC42AAAAOfklEQVR4nO3dYWgcZR7H8ZkjhVlIYQcsZKGFbmjBDRbcUKEb9IWBvugGD0xQuAQLd62CJgqaeqCJvvDs3YuLvhB7vrDxhZIWTpJCIRE82nuhZAstiWDpFizZQgtZsLADCcxAAnMv1iu12Z2d3czMM88z3w99UXYnM3+YnV/+eXbmeXTXdTUAgFr+ILoAAEDwCHcAUBDhDgAKItwBQEGEOwAoiHAHAAUR7gCgIMIdABREuAOAggh3AFAQ4Q7pjYyO6L9nWZboogDBdOaWgbyq96uZA5mGbw0+P3jl6pWI6wHig3CHrCzLMk3TYwPyHUlGuENWuq633Kb2ay39RDqCYoC4YcwdUqrcrfjZzNzn1doDCiPcIaWhF4ZElwDEGsMykJKfMZk6PuFIJjp3AFAQ4Q4ACiLcAUBBhDukxA2OgDfCHVKa/WxWdAlArBHukBKdO+CNcIeUstms6BKAWCPcIaXsIV/hvrK6EnYlQDzxEBNk1Xugt3K/xSQEfLyRWHTukNXavTXvDRaXFqOpBIghOndIzDRNj3U5+GwjyejcIbG5C3OiSwBiis4dcvOYQYzPNpKMzh0AFES4Q03nvzovugRAJMIdauo73Ce6BEAkwh2KMkQXAAhFuENR26ILAIQi3KEmZhZDwnErJOTW7FZIPthIODp3AFAQ4Q4ACiLcAUBBhDsAKIhwh9zG3xwXXQIQR4Q75DZQGBBdAhBHhDvkVniuILoEII4IdwBQEOEOuZndpugSgDjiCVVIr+FDqnywkXB07pCbs+2ILgGII8Idclu4uCC6BCCOCHfIzXZs0SUAccSYO+Rm7jWtTWvn63ywkXB07pBbw2TXNK1ypxJxJUCsEO5QU6VCuCPRCHeoKZPNiC4BEIlwh9ymPphq+HruUC7iSoBY4QtVSE/foz+2HHatVkunWUMViUbnDum5W+6tn2/V/z/++rjruiQ7QOcOAAqicwcABRHuAKAgwh0AFES4A4CCCHcAUBDhDgAKItwBQEGEOxAjltV4kkugXV2iCwCSpXq/On9pvnKvkj2QHX9zvP7i6k+r/fn+h9sYhmHbLEKCXeEJVSAiF765MHZyzP/2c1/Pjb4yGl49UBvhDoRu6buloeJQBz9odBu1X2uGYQReEpTHmDsQorGTY7qud5bsmqY5m04qlRooDARbFZKAzh0I2Jl3zlTuVhYuLQS7W9u2aeHhH+EOBKP/SP/qzdVQD8HVCv8Id2C3MplMtVqN4EDcRQP/GHMHOtd7uFfX9WiSXdM0x3GiORAUQLgDHUrtTVXuVNr9qcm3J91H1DZq9oZdq9V8/jj5Dp94iAnoxMAzA85mezm7fm+9Z3/PYy+mu9Oaphma4bquruut98IlC3/o3IG26bpeulHyuXF2f7bepO9M9se4rjv80rD3Nut3130eFwlHuANtcLYdX/21phldxsrPK67rrt1b87//+X/Pe29QqbY9EIRkItyBNqT2pFpus3h50XVde8vOP5WPoCSgIQbwAL/MfWbLbcK+tzjV1fq3C6DRuQP+WQ9azMdbOFoIu4bs/mzYh4AaeIhJYtUH1ZXrK9q2pmmaZVm2Y/cd6SscCz1fkqk321u56zXebXQZ9lYATxh5j+lzwcInwl0229r0P6bPfnjWYxOmig1Dy+9Rg7qUFr5dGHl5JOyjQHmEu0x83qdRx5kNFuEOuTDmLofSj6W2kl1r8zcBWopsRsZ8nntsEADCPdasTcs0TV3XB57rZEZvXddnv5wNvKpkMrpbhHtQM8xkD/GVKQLAsExMOZtOam8wN7319PSsr/NY4261/EK1cKywXFoO5Fgef3VxwcInOvc4Gnl5JKhk1zStWq1m9mWC2ltizXw6472Bd/QHYurdqbAPAWUQ7rFQuVPR9+gPLXwb8CI+1QfVq/+5Guw+k2b4xRazvkQwnDI03OFyfUgghmXEi+ibzy7N3eJc78on//zkzF/PNHs3wGXwmn0kuFrhH527SOWb5ejuadmO6DgKm3x3svBs42fECkcLEdxO08H08Ugswl2Yyt1K35G+Dn4w92SuPoWsvWGv31uv/zv16qnWP0m+79ryD8u1jccX1ph6b2r5ejBfpdYNHh9s+Hr1QURLPkEBDMsI01nP7m65HrO9ee9z/PXxz//1eQcHRUP1RZHCaNhnv5k9ffL0zte5WuEfnbs06t36bubxPPfFueDKgWYYRkhDMWZ36+knAW+Euxila37X8akLrGVjZEYGEdxVCeUR7mK0NXej/2TPH23x5PrYX8b8HxeinHmnwT052YM8uYo2MOYujM8x93ZPkPdu0+l0rfb494GIlWYPJy9eXiy+UIy+HkiKzl2YybcnW24T+K/e3KFcsDtE4LKHG3foJDvaQrgLM/PpTPF408t1+MXhMP6omnhrIvB9IlgNJyCLbE5KKINwF2nx+0XXdR8+F2N0G4PHB9cqa67rzi/Mh3HEmsWYjJTsjQDWeEKisEC2eMs/BPn8i7fMQWYQi7Wl75Yav8GVijbxhapqWIFTag1P38zfZybfa/0NDfAowl01pR9LHit7cLpjrmG4136tpZ9IR18MpEa4q8ZxnFSq6VzwnO6YaxjunDV0gC9UVcNtFfKyHliiS4A6CHfV1GezgoxGRkdElwB1EO6q8ZgdzNe0wBCn4WpZLZf3AxpizF01+h692exg9oZtdDNoE1Pl2+W+XIP5/blC0RnCXTUet0JyruOMpfUQLIZlkmL0T6OiS0BT5dvlhq9PfTAVcSVQBs+9KeXCNxeavsepjiuPP7Y+/ujjKCuBSujc1eFsO2Mnm07XPnR8KMpi4JNHsk+9R9uOzjHmrg4mHpAOpwzhoXNXRO+BXo93WcRHOiFNC4rkoHNXwerN1f4j/R4bcJbjqVnnnn8qv/LzSsTFQDF07tKbeGPCO9ltm6nA48XatJa+W/IYk5m7OBdlPVASnbvcZr+cPf3aaY8NbNtmtplYSaVSPqeIKBwrLJeim+sfiqFzl9jV/171TvbswSzJHivmPtP/5D+layVd18dGm94BBXigc5eY970WWpfmbnFy46XFKWuu/hdY6UYp/1SeX9jwgydbZNUyJkj2uKneb7DytU8N5ujv0uwNxtzQFJ27lFqO23Ja46njzr0ZwzD4whwNMeYun4nXJryT/Vb5VmTFQCzHcQL/hQE10LlLxtl0UnubrqKnadri0mLxRDGyetCWyt1Kb9brcbOOcSHjMXTukvFO9uKJIskeZ9mD2dpGLYw967rem+2dfn86jJ1DRnTuMmGoXT2rN1YHnx+0Nq2egz1Ll5byT+ctyzJNc5e77dnfs35vPZAKISnCXRqZTKZa9brdglOpmMrdSl+ubzeL4tpbttHF7TQJRbhLYOC5gdKPJe9tOI/JYW1a5l6/rT0fjMRizD3udF0n2fGodHfa/xmfeGci1GIQW3TusdbyLjduc06s0rXSQGHAz5Zc48lE5x5fIy+PeG8w/vo4yZ5YhWOF3KGc6CoQX3TucVS+Xe7L9Xlvs1ZZYwkODBQGStcYtUMDdO4xMvvlrK7ruq63SPYuzXVdkh2api2XlltOL3P2b2ejKQaxwsRhseA4jmn6mgyWLgyPsW3b+7uZkB6bQszRuYtUuVPR9+i6rvtcwOHK91ciqAqKWb/P00xJROcuhmmalmW19SO5J3ODxwdDqgcKS3V7TVkBVRHuAnQwjV/+aH7lOismoxPpdFp0CRCAYZmoZTKZDn5q9cbq0B+HAi8GSeC9fjpUxa2QUdvl7Nun/nzq/FfngyoGavCeoIJrPJkI90gFNZ138URxcWlx9/uBGrxn+ecaTybCPVLVB9XMvk6GZRpg/Wv8H+GOnRhzj1TPEz2B7Wtbqz/xlNqTOvP+mcB2C0AJdO5RM/eZ1oP2boL0L/dkjgVUE4jOHTsR7gJEsKKx0WXYW8wpliAeHyqu8WRiWEYA13VbzgeyS862U3/wNdSjIP7GXx8XXQLEINzFsG3bdV17yw71phfHcSL4KwFxxlPNiUW4i2R0GcUTRdef+YX5zo5CvgMJxJi7ZFZ/Wh0oDLS7aLK75TLThNqa/Qqv1WpMP5BMdO6SyT+drw/puK47+fakz7V4Fi4thF0Y4mnu4pzoEiAG4S6xmU9nbv1yqx70o6+Memw5/NJwZFUhViq/VESXADEId0XMfT1XT/mdb4V9Zw7iLHuYFbsSijF3BT06/GoYBotoJ0GzMXcu8MSic1fQbzdZXl50XZdkF2v6w2n9ERcuXgjjKDzQgJ3o3IGwNOymA/9b6twX5ybemGj2Lhd4YtG5A6Fo1k07jtPZgi3NeCQ7X6QnGZ07ELzyzXLfkT6PDYJ68sD7CTWu7iSjcweCN/bKmPcG0x9N7/YQJ8e8k53lXBKOzh0Inp8pHzq+9PpyfeXbZe9tmPwZhDsQvDDC3XEc0zT9zDzBhM/QGJYBwnDq1VMB7q3/mf76BM5+kj17KEuyQyPcgTAUTxRbblO/833ircb3uliW1Xugt77N6o1Vn8ddu7e29staG4VCXQzLAKGIfqZl27aZagIPMQ8sIL8uzd2iS8PvMCwDhCLdHcUs6oZhuK5LsmMnwh0IxcxnM6Huv3CswNxB8MCYOxCWkIbdV66v5I/mw9gzVEK4AyEKMN/zR/Mr11eC2huUx7AMEKLdN0/FF35bQp1kR1sIdyBcrusOHh9s+FbxxG/B7WHxMlPEoBMMywARWbi0UL5ZHnx+sPBsQXQtUB/hDgAKYlgGABREuAOAggh3AFAQ4Q4ACiLcAUBBhDsAKIhwBwAFEe4AoCDCHQAURLgDgIIIdwBQEOEOAAoi3AFAQYQ7ACiIcAcABRHuAKAgwh0AFES4A4CCCHcAUBDhDgAKItwBQEGEOwAoiHAHAAUR7gCgIMIdABREuAOAggh3AFAQ4Q4ACiLcAUBBhDsAKIhwBwAFEe4AoCDCHQAURLgDgIIIdwBQ0P8A4L8rQ/Fs0C4AAAAASUVORK5CYII='

class DataSigna extends Component {
  constructor(props) {
    super(props);
    this.senddata = this.senddata.bind(this);
    this.searchdata = this.searchdata.bind(this);
    this.toggle = this.toggle.bind(this);
    this.toggle2 = this.toggle2.bind(this);
    this.setValueddl = this.setValueddl.bind(this);
    this.setValueddl2 = this.setValueddl2.bind(this);
    this.setValueddl3 = this.setValueddl3.bind(this);
    this.CancelDocjob = this.CancelDocjob.bind(this);
    this.Updatedata = this.Updatedata.bind(this);
    this.getDatenow = this.getDatenow.bind(this);
    this.load_print = this.load_print.bind(this);
    this.driverdata = this.driverdata.bind(this);
    this.fildata = this.fildata.bind(this);
    this.savedata = this.savedata.bind(this);
    this.setDatagrid = this.setDatagrid.bind(this);
    this.setDatagrid2 = this.setDatagrid2.bind(this);
    this.getGriddata = this.getGriddata.bind(this);
    this.carboydata = this.carboydata.bind(this);
    this.showDocByID = this.showDocByID.bind(this);

    this.state = {
      completed: 0,

      typeoption: [],
      driverdatafill: [],
      car_name: "-- เลือกรายชื่อคนขับ --",
      modal: false,
      buffer: 10,
      mdata: [],
      headdata: [],
      dataex: [],
      rowdata: [],
      selected: "",
      dataprint: [],
      transportdata: [],
      carboydata: [],
      transelect: [],
      tran: "-- เลือกรายชื่อบริษัท--",
      tran2: "",
      jobdoclistbody: [],
      jobdoclist: [],
      driverdata: [],
      profile: {},
      doccode: "",
      lastkey: "",
      lastkey2: "",
      personalname: "",
      carname: "",
      carnumber: "",
      car_phone: "",
      typelogis: "",
      supname: "",
      car_name2: "",
      datedoc: "",
      carphone: "",
      dateprint: "",
      datacarname: "",
      datacarboy: "",
      datacarphone: "",
      doc_pack: "",
      doc_number: "",
      datacarnumber: "",
      datatotalload: "",
      dataload: "",
      dccode: "",
      idsup: "",
      dataroute: "",
      datasupname: "",

      searchsup: "",
      searchdoc: "",
      searchroute: "",
      dateselect1: this.getDatenow(),
      dateselect2: this.getDatenow(),

      description: "",
      active: false,
      doccode_: "่-- ว่าง --", //เลขที่เอกสาร
      typelogis_: "่-- ว่าง --", //ขนส่ง
      datedoc_: "่-- ว่าง --", //วันที่เอกสาร
      supname_: "่-- ว่าง --", //บริษัทขนส่ง
      carnumber_: "่-- ว่าง --", //ทะเบียนรถ
      carname_: "่-- ว่าง --", //ชื่อผู้ขับ
      carboy_: "่-- ว่าง --", //เด็กติดรถ
      carrout_: "่-- ว่าง --", //สายรถ
      carphone_: "่-- ว่าง --", //เบอร์โทร
      docstatus_: "่-- ว่าง --", //สถานะ
      logisprice_: "่-- ว่าง --", //ค่าขนส่ง
      branch_: "่-- ว่าง --", //จำนวนสาขา
      box_: "่-- ว่าง --", //จำนวนกล่อง
      listSigna: [],
    };
    this.Auth = new AuthService();
    this.ApiCall = new ApiService();
  }
  carboydata() {
    this.ApiCall.Showdriverlist()
      .then(res => {
        if (res.status === true) {
          this.setState({ carboydata: res.carboy });
          console.log(this.state.carboydata)
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  toggle = (data) => {
    this.setState({
      modal: !this.state.modal,
      dccode: data.doc_code,
      carnumber: data.car_number,
      supname: data.sup_name,
      typelogis: data.type_logis,
      tran: data.sup_name,
      tran2: data.sup_id,
      car_name2: data.carboy,

      car_name: data.car_name,
      car_phone: data.car_phone
    });
    this.fildata()
    this.getGriddata()
    this.carboydata();
    // console.log(this.state.car_name)
  }
  toggle2 = (data) => {
    this.setState({
      modal: !this.state.modal

    });

  }
  getDatenow() {
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + month + "-" + day;
    return today;
  }
  componentDidMount() {
    if (!this.Auth.loggedIn()) {
      window.location.href = "/Login";
    } else {
      let profile = this.Auth.getProfile();
      this.setState({ profile: profile });
      // console.log(profile)
    }
    this.senddata();

    this.ApiCall.DDl_GetSelectType()
      .then(res => {
        if (res.status === true) {
          this.setState({ typeoption: res.data });
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });

    this.ApiCall.gettransport()
      .then(res => {
        if (res.status === true) {
          this.setState({ transportdata: res.data });
          // console.log(this.state.transportdata)
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
    this.driverdata()

  }

  showDocByID() {
    this.setState({ active: true });
    let description = this.state.description
    this.ApiCall.showDocByID(description)
      .then(res => {
        if (res.status === true) {
          this.setState({
            doccode_: res.headdata[0].doc_code,
            typelogis_: res.headdata[0].type_logis,
            // supid_: res.headdata[0].sup_id,
            datedoc_: res.headdata[0].date_doc,
            supname_: res.headdata[0].sup_name,
            carnumber_: res.headdata[0].car_number,
            carname_: res.headdata[0].car_name,
            carboy_: res.headdata[0].carboy,
            carrout_: res.headdata[0].car_rout,
            carphone_: res.headdata[0].car_phone,
            docstatus_: res.headdata[0].doc_status,
            logisprice_: res.headdata[0].logis_price,
            branch_: res.headdata[0].branch,
            box_: res.headdata[0].box,
            listSigna: res.bodydata[0],
          }, () => {
            {
              this.state.carboy_ == "" ?
                (
                  this.setState({ carboy_: "-- ไม่มี --" })
                ) : (
                  this.setState({ carboy_: "-- Error --" })
                )
            }
            this.setState({ active: false });
          });
        } else {
          console.log(res.message);
          console.log("false");
        }
      })
  }
  Updatedata(typedate, rows) {
    if (typedate === 0) {
      this.setState(
        {
          dateselect1: rows
        },
        () => {
          // console.log(this.state.dateselect1);
          // this.Getreportnow();
        }
      );
    } else {
      this.setState(
        {
          dateselect2: rows
        },
        () => {
          // console.log(this.state.dateselect2);
          // console.log(this.state.dateselect1)
          // this.Getreportnow();
        }
      );
    }
  }
  searchdata() {
    this.senddata();
  }
  senddata() {
    var docvar = {
      doc_code: this.state.searchdoc,
      sup_name: this.state.searchsup,
      route: this.state.searchroute,
      dateselect1: this.state.dateselect1,
      dateselect2: this.state.dateselect2
    };
    this.ApiCall.GetdataLogisDoc(docvar)
      .then(res => {
        if (res.status === true) {
          this.setState({
            mdata: res.data,
          }, () => { });
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  clear() {
    this.setState(
      {
        searchdoc: "",
        searchsup: ""
      },
      () => { }
    );
  }
  load_print(data) {
    this.getexcel(data);
    this.setState(
      {
        doccode: data.doc_code,
        carname: data.car_name,
        carphone: data.car_phone,
        datedoc: data.date_doc
      },
      () => {
        this.timeout = setTimeout(() => {
          var content = document.getElementById("printarea");
          var pri = document.getElementById("ifmcontentstoprint").contentWindow;
          pri.document.open();
          pri.document.write(content.innerHTML);
          pri.document.close();

          pri.print();
        }, 500);
      }
    );
  }
  CancelDocjob(jobdoc) {
    // console.log(jobdoc.doc_code)
    this.ApiCall.CancelDocjob(jobdoc.doc_code)
      .then(res => {
        if (res.status === true) {
          // window.location.reload();
          this.props.enqueueSnackbar("ระบบยกเลิกรายการ สำเร็จแล้ว ", {
            variant: "success"
          });
          this.searchdata();
          // this.setTimeout(
          //     window.location.reload(),
          //   100000
          // );
        } else {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  getGriddata() {
    let docvar = Array();
    docvar = {

      doc_code: this.state.dccode
    };

    this.ApiCall.Receivelogisticsdochead(docvar)
      .then(res => {
        if (res.status === true) {
          this.setState({ jobdoclist: res.Head }, () => {
            // this.doc_number.focus();
          });
        } else {
          this.props.enqueueSnackbar(
            "ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });
    var data = this.state.doc_code;


  }
  getexcel(data) {
    var datadoc = data.doc_code;

    this.ApiCall.getPrintregister(datadoc)
      .then(res => {
        if (res.status === true) {
          this.setState(
            {
              headdata: res.headdata,
              rowdata: res.rowdata,
              dataex: res.data,
              dataprint: res.docdata
            },
            () => { }
          );
          this.setState(
            {
              dateprint: this.state.dataprint[0].date_doc,
              datacarname: this.state.dataprint[0].car_name,
              datacarboy: this.state.dataprint[0].carboy,
              datacarphone: this.state.dataprint[0].car_phone,
              datacarnumber: this.state.dataprint[0].car_number,
              dataroute: this.state.dataprint[0].route,
              datacount: this.state.dataex[0].countbox,
              dataload: this.state.rowdata[0].loaddoc,
              datatotalload: this.state.rowdata[0].total,
              datasupname: this.state.dataprint[0].sup_name
            },
            () => { }
          );
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });


  }
  setValueddl(listname) {



    this.setState(
      {

        car_name: listname.target.value,

      }, () => {
        var drive = this.state.car_name;
        let result = this.state.driverdata.filter(function (item) {
          return item.listname == (drive);

        });

        this.setState({ car_phone: result[0].phone });
        console.log(this.state.car_name)
      }

    )

  }
  setValueddl3(listname) {
    this.setState(
      {
        car_name2: listname.target.value
      },
      () => {
        console.log(this.state.car_name2)
      }
    );

  }
  setValueddl2(label) {

    // console.log(label.target.id)

    this.setState(
      {

        tran: label.target.value,
        tran2: label.target.id
        // tranname:label.target.value
        // console.log(this.state.tran)

      }, () => {
        //   var drive =this.state.tran;
        //   let result = this.state.driverdata.filter(function(item) {
        //   return item.listname==(drive);

        // });

        // // this.setState({ car_phone:result[0].phone });
        // console.log(this.state.tran2)
        // console.log(this.state.tran)
      }

    )

  }
  fildata() {

    var drive = this.state.tran;
    let result = this.state.driverdata.filter(function (item) {
      return item.transportname == (drive);

    });
    // console.log(result[0].phone)
    this.setState({ driverdatafill: result });
    // console.log(this.state.driverdatafill)
    // console.log(this.state.transelect[0].value)


  }
  driverdata() {
    this.ApiCall.Showdriverlist()
      .then(res => {
        if (res.status === true) {
          this.setState({ driverdata: res.data });
          //  console.log(this.state.driverdata)
        } else {
          console.log(res.message);
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  savedata() {
    let docvar = Array();
    if (
      this.state.car_name === "-- เลือกรายชื่อบริษัท--" ||
      this.state.car_number === "" ||
      this.state.selected === "" ||
      this.state.car_name === "-- เลือกรายชื่อคนขับ--" ||
      this.state.car_phone === ""
    ) {
      this.props.enqueueSnackbar(
        "โปรดป้อนข้อมูลให้ครบก่อน..ตรวจสอบดูให้แน่ใจ",
        {
          variant: "error"
        }
      );
    } else {
      docvar = {
        doc_code: this.state.dccode,
        sup_id: this.state.tran2,
        sup_name: this.state.tran,
        logis_type: this.state.selected,
        car_number: this.state.carnumber,
        car_name: this.state.car_name,
        userlog: this.state.profile.username,
        carboy: this.state.car_name2,
        car_phone: this.state.car_phone
      };

      console.log(docvar)
      this.ApiCall.UpdateDoc(docvar)
        .then(res => {
          if (res.status === true) {
            this.props.enqueueSnackbar(
              "บันทึกรายการเรียบร้อยแล้วค่ะ ",
              {
                variant: "success"
              }
            );
            this.searchdata()
            this.toggle2()
          } else {
            this.props.enqueueSnackbar(
              "ไม่สามารถทำรายการได้ โปรดลองใหม่อีกครั้ง..",
              {
                variant: "error"
              }
            );
          }
        })
        .catch(error => {
          console.error(error.message);
        });

    }
  }
  setDatagrid2() {

    let docvar = Array();
    docvar = {

      packdoc: this.state.doc_pack,
      doc_code: this.state.dccode
    };

    this.ApiCall.PackGetBoxInsert(docvar)
      .then(res => {
        if (res.status === true) {
          this.getGriddata();
        } else {
          this.props.enqueueSnackbar(
            "ไม่พบข้อมูลงานนี้จากระบบ..ตรวจสอบดูให้แน่ใจ",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });
  }
  setDatagrid() {


    let docvar = Array();
    docvar = {

      loaddoc: this.state.doc_number,

      doc_code: this.state.dccode
    };



    this.ApiCall.InsertDocHead(docvar)
      .then(res => {
        if (res.status === true) {
          this.getGriddata();
        } else {
          this.props.enqueueSnackbar(
            "ไม่พบข้อมูลงานนี้จากระบบ..ตรวจสอบดูให้แน่ใจ",
            {
              variant: "error"
            }
          );
        }
      })
      .catch(error => {
        console.error(error.message);
      });

  }

  render() {
    const list = this.state.mdata.map(data => (
      <tr>
        <th>{data.doc_code}</th>
        <p>{data.sup_name}</p>
        <th />
      </tr>
    ));
    const { completed } = this.state;
    return (
      <div style={{ fontFamily: "Prompt" }}>
        <MDBRow>
          <Grid container spacing={24}>
            <MDBCol md="12">
              <MDBCard>
                <MDBCardBody style={{ fontFamily: "Prompt" }}>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 0 }}
                  />
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 3 }}
                  >
                    <h4 style={{ color: "green" }}>
                      <MDBIcon icon="file-signature" className="pink-text pr-3" />&nbsp;ค้นหาข้อมูลรับของ (Data Signa)
            </h4>
                    <br />
                  </Grid>
                  <Grid
                    item
                    lg={12}
                    xl={12}
                    xs={12}
                    sm={12}
                    md={12}
                    style={{ padding: 3 }}
                  >
                    <LinearProgress
                      color="secondary"
                      variant="determinate"
                      value={completed}
                    />
                    <hr />
                  </Grid>

                  <Grid item lg={12} xl={12} xs={12} sm={12} md={12} style={{ padding: 3 }}>
                    <Grid container spacing={24}>
                      <Grid
                        item
                        lg={4}
                        xl={4}
                        xs={4}
                        sm={4}
                        md={4}
                        style={{ padding: 3, marginTop: 10 }}
                      >
                        {/* <p>LGT201910000292</p> */}
                        <h6 style={{ textAlign: "right", marginLeft: 0 }}>
                          <strong style={{ fontSize: 16 }}>เลขที่เอกสาร</strong>
                        </h6>
                      </Grid>
                      <Grid
                        item
                        lg={4}
                        xl={4}
                        xs={4}
                        sm={4}
                        md={4}
                        style={{ padding: 3 }}
                      >
                        <TextInput
                          style={{ width: "100%", fontSize: 14 }}
                          size="xsmall"
                          value={this.state.description}
                          placeholder="//EXP LGT2019XXX"
                          ref={input => {
                            this.description = input;
                          }}
                          onChange={event =>
                            this.setState({ description: event.target.value }, () => { })
                          }
                        />
                      </Grid>
                      <Grid
                        item
                        lg={4}
                        xl={4}
                        xs={4}
                        sm={4}
                        md={4}
                        style={{ padding: 3 }}
                      >
                        <MDBBtn
                          color="default"
                          onClick={() => {
                            // this.setBoxnow();
                            this.showDocByID()
                          }}
                        >
                          <MDBIcon icon="search" className="ml-1" /> &nbsp;ค้นหา
                        </MDBBtn>
                      </Grid>
                    </Grid>
                  </Grid>

                  <LoadingOverlay active={this.state.active} spinner text="loading...">
                    <Grid item lg={12} xl={12} xs={12} sm={12} md={12} style={{ padding: 3 }}>
                      <Grid container spacing={24}>
                        <Grid
                          item
                          lg={6}
                          xl={6}
                          xs={6}
                          sm={6}
                          md={6}
                          style={{ padding: 3, color: "#000", marginTop: "30px" }}
                        >{/* แบ่งครึ่งกลาง */}
                          {/* <Grid
                          item
                          lg={2}
                          xl={2}
                          xs={2}
                          sm={2}
                          md={2}
                          style={{ padding: 3, backgroundColor: "#000ffff" }}
                        > */}
                          {/* <MDBContainer> */}
                          <MDBRow>
                            <MDBCol style={{ textAlign: "right", marginTop: "10px" }}>
                              <p className="font-weight-bold">เลขที่เอกสาร</p>
                            </MDBCol>
                            <MDBCol style={{color:'#a70000'}}>
                              {/* <p>{this.state.doccode_}</p> */}
                             
                              <TextInput
                                
                                // disabled={true}
                                style={{ width: "100%", fontSize: 14 }}
                                size="xsmall"
                                value={this.state.doccode_}
                              />
                            </MDBCol>
                          </MDBRow>
                          <MDBRow>
                            <MDBCol style={{ textAlign: "right", marginTop: "10px" }}>
                              <p className="font-weight-bold">ขนส่ง</p>
                            </MDBCol>
                            <MDBCol  style={{color:'#a70000'}}>
                              {/* <p>{this.state.typelogis_}</p> */}
                              <TextInput
                                style={{ width: "100%", fontSize: 14 }}
                                size="xsmall"
                                value={this.state.typelogis_}
                              />
                            </MDBCol>
                          </MDBRow>
                          <MDBRow>
                            <MDBCol style={{ textAlign: "right", marginTop: "10px" }}>
                              <p className="font-weight-bold">วันที่เอกสาร</p>
                            </MDBCol>
                            <MDBCol  style={{color:'#a70000'}}>
                              {/* <p>{this.state.datedoc_}</p> */}
                              <TextInput
                                style={{ width: "100%", fontSize: 14 }}
                                size="xsmall"
                                value={this.state.datedoc_}
                              />
                            </MDBCol>
                          </MDBRow>
                          <MDBRow>
                            <MDBCol style={{ textAlign: "right", marginTop: "10px" }}>
                              <p className="font-weight-bold">บริษัทขนส่ง</p>
                            </MDBCol>
                            <MDBCol  style={{color:'#a70000'}}>
                              {/* <p>{this.state.supname_}</p> */}
                              <TextInput
                                style={{ width: "100%", fontSize: 14 }}
                                size="xsmall"
                                value={this.state.supname_}
                              />
                            </MDBCol>
                          </MDBRow>
                          <MDBRow>
                            <MDBCol style={{ textAlign: "right", marginTop: "10px" }}>
                              <p className="font-weight-bold">ทะเบียนรถ</p>
                            </MDBCol>
                            <MDBCol  style={{color:'#a70000'}}>
                              {/* <p>{this.state.carnumber_}</p> */}
                              <TextInput
                                style={{ width: "100%", fontSize: 14 }}
                                size="xsmall"
                                value={this.state.carnumber_}
                              />
                            </MDBCol>
                          </MDBRow>
                          <MDBRow>
                            <MDBCol style={{ textAlign: "right", marginTop: "10px" }}>
                              <p className="font-weight-bold">ชื่อผู้ขับ</p>
                            </MDBCol>
                            <MDBCol  style={{color:'#a70000'}}>
                              {/* <p>{this.state.carname_}</p> */}
                              <TextInput
                                style={{ width: "100%", fontSize: 14 }}
                                size="xsmall"
                                value={this.state.carname_}
                              />
                            </MDBCol>
                          </MDBRow>
                          <MDBRow >
                            <MDBCol style={{ textAlign: "right", marginTop: "10px" }}>
                              <p className="font-weight-bold">เด็กติดรถ</p>
                            </MDBCol>
                            <MDBCol  style={{color:'#a70000'}}>
                              {/* <p>{this.state.carboy_}</p> */}
                              <TextInput
                                style={{ width: "100%", fontSize: 14 }}
                                size="xsmall"
                                value={this.state.carboy_}
                              />
                            </MDBCol>
                          </MDBRow>
                          <MDBRow>
                            <MDBCol style={{ textAlign: "right", marginTop: "10px" }}>
                              <p className="font-weight-bold">สายรถ</p>
                            </MDBCol>
                            <MDBCol  style={{color:'#a70000'}}>
                              {/* <p>{this.state.carrout_}</p> */}
                              <TextInput
                                style={{ width: "100%", fontSize: 14 }}
                                size="xsmall"
                                value={this.state.carrout_}
                              />
                            </MDBCol>
                          </MDBRow>
                          <MDBRow>
                            <MDBCol style={{ textAlign: "right", marginTop: "10px" }}>
                              <p className="font-weight-bold">เบอร์โทร</p>
                            </MDBCol>
                            <MDBCol  style={{color:'#a70000'}}>
                              {/* <p>{this.state.carphone_}</p> */}
                              <TextInput
                                style={{ width: "100%", fontSize: 14 }}
                                size="xsmall"
                                value={this.state.carphone_}
                              />
                            </MDBCol>
                          </MDBRow>
                          <MDBRow>
                            <MDBCol style={{ textAlign: "right", marginTop: "10px" }}>
                              <p className="font-weight-bold">ค่าขนส่ง</p>
                            </MDBCol>
                            <MDBCol  style={{color:'#a70000'}}>
                              {/* <p>{this.state.logisprice_}</p> */}
                              <TextInput
                                style={{ width: "100%", fontSize: 14 }}
                                size="xsmall"
                                value={this.state.logisprice_}
                              />
                            </MDBCol>
                          </MDBRow><MDBRow>
                            <MDBCol style={{ textAlign: "right", marginTop: "10px" }}>
                              <p className="font-weight-bold">จำนวนสาขา</p>
                            </MDBCol>
                            <MDBCol  style={{color:'#a70000'}}>
                              {/* <p>{this.state.branch_}</p> */}
                              <TextInput
                                style={{ width: "100%", fontSize: 14 }}
                                size="xsmall"
                                value={this.state.branch_}
                              />
                            </MDBCol>
                          </MDBRow><MDBRow>
                            <MDBCol style={{ textAlign: "right", marginTop: "10px" }}>
                              <p className="font-weight-bold">จำนวนกล่อง</p>
                            </MDBCol>
                            <MDBCol  style={{color:'#a70000'}}>
                              {/* <p>{this.state.box_}</p> */}
                              <TextInput
                                style={{ width: "100%", fontSize: 14 }}
                                size="xsmall"
                                value={this.state.box_}
                              />
                            </MDBCol>
                          </MDBRow>
                          {/* </MDBContainer> */}
                          {/* </Grid> */}


                        </Grid>
                        <Grid
                          item
                          lg={6}
                          xl={6}
                          xs={6}
                          sm={6}
                          md={6}
                          style={{ padding: 3, color: "#000", marginTop: "30px" }}
                        >
                          {/* <LoadingOverlay active={this.state.active} spinner text="loading...">
                            <Datagrid
                              datadoc={this.state.mdata}
                              loadprint={this.load_print}
                              CancelDocjob={this.CancelDocjob}
                            />
                          </LoadingOverlay> */}
                          <MDBRow>
                            <MDBCol lg="1" xl="1" xs="1" sm="1" md="1" />
                            <MDBCol style={{ textAlign: "right", marginTop: "10px" }} lg="2" xl="2" xs="2" sm="2" md="2">
                              <p className="font-weight-bold">สถานะ</p>
                            </MDBCol>
                            <MDBCol  style={{color:'#a70000'}}>
                              {/* <p>{this.state.docstatus_}</p> */}
                              <TextInput
                                style={{ width: "100%", fontSize: 14 }}
                                size="xsmall"
                                value={this.state.docstatus_}
                              />
                            </MDBCol>
                            <MDBCol lg="2" xl="2" xs="2" sm="2" md="2" />
                          </MDBRow>

                          {this.state.listSigna.map((item, index) => (

                            // item.sign_time == null ?
                            //   (
                            //     console.log("true")
                            //   ) : (
                            //     console.log("false")
                            //   )

                            <MDBRow>
                              <MDBCol sm="1" md="1" lg="1" />
                              <MDBCol style={{ marginTop: "20px" }}>
                                <MDBCard>
                                  <MDBCardBody>
                                    <MDBRow>
                                      <MDBCol lg="1" xl="1" xs="1" sm="1" md="1" />
                                      <MDBCol lg="3" xl="3" xs="3" sm="3" md="3" >
                                        <MDBRow>
                                          <p className="font-weight-bold">Br. {item.branch}</p>
                                        </MDBRow>
                                        <MDBRow>
                                          <p className="font-weight-bold">สาขา {item.branchname}</p>
                                        </MDBRow>
                                        <MDBRow>
                                          <p className="font-weight-bold">{item.sign_time === null ? '' : 'เวลา '+item.sign_time}</p>
                                        </MDBRow>
                                      </MDBCol>
                                      <MDBCol lg="8" xl="8" xs="8" sm="8" md="8">
                                        <MDBView hover zoom>
                                        {item.sign === null&&(
                                          <img
                                            style={{ width: '200px' }}
                                            src={noimg}
                                            className="img-fluid"
                                            alt=""
                                          />
                                        )}

                                        {item.sign !== null&&(
                                          <img
                                            style={{ width: '280px' }}
                                            src={DecodeImg + item.sign}
                                            className="img-fluid"
                                            alt=""
                                          />
                                        )}
                                         
    
                                        </MDBView>
                                      </MDBCol>
                                    </MDBRow>
                                  </MDBCardBody>
                                </MDBCard>
                              </MDBCol>
                              <MDBCol sm="1" md="1" lg="1" />
                            </MDBRow>
                          ))}
                        </Grid>
                        {/* style={{ backgroundColor: "#2bbbad" }} */}
                      </Grid>
                    </Grid>
                  </LoadingOverlay>
                </MDBCardBody>
              </MDBCard>
            </MDBCol>
          </Grid>
        </MDBRow>
      </div>


    );
  }
}
export default withSnackbar(DataSigna);
